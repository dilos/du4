dosfstools (4.2-1+dilos1) unstable; urgency=medium

  * Build for DilOS.

 -- DilOS Team <dilos@dilos.org>  Sat, 03 Jun 2023 18:25:30 +0300

dosfstools (4.2-1) unstable; urgency=medium

  [ Andreas Bombe ]
  * New upstream version 4.2
    * fatlabel now additionally supports changing the volume ID (Closes:
      #563746)
    * fsck.fat handles reads user input by character instead of by line now,
      resolving some confusing reactions to input (Closes: #831671, #300505)
    * fsck.fat also offers a quit option on every prompt (Closes: #728355)
    * Update d/copyright, change Upstream-Contact to Pali Rohár
    * Remove Build-Depends on libudev-dev and pkg-config, not used anymore
  * No longer automatically enables Atari mode when running on Atari 68k Linux
    for predictable behavior across all hardware (Closes: #559003)
  * Increase debhelper compat to 13 and use new method to specify compat
    * Remove override of dh_missing in d/rules, default now with debhelper 13
  * Remove d/source/options with compression setting
  * Do not install superfluous COPYING file to /usr/share/doc
  * Add Rules-Requires-Root: no to d/control
  * Change d/watch file to version 4
  * Bump Standards-Version to 4.5.1

  [ Ondřej Nový ]
  * d/changelog: Remove trailing whitespaces

 -- Andreas Bombe <aeb@debian.org>  Mon, 08 Feb 2021 02:13:30 +0100

dosfstools (4.1-2) unstable; urgency=medium

  * Drop Build-Depends on dh-autoreconf, not needed with debhelper >= 10
  * Change Vcs-* fields in debian/control to salsa repository addresses
  * Raise debhelper compat level to 11
  * Use `dh_missing --fail-missing` instead of `dh_install --fail-missing`
  * Bump Standards-Version to 4.1.4, no changes necessary
  * Use https URL for debian/copyright Format field

 -- Andreas Bombe <aeb@debian.org>  Sun, 13 May 2018 00:59:39 +0200

dosfstools (4.1-1) unstable; urgency=medium

  * New upstream version 4.1
  * Remove patch for heads/sectors default, integrated upstream
  * Add xxd to Build-Depends and re-enable dh_auto_test to run test suite
  * Bump debhelper compat level to 10
  * Remove --parallel and --with autoreconf from dh invocation, these are
    default with debhelper 10

 -- Andreas Bombe <aeb@debian.org>  Wed, 25 Jan 2017 00:48:34 +0100

dosfstools (4.0-2) unstable; urgency=medium

  * Add patch to default to 64/32 heads/sectors for filesystems smaller than
    512 MB (Closes: #823881)

 -- Andreas Bombe <aeb@debian.org>  Wed, 11 May 2016 04:09:18 +0200

dosfstools (4.0-1) unstable; urgency=medium

  * New Upstream version 4.0
  * Add debian/NEWS entry about reinstated alignment by mkfs.fat
  * Add Public Domain license for src/blkdev/* and update copyright lines in
    debian/copyright
  * Bump Standards-Version to 3.9.8

 -- Andreas Bombe <aeb@debian.org>  Sat, 07 May 2016 00:31:35 +0200

dosfstools (3.99.0~git20160203-1) experimental; urgency=medium

  * New Upstream snapshot version 3.99.0~git20160203
    - removes unneeded linux/fd.h include
    - reintroduces the missing historic documentation
  * Switch Vcs-Browser and Vcs-Git URLs to https transport

 -- Andreas Bombe <aeb@debian.org>  Wed, 03 Feb 2016 04:08:32 +0100

dosfstools (3.99.0~git20160127-2) experimental; urgency=medium

  * Add pkg-config to Build-Depends

 -- Andreas Bombe <aeb@debian.org>  Tue, 02 Feb 2016 01:19:58 +0100

dosfstools (3.99.0~git20160127-1) experimental; urgency=medium

  * New Upstream snapshot version 3.99.0~git20160127
    - new device probing in mkfs fixes long standing problems with
      misidentifying devices and randomly requiring the -I option
    - no longer Linux-only
  * Replace upstream/signing-key.asc with minimal export of the same key
  * Remove building of dosfstools-dbg, let automatic debug package be built
    instead
  * Adjust debian/rules for upstream change to autotools and add Build-Depends
    on dh-autoreconf
  * Change Architecture from linux-any to any
  * Add Build-Depends on libudev-dev for linux-any architectures
  * Remove override_dh_builddeb to enable xz compression, it is already
    default
  * Remove handling for cross compiling and let dh_auto_configure do it
    automatically

 -- Andreas Bombe <aeb@debian.org>  Mon, 01 Feb 2016 02:39:02 +0100

dosfstools (3.0.28-2) unstable; urgency=medium

  * Enable checking of PGP signatures on upstream tarballs in debian/watch
  * Notify users about the default mode change for fsck in NEWS file

 -- Andreas Bombe <aeb@debian.org>  Sat, 22 Aug 2015 01:03:28 +0200

dosfstools (3.0.28-1) unstable; urgency=medium

  * New upstream version 3.0.28
    - interactive repair mode is now the default for fsck.fat, ending
      confusion about the previous default mode that looked like interactive
      repair but never offered the option at the end to actually modify the
      filesystem (Closes: #417639)
    - fsck.fat now checks that the first cluster of a file is not 1, thereby
      also preventing a possible segfault (Closes: #773885)
    - 0xF0 is now allowed to be specified as media type for mkfs.fat
      (Closes: #753951)

 -- Andreas Bombe <aeb@debian.org>  Mon, 01 Jun 2015 02:33:30 +0200

dosfstools (3.0.27-1) unstable; urgency=medium

  * New upstream version 3.0.27
    - fixes fatlabel mangling long file names in root directory
      (Closes: #768909)
    - fixes spurious uncorrectable empty file name error reported by
      fsck.fat (thanks to AlexisM for finding the cause)
      (Closes: #764992)
  * New maintainer
  * New upstream, change debian/watch and fields in debian/control and
    debian/copyright accordingly
  * Remove 0001-LFN-is-no-volume-entry.patch, already fixed in upstream

 -- Andreas Bombe <aeb@debian.org>  Wed, 12 Nov 2014 03:21:26 +0100

dosfstools (3.0.26-5) unstable; urgency=medium

  * QA upload
  * set Vcs-* to collab-maint
  * add 0001-LFN-is-no-volume-entry.patch:
    don't overwrite LFN entries with volume entries (Closes: #768909)

 -- Bernhard R. Link <brlink@debian.org>  Tue, 11 Nov 2014 20:38:15 +0100

dosfstools (3.0.26-4) unstable; urgency=medium

  * QA upload.
  * Bumped Standards-Version to 3.9.6.
  * Added a debian/watch file.
  * debian/rules: added the DEB_BUILD_MAINT_OPTIONS
    variable to fix the issues pointed by blhc.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 12 Oct 2014 03:09:10 -0300

dosfstools (3.0.26-3) unstable; urgency=low

  * Orphaning package.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Fri, 25 Jul 2014 21:11:41 +0200

dosfstools (3.0.26-2) unstable; urgency=low

  * Updating year in copyright file.
  * Building with dh --parallel.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Mon, 31 Mar 2014 19:47:37 +0200

dosfstools (3.0.26-1) experimental; urgency=low

  * Merging upstream version 3.0.26.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Fri, 07 Mar 2014 18:52:50 +0100

dosfstools (3.0.25-1) experimental; urgency=low

  * Merging upstream version 3.0.25.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Fri, 17 Jan 2014 07:14:46 +0100

dosfstools (3.0.24-1) experimental; urgency=low

  * Updating to standards version 3.9.5.
  * Merging upstream version 3.0.24.
  * Including legacy symlinks.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Sat, 23 Nov 2013 10:47:09 +0100

dosfstools (3.0.23-1) experimental; urgency=low

  * Updating vcs fields.
  * Merging upstream version 3.0.23.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Tue, 15 Oct 2013 08:09:23 +0200

dosfstools (3.0.22-1) experimental; urgency=low

  * Merging upstream version 3.0.22:
    - improves bootability of hdd filesystems (Closes: #552673, #557061).
    - allows writing all lowercase labels (Closes: #714971).
  * Using uninstall-symlinks target rather than manual removal in rules.
  * Calling dh_install with --fail-missing in rules.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Fri, 19 Jul 2013 07:06:36 +0200

dosfstools (3.0.21-2) experimental; urgency=low

  * Adding vcs fields.
  * Wrapping control fields.
  * Avoid pulling in legacy symlinks in udeb.
  * Also including fatlabel in udeb.
  * Dropping legacy symlinks.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Wed, 17 Jul 2013 12:48:19 +0200

dosfstools (3.0.21-1) experimental; urgency=low

  * Merging upstream version 3.0.21.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Tue, 16 Jul 2013 08:50:22 +0200

dosfstools (3.0.20-1) experimental; urgency=low

  * Merging upstream version 3.0.20:
    - Fixing default sectors per cluster for FAT32 (Closes: #690062).
    - Softening message about different boot sectors a bit (Closes: #704198).

 -- Daniel Baumann <mail@daniel-baumann.ch>  Wed, 12 Jun 2013 12:29:24 +0200

dosfstools (3.0.19-1) experimental; urgency=low

  * Updating package descriptions.
  * Merging upstream version 3.0.19.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Tue, 11 Jun 2013 18:50:53 +0200

dosfstools (3.0.18-1) experimental; urgency=low

  * Merging upstream version 3.0.18:
    - dosfslabel: Do not read beyond string length (Closes: #709587).
  * Updating udeb debhelper install file for upstreams file renamings.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Thu, 06 Jun 2013 09:56:40 +0200

dosfstools (3.0.17-2) experimental; urgency=low

  * Building dosfstools on linux architectures only.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Thu, 06 Jun 2013 08:14:53 +0200

dosfstools (3.0.17-1) experimental; urgency=low

  * Shortening links to upstream homepage.
  * Merging upstream version 3.0.17:
    - fixes segfault in dosfslabel (Closes: #702392).
    - clarifies label max lenghts in manpage (Closes: #655953).

 -- Daniel Baumann <mail@daniel-baumann.ch>  Wed, 29 May 2013 10:24:54 +0200

dosfstools (3.0.16-2) unstable; urgency=low

  * Removing all references to my old email address.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Sun, 10 Mar 2013 19:58:06 +0100

dosfstools (3.0.16-1) unstable; urgency=low

  * Merging upstream version 3.0.16.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Fri, 01 Mar 2013 09:03:53 +0100

dosfstools (3.0.15-1) unstable; urgency=low

  [ Colin Watson ]
  * Using correct compiler when cross-building.

  [ Daniel Baumann ]
  * Merging upstream version 3.0.15:
    - Bugfix root directory allocation (Closes: #639393).
    - Bugfix messages about unicode filenames (Closes: #596336).

 -- Daniel Baumann <mail@daniel-baumann.ch>  Thu, 21 Feb 2013 15:20:07 +0100

dosfstools (3.0.14-1) unstable; urgency=low

  * Merging upstream version 3.0.14.
  * Updating to standards version 3.9.4.
  * Updating copyright file for 2013.
  * Dropping dpkg compression level.
  * Adding dpkg-source local-options.
  * Applying slightly modified patch from Colin Watson
    <cjwatson@ubuntu.com> to add a udeb package, mainly so that d-i can
    use mkdosfs instead of deprecated and semi-broken libparted code
    (Closes: #698433).

 -- Daniel Baumann <mail@daniel-baumann.ch>  Wed, 23 Jan 2013 13:42:32 +0100

dosfstools (3.0.13-1) unstable; urgency=low

  * Merging upstream version 3.0.13.
  * Switching to xz compression.
  * Updating to debhelper version 9.
  * Updating to standards version 3.9.3.
  * Updating copyright file to format 1.0.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Sat, 30 Jun 2012 19:15:41 +0200

dosfstools (3.0.12-1) unstable; urgency=low

  * Merging upstream version 3.0.12.
  * Updating to standards version 3.9.2.
  * Removing vcs fields.
  * Updating maintainer and uploaders fields.
  * Updating year in copyright.
  * Making packaging distribution neutral.
  * Removing references to my old email address.
  * Sorting overrides in rules.
  * Using compression level 9 also for binary packages.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Sat, 29 Oct 2011 08:53:43 +0200

dosfstools (3.0.11-1) experimental; urgency=low

  * Merging upstream version 3.0.11.
  * Switching to source format 3.0 (quilt).
  * Updating to debhelper version 8.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Fri, 24 Dec 2010 18:08:19 +0100

dosfstools (3.0.10-1) experimental; urgency=low

  * Updating standards version to 3.9.0.
  * Merging upstream version 3.0.10:
    - Fixes problem in dosfsck with false positives in bad_name() when running
      in interactive mode (Closes: #596327).
    - Updates LFN when renaming or deleting short filename (Closes: #596329).
  * Updating standards version to 3.9.1.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Sun, 12 Sep 2010 09:39:52 +0200

dosfstools (3.0.9-1) unstable; urgency=low

  * Updating to standards 3.8.4.
  * Merging upstream version 3.0.9:
    - fixes regression with aligning (Closes: #567337).

 -- Daniel Baumann <mail@daniel-baumann.ch>  Sun, 31 Jan 2010 08:34:41 +0100

dosfstools (3.0.8-1) unstable; urgency=low

  * Updating year in copyright file.
  * Merging upstream version 3.0.8.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Sat, 23 Jan 2010 10:19:23 +0100

dosfstools (3.0.7-1) unstable; urgency=low

  * Correcting wrong vcs-browser field.
  * Adding explicit debian source version 1.0 until switch to 3.0.
  * Merging upstream version 3.0.7:
    - Fixes dosfslabel to set volume label in the right place (Closes: #559985).

 -- Daniel Baumann <mail@daniel-baumann.ch>  Thu, 24 Dec 2009 10:57:15 +0100

dosfstools (3.0.6-1) unstable; urgency=low

  * Updating maintainer field.
  * Updating vcs fields.
  * Updating package to standards version 3.8.3.
  * Sorting depends.
  * Adding misc depends to debug package.
  * Shortening long description of debug package.
  * Minimizing rules file.
  * Bumping versioned build-depends on debhelper.
  * Removing not used patch from debian directory.
  * Merging upstream version 3.0.6.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Sun, 04 Oct 2009 11:06:57 +0200

dosfstools (3.0.5-1) unstable; urgency=low

  * Merging upstream version 3.0.5:
    - fixes bug from VFAT patent avoidance patch that can resultin data loss
      (Closes: #538758).

 -- Daniel Baumann <mail@daniel-baumann.ch>  Mon, 27 Jul 2009 14:38:51 +0200

dosfstools (3.0.4-1) unstable; urgency=low

  * Adding reference to #502961 in previous changelog entry.
  * Updating section of the debug package.
  * Merging upstream version 3.0.4.
  * Updating package to standards version 3.8.2.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Tue, 21 Jul 2009 08:17:00 +0200

dosfstools (3.0.3-1) unstable; urgency=low

  * Merging upstream version 3.0.3:
    - Also declaring arm as an unaligned architecture, see Debian bug #502961.
  * Tidy rules file.
  * Upgrading to standards 3.8.1.
  * Rewriting copyright file for upstream version 3.x.
  * Using correct rfc-2822 date formats in changelog.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Mon, 18 May 2009 15:23:49 +0200

dosfstools (3.0.2-1) unstable; urgency=low

  * Merging upstream version 3.0.2.
  * Removing double files.
  * Prefixing debhelper files with package name.
  * Updating year in copyright.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Sat, 28 Feb 2009 09:57:00 +0100

dosfstools (3.0.1-1) unstable; urgency=low

  * Replacing obsolete dh_clean -k with dh_prep.
  * Merging upstream version 3.0.1:
    - Changing some wording to make the indended meaning of "full-disk device"
      more obvious (Closes: #500623).

 -- Daniel Baumann <mail@daniel-baumann.ch>  Sun, 23 Nov 2008 22:51:46 +0100

dosfstools (3.0.0-1) unstable; urgency=low

  * Merging upstream version 3.0.0.
  * Updating homepage field in control.
  * Updating upstream information in copyright file.
  * Removing now useless lintian override.
  * Removing now useless manpage.
  * Removing now useless patches.
  * Removing now useless docs.
  * Updating rules to for new upstream.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Sun, 28 Sep 2008 11:56:00 +0200

dosfstools (2.11-8) unstable; urgency=low

  * Adding debug package.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Sat, 20 Sep 2008 21:52:00 +0200

dosfstools (2.11-7) unstable; urgency=low

  * Adding manpage for dosfslabel, thanks to Francois Wendling
    <frwendling@free.fr> (Closes: #496760).
  * Updating vcs fields in control file.
  * Using patch-stamp rather than patch in rules file.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Sat, 20 Sep 2008 21:48:00 +0200

dosfstools (2.11-6) unstable; urgency=high

  * Temporarily disabling bootcode.dpatch since it breaks syslinux,
    thanks to Joey Hess <joeyh@debian.org> (Closes: #489292).

 -- Daniel Baumann <mail@daniel-baumann.ch>  Fri, 04 Jul 2008 20:28:00 +0200

dosfstools (2.11-5) unstable; urgency=medium

  * Adding patch to mention fsckNNNN.rec files in dosfsck manpage
    (Closes: #444596).
  * Adding patch from Sam Bingner <sam@bingner.com> to add option for using a
    bootcode template (Closes: #303442).
  * Adding patch from Adonikam Virgo <adonikam@virgonet.org> to fix backup
    sector getopt (Closes: #232387, #479794).
  * Adding patch from Karl Tomlinson <karlt@karlt.net> to fix segfaults with
    zero slots in lfn (Closes: #152550, #353198, #356377, #401798).
  * Rediffing 99-conglomeration.dpatch.
  * Adding patch from Eero Tamminen <eero.tamminen@nokia.com> to improve memory
    efficiencey when checking filesystems.
  * Adding patch to list alternative binary names in manpage synopsis
    (Closes: #284983).
  * Adding patch to not deny FAT32 auto-selection in mkdosfs manpage
    (Closes: #414183).
  * Adding patch to not use confusing 'drop' in dosfsck manpage where 'delete'
    in dosfsck manpage where 'delete' is meant (Closes: #134100).
  * Listing alternative binary names in long-description (Closes: #434381).
  * Updating manpage spelling patch to also cover wrong acknowledge header
    (Closes: #306659).
  * Breaking out manpage typos patch.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Fri, 27 Jun 2008 09:03:00 +0200

dosfstools (2.11-4) unstable; urgency=low

  * Redone debian packaging from scratch.
  * Both stop avoiding -O2 and stop adding -fno-strict-aliasing to OPTFLAGS on
    alpha; seems not to be required anymore.
  * Added patch from Jakub Jelinek <jakub@redhat.com> to support
    -D_FORTIFY_SOURCE=2 (for future use).
  * Added patch from Jeremy Katz <katzj@redhat.com> to add dosfslabel
    (originally by Peter Jones).
  * Added patch from Pavol Rusnak <prusnak@suse.cz> to use O_EXCL in mkdosfs.
  * Added patch from Petr Gajdos <pgajdos@suse.cz> to automatically determine
    sector size of the device.

 -- Daniel Baumann <mail@daniel-baumann.ch>  Thu, 26 Jun 2008 13:13:00 +0200

dosfstools (2.11-3) unstable; urgency=low

  * New maintainer (Closes: #488018).

 -- Daniel Baumann <mail@daniel-baumann.ch>  Thu, 26 Jun 2008 12:31:00 +0200

dosfstools (2.11-2.3) unstable; urgency=low

  * NMU
  * dpkg --print-gnu-build-architecture is gone, use dpkg-architecture
    instead. Closes: #407192
  * Fixed errors in the mkdosfs manpage that could cause confusion to
    readers, Thanks to Onno Benschop. Closes: #433561 (LP: #126121)

 -- Joey Hess <joeyh@debian.org>  Mon, 10 Sep 2007 15:57:36 -0400

dosfstools (2.11-2.2) unstable; urgency=low

  * Non-maintainer upload.
  * Apply Ubuntu patches. Closes: #417673

 -- Andreas Barth <aba@not.so.argh.org>  Sat, 09 Jun 2007 21:18:21 +0000

dosfstools (2.11-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix unaligned memory accesses which cause warnings to appear everytime
    the elilo bootloader script runs.  This has led a number of users to
    believe their install has failed.  Thanks to Khalid Aziz for the patch.
    Closes: #258839.

 -- dann frazier <dannf@debian.org>  Thu, 09 Jun 2005 18:14:45 -0600

dosfstools (2.11-2) unstable; urgency=low

  * Oops, debian/rules overrides OPTFLAGS and therefore the
    -D_FILE_OFFSET_BITS=64 in the toplevel Makefile had no effect; added
    $(shell getconf LFS_CFLAGS) to OPTFLAGS as suggested by Lars Wirzenius in
    #300126. Sorry, I tested a version compiled by the upstream Makefile...
    Closes: #300126, #301254.
  * #302517 was indeed the same as #294177, and fix is the same (use __u8) as
    in 2.11-1. Closes: #302517.

 -- Roman Hodek <roman@hodek.net>  Sun, 03 Apr 2005 13:56:55 +0200

dosfstools (2.11-1) unstable; urgency=low

  * New upstream version (Closes: #293394, #295181, #294177, #270023, #258402,
    #232482, #214656, #286219, #276834, #266254, #128800)

 -- Roman Hodek <roman@hodek.net>  Sat, 12 Mar 2005 17:19:27 +0100

dosfstools (2.10-1) unstable; urgency=low

  * New upstream version:
     - dosfsck: various 64-bit fixes and removed some warnings by Michal
       Cihar <mcihar@suse.cz>
     - mkdosfs: better error message if called without parameters (also
       suggested by Michal)
  * recompilation removed errno@GLIBC_2.0 symbol; Closes: #168540
    (probably already 2.9-1 did)

 -- Roman Hodek <roman@hodek.net>  Mon, 22 Sep 2003 22:15:32 +0200

dosfstools (2.9-1) unstable; urgency=low

  * New upstream version:
   (Closes: #156266, #139198, #152769, #152868, #181196)
    - dosfsck: if EOF from stdin, exit with error code
   	- dosfsck: Fix potential for "Internal error: next_cluster on bad cluster".
   	- dosfsck: When clearing long file names, don't overwrite the dir
   	  entries with all zeros, but put 0xe5 into the first byte.
   	  Otherwise, some OSes stop reading the directory at that point...
   	- dosfsck: in statistics printed by -v, fix 32bit overflow in number
   	  of data bytes.
   	- dosfsck: fix an potential overflow in "too many clusters" check
   	- dosfsck: fix 64bit problem in fat.c (Debian bug #152769)
   	- dosfsck: allow FAT size > 32MB.
   	- dosfsck: allow for only one FAT
   	- dosfsck: with -v, also check that last sector of the filesystem can
   	  be read (in case a partition is smaller than the fs thinks)
   	- mkdosfs: add note in manpage that creating bootable filesystems is
   	  not supported.
   	- mkdosfs: better error message with pointer to -I if target is a
   	  full-disk device.
  * debian/control: Added build dependency on debhelper (Closes: #168388)
  * debian/control: spelling fix (Closes: #124564)
  * debian/control: metion names of tools in description (Closes: #186047)

 -- Roman Hodek <roman@hodek.net>  Thu, 15 May 2003 20:54:04 +0200

dosfstools (2.8-1) unstable; urgency=low

  * New upstream version fixing an endless loop.
    (Closes: #87205, #86373, #87590)

 -- Roman Hodek <roman@hodek.net>  Wed, 28 Feb 2001 17:23:16 +0100

dosfstools (2.7-1) unstable; urgency=low

  * New upstream version with various bug fixes. (Closes: #83883)
  * Changed maintainer e-mail addr.

 -- Roman Hodek <roman@hodek.net>  Wed, 14 Feb 2001 12:49:00 +0100

dosfstools (2.6-1) unstable; urgency=low

  * New upstream version with various bug fixes.

 -- Roman Hodek <roman@hodek.net>  Tue, 28 Nov 2000 17:27:35 +0100

dosfstools (2.5-1) unstable; urgency=low

  * New upstream version fixing llseek() on alpha (Closes: #54145)

 -- Roman Hodek <Roman.Hodek@informatik.uni-erlangen.de>  Fri, 7 Jan 2000 09:26:51 +0100

dosfstools (2.4-1) unstable; urgency=low

  * New upstream version fixing compilation problem on alpha (Closes: #48331)

 -- Roman Hodek <Roman.Hodek@informatik.uni-erlangen.de>  Tue, 26 Oct 1999 09:38:39 +0200

dosfstools (2.3-1) unstable; urgency=low

  * New upstream version:
     - Manpage fixes.
     - Fixed usage message of mkdosfs.
     - Fixed compilation on alpha (llseek).
     - Fixed unaligned accesses on alpha (Closes: #47714)
     - Fixed renaming of files in dosfsck (extension wasn't really
       written). Closes: #45774
  * Remove bashisms from debian/rules.
  * FHS transition.
  * On alpha, omit -O2 and add -fno-strict-aliasing to OPTFLAGS, as
    otherwise the programs fail with unaligned traps.
  * FHS transition (Standards-Version 3.0.1).

 -- Roman Hodek <Roman.Hodek@informatik.uni-erlangen.de>  Fri, 22 Oct 1999 13:30:59 +0200

dosfstools (2.2-1) unstable; urgency=low

  * New upstream version. Closes: #40533
  * Updated copyright file for new location /usr/share/common-licenses/GPL.
  * Updated Standards-Version to 3.0.0.

 -- Roman Hodek <Roman.Hodek@informatik.uni-erlangen.de>  Tue, 6 Jul 1999 16:07:22 +0200

dosfstools (2.1-1) unstable; urgency=low

  * New upstream version.
  * Also installs symlinks mkfs.vfat and fsck.vfat,so that also
    filesystems listed with type "vfat" in /etc/fstab can be automatically
    checked.

 -- Roman Hodek <Roman.Hodek@informatik.uni-erlangen.de>  Fri, 28 May 1999 11:25:17 +0200

dosfstools (2.0-1) unstable; urgency=low

  * I'm now upstream maintainer, too.
  * Adapted debian/rules for new central Makefile and rewritten to
    debhelper.
  * Fixed harmless warnings in mkdosfs.c and fat.c.
  * mkdosfs.c: Return type of getopt() must be stored in an int, not in a
    char. (Showed up on powerpc, where chars are default unsigned.)

 -- Roman Hodek <Roman.Hodek@informatik.uni-erlangen.de>  Fri, 30 Apr 1999 14:37:37 +0200

dosfstools (1.0-16) unstable; urgency=low

  * Both tools are not maintained upstreams anymore, so I take over
    general maintainership now...
  * Both mkdosfs and dosfsck now support the FAT32 format. dosfsck
    automatically detects it. For mkdosfs, 32 is now a valid argument to
    -F, but FAT32 format isn't selected automatically (yet). With FAT32,
    you can also set the number of reserved sectors (-R) and location of
    the boot sector backup (-b) if you like (defaults are reasonable).
  * dosfsck can handle and check VFAT-style long filenames now. It uses
    the long names in listing etc. when available. There are also some
    checks on the structures of LFNs and some fixes for possible problems.
  * Implemented Atari format in both, dosfsck and mkdosfs. Under Atari TOS
    basically the same FAT format is used as under DOS, however, there are
    some little differences. Both tools now automatically select Atari
    format if they run on an Atari. You can switch between standard MS-DOS
    and Atari format with the -A option.
  * Applied patch by Giuliano Procida <gpp10@cus.cam.ac.uk> to add loop
    device support to mkdosfs: Usual floppy sizes are detected and
    parameters (media byte,...) are set up accordingly.
    My own additions to this: Don't die on loop devices that don't have
    such a floppy size, but use some default hd params. Added endianess
    conversions to Giulianos patch.
  * More/better data in boot sector dump of dosfsck -v.
  * Fixed lots of gcc warnings in the source. Removed -Wno-parentheses flag.
  * Made dosfsck -v a bit more verbose.
  * Extended README's for FAT32/LFN.
  * Written a README for Atari format differences.
  * Some minor cleanups in debian/rules.
  * Install README files as README.{mkdosfs,dosfsck,Atari}.gz.
  * AFAIK the tools still have alignment problems on Alpha machines.
    Someone wanted to send me a patch, but I haven't heard from him for
    months...
  * Set Standards-Version to 2.5.0.0 (no changes).

 -- Roman Hodek <Roman.Hodek@informatik.uni-erlangen.de>  Wed, 28 Apr 1999 11:06:15 +0200

dosfstools (1.0-15) frozen unstable; urgency=low

  * Applied patch by Juan Cespedes <cespedes@debian.org> to make mkdosfs
    work with newer 2.1 kernels again. (Fixes: #20320)
  * Remove CC=gcc in debian/rules to make cross-compiling possible.

 -- Roman Hodek <Roman.Hodek@informatik.uni-erlangen.de>  Thu, 30 Apr 1998 17:09:08 +0200

dosfstools (1.0-14) frozen unstable; urgency=medium

  * New maintainer
  * Ignore long name directory slots of VFAT, instead of trying to correct
    that "file names". Fixes: #20711
  * Don't consider file names with chars >= 128 to be bad, they're allowed.

 -- Roman Hodek <Roman.Hodek@informatik.uni-erlangen.de>  Thu, 30 Apr 1998 10:00:16 +0200

dosfstools (1.0-13) stable; urgency=low

  * Fixed nasty bug that caused every file with a name like xxxxxxxx.xxx
    to be treated as bad name that needed to be fixed. (closes: Bug#17389)

 -- Martin Schulze <joey@finlandia.infodrom.north.de>  Thu, 26 Feb 1998 01:14:40 +0100

dosfstools (1.0-12) stable; urgency=low

  * Moved executables and their links into /sbin (Bug#15037)
  * Corrected Standards-Version to 2.3.0.1

 -- Martin Schulze <joey@finlandia.infodrom.north.de>  Fri, 9 Jan 1998 21:49:48 +0100

dosfstools (1.0-11) stable; urgency=low

  * Applied patches to source files from Juan Cespedes which got lost in
    the last upload (Bug#16493, Bug#16494, Bug#16490)
  * build-stamp is now removed first in clean target (Bug#16491)

 -- Martin Schulze <joey@finlandia.infodrom.north.de>  Sat, 3 Jan 1998 15:06:27 +0100

dosfstools (1.0-10) stable; urgency=low

  * Added links for mkfs.msdos and fsck.msdos (Bug#15037)
  * Added links for mkfs.msdos.8 and fsck.msdos.8
  * Corrected source location
  * Rewrote bad_name() (Bug#9871, part 2)
  * s/int/time_t in check.c to omit a compiler warning (Bug#9871, part 3.1)
  * Modified defaults, interactive is the default now
  * Fixed renaming of files, they are saved now (Bug#9871, part 1)
  * Fixed return type of date_dos2unix (Bug#9871, part 3.2)

 -- Martin Schulze <joey@finlandia.infodrom.north.de>  Wed, 31 Dec 1997 18:59:53 +0100

dosfstools (1.0-9.1) unstable; urgency=low

  * Non-maintainer release
  * .diff file was wrong in 1.0-9; fixed (Bug#13102)
  * Fixed endianess patches (fixes Bug#11648)
  * Built with libc6

 -- Juan Cespedes <cespedes@etsit.upm.es>  Thu, 23 Oct 1997 23:19:34 +0200

dosfstools (1.0-9) stable; urgency=low

  * Added endianess patches from Frank Neumann
    <Frank.Neumann@Informatik.Uni-Oldenburg.DE> (Bug#9959)
  * Updated Werner Almesbergers address
  * Added -I switch to mkdosfs to allow full disk devices (Bug#10789)

 -- Martin Schulze <joey@finlandia.infodrom.north.de>  Sun, 20 Jul 1997 14:51:33 +0200

dosfstools (1.0-8) stable; urgency=low

  * Fixed typo in mkdosfs.c (Bug#7396)

  * New maintainer address

 -- Martin Schulze <joey@infodrom.north.de>  Mon, 28 Apr 1997 12:51:13 +0200

dosfstools (1.0-7) stable; urgency=low

  * Minor fixes

  * Converted to Standards-Version 2.1.1.2

 -- Martin Schulze <joey@debian.org>  Sat, 8 Feb 1997 15:03:52 +0100

Mon Jan 20 22:45:23 1997  Martin Schulze  <joey@finlandia.infodrom.north.de>

	* debian.control: Corrected otherfs to otherosfs :-)

Sun Jan 19 23:57:09 1997  Martin Schulze  <joey@finlandia.infodrom.north.de>

	* mkdosfs.c: Removed one line printing out debug information.

Wed Jan 15 00:25:02 1997  Martin Schulze  <joey@finlandia.infodrom.north.de>

	* mkdosfs.c: Modified code to work properly under AXP. Thanks to
	Matt Gundry <mjgundry@primenet.com> for contacting me and
	providing patched sources.

Mon Jan 13 13:00:14 1997  Martin Schulze  <joey@finlandia.infodrom.north.de>

	* mkdosfs.c: Added patch from Sven Rudolph <sr1@inf.tu-dresden.de>
 	to support creation on disk images as well.

Tue Jan  7 12:04:21 1997  Martin Schulze  <joey@finlandia.infodrom.north.de>

	* Moved into section 'otherfs'

-- Released 1.0-4

Sun Dec 22 11:28:03 1996  Martin Schulze  <joey@finlandia.infodrom.north.de>

	* debian.rules: Installed ChangeLog

Wed Aug  7 19:07:15 1996  Martin Schulze  <joey@finlandia.infodrom.north.de>

	* Corrected debian.rules to provide $(package)_$(revision).diff.gz

Mon Aug  5 11:13:34 1996  Martin Schulze  <joey@finlandia.infodrom.north.de>

	* Added Conflicts: and Provides: entries in control file to really
	replace mkdosfs.  Thanks to Michael Meskes for reporting this bug.

Thu Jul 18 22:01:34 1996  Martin Schulze  <joey@finlandia.infodrom.north.de>

	* added dosfscheck to the package

Wed Jul  3 00:57:23 1996  Martin Schulze  <joey@finlandia.infodrom.north.de>

	* Added debian specific files

	* mkdosfs.c: inserted some brackets to stop gcc from moaning

	* mkdosfs.8: corrected some bold/inverse text phrases

	* mkdosfs.c: initialized a variable to stop gcc from moaning
