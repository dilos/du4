Source: libiksemel
Section: libs
Priority: optional
Maintainer: Dmitry Smirnov <onlyjob@debian.org>
Build-Depends: debhelper (>= 9), dh-autoreconf, pkg-config, libgnutls28-dev, texinfo
Standards-Version: 3.9.6
Homepage: https://github.com/meduketto/iksemel
Vcs-Browser: http://anonscm.debian.org/cgit/collab-maint/libiksemel.git
Vcs-Git: git://anonscm.debian.org/collab-maint/libiksemel.git

Package: libiksemel-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libiksemel3 (= ${binary:Version}), libc6-dev | libc-dev, ${misc:Depends}
Description: C library for the Jabber IM platform - development files
 iksemel handles Jabber connections, parses XML, and sends and
 receives Jabber messages. It works pretty good for parsing other
 kinds of XML, too, if the need arises.
 .
 This package provides headers, static linked library and info
 documentation.

Package: libiksemel3
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: C library for the Jabber IM platform
 iksemel handles Jabber connections, parses XML, and sends and
 receives Jabber messages. It works pretty good for parsing other
 kinds of XML, too, if the need arises.

Package: libiksemel-utils
Section: utils
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: utilities from the iksemel library
 iksemel handles Jabber connections, parses XML, and sends and
 receives Jabber messages. It works pretty good for parsing other
 kinds of XML, too, if the need arises.
 .
 This package includes three utilitaries from the library: ikslint,
 which checks xml files for well-formedness, iksperf, which tests
 speed and memory usage, and, finally, iksroster, which backups your
 roster.
