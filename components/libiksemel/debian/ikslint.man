.\"This man page is licensed under GPL version 2 or any later version
.\"published by the Free Software Foundation.
.TH IKSLINT 1 2008-01-14

.SH NAME
ikslint - checks for well-formedness of an XML document

.SH SYNOPSIS
ikslint [OPTIONS] FILE

.SH DESCRIPTION
Checks for well-formedness of the XML document contained in FILE.

.SH OPTIONS

.TP
\fB\-s\fR, \fB\-\-stats\fR
Print statistics

.TP
\fB\-t\fR, \fB\-\-histogram\fR
Print tag histogram

.TP
\fB\-h\fR, \fB\-\-help\fR
Print help text and exit

.TP
\fB\-V\fR, \fB\-\-version\fR
Print version and exit

.SH EXIT STATUS
Returns 0 if document is well-formed, and 1 otherwise.

.SH SEE ALSO
.BR iksperf (1)
.BR iksroster (1)
