.\"This man page is licensed under GPL version 2 or any later version
.\"published by the Free Software Foundation.
.TH IKSROSTER 1 2008-01-14

.SH NAME
iksroster - a backup tool for your jabber roster

.SH SYNOPSIS
iksroster [OPTIONS]

.SH DESCRIPTION
Downloads and uploads your jabber roster to/from a file.

.SH OPTIONS

.TP
\fB\-b\fR, \fB\-\-backup\fR=\fIJID\fR
Download roster from the server

.TP
\fB\-r\fR, \fB\-\-restore\fR=\fIJID\fR
Upload roster to the server

.TP
\fB\-f\fR, \fB\-\-file\fR=\fIFILE\fR
Load/Save roster to this file

.TP
\fB\-t\fR, \fB\-\-timeout\fR=\fISECS\fR
Set network timeout

.TP
\fB\-s\fR, \fB\-\-secure\fR
Use encrypted connection

.TP
\fB\-a\fR, \fB\-\-sasl\fR
Use SASL authentication

.TP
\fB\-l\fR, \fB\-\-log\fR
Print exchanged XML data

.TP
\fB\-h\fR, \fB\-\-help\fR
Print help text and exit

.TP
\fB\-v\fR, \fB\-\-version\fR
Print version and exit

.SH SEE ALSO
.BR ikslint (1)
.BR iksperf (1)
