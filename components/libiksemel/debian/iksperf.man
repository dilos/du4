.\"This man page is licensed under GPL version 2 or any later version
.\"published by the Free Software Foundation.
.TH IKSPERF 1 2008-01-14

.SH NAME
iksperf - measures the performance of the iksemel library

.SH SYNOPSIS
iksperf [OPTIONS] FILE

.SH DESCRIPTION
Measures the performance of the iksemel library when parsing FILE.

.SH OPTIONS

.TP
\fB\-a\fR, \fB\-\-all\fR
Make all tests

.TP
\fB\-s\fR, \fB\-\-sax\fR
Sax test

.TP
\fB\-d\fR, \fB\-\-dom\fR
Tree generating test

.TP
\fB\-e\fR, \fB\-\-serialize\fR
Tree serializing test

.TP
\fB\-1\fR, \fB\-\-sha1\fR
SHA1 hashing test

.TP
\fB\-b\fR, \fB\-\-block\fR \fISIZE\fR
Parse the file in \fISIZE\fR byte blocks

.TP
\fB\-m\fR, \fB\-\-memdbg\fR
Trace malloc and free calls

.TP
\fB\-h\fR, \fB\-\-help\fR
Print help text and exit

.TP
\fB\-V\fR, \fB\-\-version\fR
Print version and exit

.SH SEE ALSO
.BR ikslint (1)
.BR iksroster (1)
