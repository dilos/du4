Subject: [PATCH 3/3] Support the PGO build for binutils+gdb

Add the --enable-pgo-build[=lto] configure option.  When binutils+gdb
is not built together with GCC, --enable-pgo-build enables the PGO build:

1. First build with -fprofile-generate.
2. Use "make maybe-check-*" to generate profiling data.
3. Use "make clean" to remove the previous build.
4. Rebuild with -fprofile-use.

With --enable-pgo-build=lto, -flto=jobserver is used together with
-fprofile-generate and -fprofile-use.  Add '+' to the command line for
recursive make to support -flto=jobserver.

NB: --enable-pgo-build=lto enables the PGO build with LTO while
--enable-lto enables LTO support in toolchain.

	PR binutils/26766
	* Makefile.tpl (BUILD_CFLAGS): New.
	(CFLAGS): Append $(BUILD_CFLAGS).
	(CXXFLAGS): Likewise.
	(PGO_BUILD_GEN_FLAGS_TO_PASS): New.
	(PGO_BUILD_TRAINING_CFLAGS): Likewise.
	(PGO_BUILD_TRAINING_CXXFLAGS): Likewise.
	(PGO_BUILD_TRAINING_FLAGS_TO_PASS): Likewise.
	(PGO_BUILD_USE_FLAGS_TO_PASS): Likewise.
	(PGO-TRAINING-TARGETS): Likewise.
	(PGO_BUILD_TRAINING): Likewise.
	(all): Add '+' to the command line for recursive make.  Support
	the PGO build.
	* configure.ac: Add --enable-pgo-build[=lto].
	AC_SUBST PGO_BUILD_GEN_CFLAGS, PGO_BUILD_USE_CFLAGS and
	PGO_BUILD_LTO_CFLAGS.  Enable the PGO build in Makefile.
	* Makefile.in: Regenerated.
	* configure: Likewise.
---
 Makefile.in  |  58 ++++++++++++++++++++++++++++--
 Makefile.tpl |  58 ++++++++++++++++++++++++++++--
 configure    | 100 +++++++++++++++++++++++++++++++++++++++++++++++++--
 configure.ac |  65 +++++++++++++++++++++++++++++++++
 4 files changed, 275 insertions(+), 6 deletions(-)

--- a/Makefile.in
+++ b/Makefile.in
@@ -412,6 +412,45 @@ CXXFLAGS = @CXXFLAGS@
 LIBCXXFLAGS = $(CXXFLAGS) -fno-implicit-templates
 GOCFLAGS = $(CFLAGS)
 
+# Pass additional PGO and LTO compiler options to the PGO build.
+BUILD_CFLAGS = $(PGO_BUILD_CFLAGS) $(PGO_BUILD_LTO_CFLAGS)
+override CFLAGS += $(BUILD_CFLAGS)
+override CXXFLAGS += $(BUILD_CFLAGS)
+
+# Additional PGO and LTO compiler options to generate profiling data
+# for the PGO build.
+PGO_BUILD_GEN_FLAGS_TO_PASS = \
+	PGO_BUILD_CFLAGS="@PGO_BUILD_GEN_CFLAGS@" \
+	PGO_BUILD_LTO_CFLAGS="@PGO_BUILD_LTO_CFLAGS@"
+
+# NB: Filter out any compiler options which may fail PGO training runs.
+PGO_BUILD_TRAINING_CFLAGS:= \
+	$(filter-out -Werror=%,$(CFLAGS))
+PGO_BUILD_TRAINING_CXXFLAGS:=\
+	$(filter-out -Werror=%,$(CXXFLAGS))
+PGO_BUILD_TRAINING_CFLAGS:= \
+	$(filter-out -Wall,$(PGO_BUILD_TRAINING_CFLAGS))
+PGO_BUILD_TRAINING_CXXFLAGS:= \
+	$(filter-out -Wall,$(PGO_BUILD_TRAINING_CXXFLAGS))
+PGO_BUILD_TRAINING_CFLAGS:= \
+	$(filter-out -specs=%,$(PGO_BUILD_TRAINING_CFLAGS))
+PGO_BUILD_TRAINING_CXXFLAGS:= \
+	$(filter-out -specs=%,$(PGO_BUILD_TRAINING_CXXFLAGS))
+PGO_BUILD_TRAINING_FLAGS_TO_PASS = \
+	CFLAGS_FOR_TARGET="$(PGO_BUILD_TRAINING_CFLAGS)" \
+	CXXFLAGS_FOR_TARGET="$(PGO_BUILD_TRAINING_CXXFLAGS)"
+
+# Additional PGO and LTO compiler options to use profiling data for the
+# PGO build.
+PGO_BUILD_USE_FLAGS_TO_PASS = \
+	PGO_BUILD_CFLAGS="@PGO_BUILD_USE_CFLAGS@" \
+	PGO_BUILD_LTO_CFLAGS="@PGO_BUILD_LTO_CFLAGS@"
+
+# PGO training targets for the PGO build.  FIXME: Add gold tests to
+# training.
+PGO-TRAINING-TARGETS = binutils gas gdb ld sim
+PGO_BUILD_TRAINING = $(addprefix maybe-check-,$(PGO-TRAINING-TARGETS))
+
 CREATE_GCOV = create_gcov
 
 TFLAGS =
@@ -985,6 +1024,12 @@ configure-target:  \
 
 # The target built for a native non-bootstrap build.
 .PHONY: all
+
+# --enable-pgo-build enables the PGO build.
+# 1. First build with -fprofile-generate.
+# 2. Use "make maybe-check-*" to generate profiling data.
+# 3. Use "make clean" to remove the previous build.
+# 4. Rebuild with -fprofile-use.
 all:
 @if gcc-bootstrap
 	[ -f stage_final ] || echo stage3 > stage_final
@@ -993,7 +1038,7 @@ all:
 	$(MAKE) $(RECURSE_FLAGS_TO_PASS) `cat stage_final`-bubble
 @endif gcc-bootstrap
 	@: $(MAKE); $(unstage)
-	@r=`${PWD_COMMAND}`; export r; \
+	+@r=`${PWD_COMMAND}`; export r; \
 	s=`cd $(srcdir); ${PWD_COMMAND}`; export s; \
 @if gcc-bootstrap
 	if [ -f stage_last ]; then \
@@ -1001,7 +1046,16 @@ all:
 	  $(MAKE) $(TARGET_FLAGS_TO_PASS) all-host all-target; \
 	else \
 @endif gcc-bootstrap
-	  $(MAKE) $(RECURSE_FLAGS_TO_PASS) all-host all-target \
+	  $(MAKE) $(RECURSE_FLAGS_TO_PASS) \
+		$(PGO_BUILD_GEN_FLAGS_TO_PASS) all-host all-target \
+@if pgo-build
+	&& $(MAKE) $(RECURSE_FLAGS_TO_PASS) \
+		$(PGO_BUILD_TRAINING_FLAGS_TO_PASS) \
+		$(PGO_BUILD_TRAINING) \
+	&& $(MAKE) $(RECURSE_FLAGS_TO_PASS) clean \
+	&& $(MAKE) $(RECURSE_FLAGS_TO_PASS) \
+		$(PGO_BUILD_USE_FLAGS_TO_PASS) all-host all-target \
+@endif pgo-build
 @if gcc-bootstrap
 	    ; \
 	fi \
--- a/Makefile.tpl
+++ b/Makefile.tpl
@@ -415,6 +415,45 @@ CXXFLAGS = @CXXFLAGS@
 LIBCXXFLAGS = $(CXXFLAGS) -fno-implicit-templates
 GOCFLAGS = $(CFLAGS)
 
+# Pass additional PGO and LTO compiler options to the PGO build.
+BUILD_CFLAGS = $(PGO_BUILD_CFLAGS) $(PGO_BUILD_LTO_CFLAGS)
+override CFLAGS += $(BUILD_CFLAGS)
+override CXXFLAGS += $(BUILD_CFLAGS)
+
+# Additional PGO and LTO compiler options to generate profiling data
+# for the PGO build.
+PGO_BUILD_GEN_FLAGS_TO_PASS = \
+	PGO_BUILD_CFLAGS="@PGO_BUILD_GEN_CFLAGS@" \
+	PGO_BUILD_LTO_CFLAGS="@PGO_BUILD_LTO_CFLAGS@"
+
+# NB: Filter out any compiler options which may fail PGO training runs.
+PGO_BUILD_TRAINING_CFLAGS:= \
+	$(filter-out -Werror=%,$(CFLAGS))
+PGO_BUILD_TRAINING_CXXFLAGS:=\
+	$(filter-out -Werror=%,$(CXXFLAGS))
+PGO_BUILD_TRAINING_CFLAGS:= \
+	$(filter-out -Wall,$(PGO_BUILD_TRAINING_CFLAGS))
+PGO_BUILD_TRAINING_CXXFLAGS:= \
+	$(filter-out -Wall,$(PGO_BUILD_TRAINING_CXXFLAGS))
+PGO_BUILD_TRAINING_CFLAGS:= \
+	$(filter-out -specs=%,$(PGO_BUILD_TRAINING_CFLAGS))
+PGO_BUILD_TRAINING_CXXFLAGS:= \
+	$(filter-out -specs=%,$(PGO_BUILD_TRAINING_CXXFLAGS))
+PGO_BUILD_TRAINING_FLAGS_TO_PASS = \
+	CFLAGS_FOR_TARGET="$(PGO_BUILD_TRAINING_CFLAGS)" \
+	CXXFLAGS_FOR_TARGET="$(PGO_BUILD_TRAINING_CXXFLAGS)"
+
+# Additional PGO and LTO compiler options to use profiling data for the
+# PGO build.
+PGO_BUILD_USE_FLAGS_TO_PASS = \
+	PGO_BUILD_CFLAGS="@PGO_BUILD_USE_CFLAGS@" \
+	PGO_BUILD_LTO_CFLAGS="@PGO_BUILD_LTO_CFLAGS@"
+
+# PGO training targets for the PGO build.  FIXME: Add gold tests to
+# training.
+PGO-TRAINING-TARGETS = binutils gas gdb ld sim
+PGO_BUILD_TRAINING = $(addprefix maybe-check-,$(PGO-TRAINING-TARGETS))
+
 CREATE_GCOV = create_gcov
 
 TFLAGS =
@@ -719,6 +758,12 @@ configure-target: [+
 
 # The target built for a native non-bootstrap build.
 .PHONY: all
+
+# --enable-pgo-build enables the PGO build.
+# 1. First build with -fprofile-generate.
+# 2. Use "make maybe-check-*" to generate profiling data.
+# 3. Use "make clean" to remove the previous build.
+# 4. Rebuild with -fprofile-use.
 all:
 @if gcc-bootstrap
 	[ -f stage_final ] || echo stage3 > stage_final
@@ -727,7 +772,7 @@ all:
 	$(MAKE) $(RECURSE_FLAGS_TO_PASS) `cat stage_final`-bubble
 @endif gcc-bootstrap
 	@: $(MAKE); $(unstage)
-	@r=`${PWD_COMMAND}`; export r; \
+	+@r=`${PWD_COMMAND}`; export r; \
 	s=`cd $(srcdir); ${PWD_COMMAND}`; export s; \
 @if gcc-bootstrap
 	if [ -f stage_last ]; then \
@@ -735,7 +780,16 @@ all:
 	  $(MAKE) $(TARGET_FLAGS_TO_PASS) all-host all-target; \
 	else \
 @endif gcc-bootstrap
-	  $(MAKE) $(RECURSE_FLAGS_TO_PASS) all-host all-target \
+	  $(MAKE) $(RECURSE_FLAGS_TO_PASS) \
+		$(PGO_BUILD_GEN_FLAGS_TO_PASS) all-host all-target \
+@if pgo-build
+	&& $(MAKE) $(RECURSE_FLAGS_TO_PASS) \
+		$(PGO_BUILD_TRAINING_FLAGS_TO_PASS) \
+		$(PGO_BUILD_TRAINING) \
+	&& $(MAKE) $(RECURSE_FLAGS_TO_PASS) clean \
+	&& $(MAKE) $(RECURSE_FLAGS_TO_PASS) \
+		$(PGO_BUILD_USE_FLAGS_TO_PASS) all-host all-target \
+@endif pgo-build
 @if gcc-bootstrap
 	    ; \
 	fi \
--- a/configure
+++ b/configure
@@ -693,6 +693,9 @@ extra_mpfr_configure_flags
 gmpinc
 gmplibs
 LIBDEBUGINFOD
+PGO_BUILD_LTO_CFLAGS
+PGO_BUILD_USE_CFLAGS
+PGO_BUILD_GEN_CFLAGS
 do_compare
 GNATMAKE
 GNATBIND
@@ -753,6 +756,7 @@ infodir
 docdir
 oldincludedir
 includedir
+runstatedir
 localstatedir
 sharedstatedir
 sysconfdir
@@ -793,6 +797,7 @@ enable_libssp
 enable_libstdcxx
 enable_liboffloadmic
 enable_bootstrap
+enable_pgo_build
 with_debuginfod
 with_mpc
 with_mpc_include
@@ -917,6 +922,7 @@ datadir='${datarootdir}'
 sysconfdir='${prefix}/etc'
 sharedstatedir='${prefix}/com'
 localstatedir='${prefix}/var'
+runstatedir='${localstatedir}/run'
 includedir='${prefix}/include'
 oldincludedir='/usr/include'
 docdir='${datarootdir}/doc/${PACKAGE}'
@@ -1169,6 +1175,15 @@ do
   | -silent | --silent | --silen | --sile | --sil)
     silent=yes ;;
 
+  -runstatedir | --runstatedir | --runstatedi | --runstated \
+  | --runstate | --runstat | --runsta | --runst | --runs \
+  | --run | --ru | --r)
+    ac_prev=runstatedir ;;
+  -runstatedir=* | --runstatedir=* | --runstatedi=* | --runstated=* \
+  | --runstate=* | --runstat=* | --runsta=* | --runst=* | --runs=* \
+  | --run=* | --ru=* | --r=*)
+    runstatedir=$ac_optarg ;;
+
   -sbindir | --sbindir | --sbindi | --sbind | --sbin | --sbi | --sb)
     ac_prev=sbindir ;;
   -sbindir=* | --sbindir=* | --sbindi=* | --sbind=* | --sbin=* \
@@ -1306,7 +1321,7 @@ fi
 for ac_var in	exec_prefix prefix bindir sbindir libexecdir datarootdir \
 		datadir sysconfdir sharedstatedir localstatedir includedir \
 		oldincludedir docdir infodir htmldir dvidir pdfdir psdir \
-		libdir localedir mandir
+		libdir localedir mandir runstatedir
 do
   eval ac_val=\$$ac_var
   # Remove trailing slashes.
@@ -1466,6 +1481,7 @@ Fine tuning of the installation director
   --sysconfdir=DIR        read-only single-machine data [PREFIX/etc]
   --sharedstatedir=DIR    modifiable architecture-independent data [PREFIX/com]
   --localstatedir=DIR     modifiable single-machine data [PREFIX/var]
+  --runstatedir=DIR       modifiable per-process data [LOCALSTATEDIR/run]
   --libdir=DIR            object code libraries [EPREFIX/lib]
   --includedir=DIR        C header files [PREFIX/include]
   --oldincludedir=DIR     C header files for non-gcc [/usr/include]
@@ -1524,6 +1540,8 @@ Optional Features:
   --enable-liboffloadmic=ARG
                           build liboffloadmic [ARG={no,host,target}]
   --enable-bootstrap      enable bootstrapping [yes if native build]
+  --enable-pgo-build[=lto]
+                          enable the PGO build
   --disable-isl-version-check
                           disable check for isl version
   --enable-lto            enable link time optimization support
@@ -2833,7 +2851,7 @@ host_libs="intl libiberty opcodes bfd re
 # binutils, gas and ld appear in that order because it makes sense to run
 # "make check" in that particular order.
 # If --enable-gold is used, "gold" may replace "ld".
-host_tools="texinfo flex bison binutils gas ld fixincludes gcc cgen sid sim gdb gdbserver gprof etc expect dejagnu m4 utils guile fastjar gnattools libcc1 gotools"
+host_tools="flex bison binutils gas ld fixincludes gcc cgen sid sim gdb gdbserver gprof etc expect dejagnu m4 utils guile fastjar gnattools libcc1 gotools"
 
 # these libraries are built for the target environment, and are built after
 # the host libraries and the host tools (which may be a cross compiler)
@@ -5472,6 +5490,91 @@ if test "$enable_bootstrap:$GXX" = "yes:
   CXX="$CXX -std=gnu++98"
 fi
 
+# Check whether --enable-pgo-build was given.
+if test "${enable_pgo_build+set}" = set; then :
+  enableval=$enable_pgo_build; enable_pgo_build=$enableval
+else
+  enable_pgo_build=no
+fi
+
+
+# Issue errors and warnings for invalid/strange PGO build combinations.
+case "$have_compiler:$host:$target:$enable_pgo_build" in
+  *:*:*:no) ;;
+
+  # Allow the PGO build only if we aren't building a compiler and
+  # we are in a native configuration.
+  no:$build:$build:yes | no:$build:$build:lto) ;;
+
+  # Disallow the PGO bootstrap if we are building a compiler.
+  yes:*:*:yes | yes:*:*:lto)
+    as_fn_error $? "cannot perform the PGO bootstrap when building a compiler" "$LINENO" 5 ;;
+
+  *)
+    as_fn_error $? "invalid option for --enable-pgo-build" "$LINENO" 5
+    ;;
+esac
+
+if test "$enable_pgo_build" != "no"; then
+  { $as_echo "$as_me:${as_lineno-$LINENO}: checking whether the compiler supports -fprofile-generate" >&5
+$as_echo_n "checking whether the compiler supports -fprofile-generate... " >&6; }
+  old_CFLAGS="$CFLAGS"
+  PGO_BUILD_GEN_CFLAGS="-fprofile-generate"
+  CFLAGS="$CFLAGS $PGO_BUILD_CFLAGS"
+
+cat confdefs.h - <<_ACEOF >conftest.$ac_ext
+/* end confdefs.h.  */
+int foo;
+_ACEOF
+if ac_fn_c_try_compile "$LINENO"; then :
+
+else
+  PGO_BUILD_GEN_CFLAGS=
+fi
+rm -f core conftest.err conftest.$ac_objext conftest.$ac_ext
+  CFLAGS="$old_CFLAGS"
+  if test -n "$PGO_BUILD_GEN_CFLAGS"; then
+    { $as_echo "$as_me:${as_lineno-$LINENO}: result: yes" >&5
+$as_echo "yes" >&6; }
+    PGO_BUILD_USE_CFLAGS="-fprofile-use"
+  else
+    { $as_echo "$as_me:${as_lineno-$LINENO}: result: no" >&5
+$as_echo "no" >&6; }
+    as_fn_error $? "cannot perform the PGO build without -fprofile-generate" "$LINENO" 5
+  fi
+
+  if test "$enable_pgo_build" = "lto"; then
+    { $as_echo "$as_me:${as_lineno-$LINENO}: checking whether the compiler supports -flto=jobserver" >&5
+$as_echo_n "checking whether the compiler supports -flto=jobserver... " >&6; }
+    old_CFLAGS="$CFLAGS"
+    PGO_BUILD_LTO_CFLAGS="-flto=jobserver"
+    CFLAGS="$CFLAGS $PGO_BUILD_LTO_CFLAGS"
+    cat confdefs.h - <<_ACEOF >conftest.$ac_ext
+/* end confdefs.h.  */
+int foo;
+_ACEOF
+if ac_fn_c_try_compile "$LINENO"; then :
+
+else
+  PGO_BUILD_LTO_CFLAGS=
+fi
+rm -f core conftest.err conftest.$ac_objext conftest.$ac_ext
+    CFLAGS="$old_CFLAGS"
+    if test -n "$PGO_BUILD_LTO_CFLAGS"; then
+      { $as_echo "$as_me:${as_lineno-$LINENO}: result: yes" >&5
+$as_echo "yes" >&6; }
+    else
+      { $as_echo "$as_me:${as_lineno-$LINENO}: result: no" >&5
+$as_echo "no" >&6; }
+      { $as_echo "$as_me:${as_lineno-$LINENO}: WARNING: LTO is disabled for the PGO build" >&5
+$as_echo "$as_me: WARNING: LTO is disabled for the PGO build" >&2;}
+    fi
+  fi
+fi
+
+
+
+
 # Used for setting $lt_cv_objdir
 { $as_echo "$as_me:${as_lineno-$LINENO}: checking for objdir" >&5
 $as_echo_n "checking for objdir... " >&6; }
@@ -5509,7 +5612,6 @@ have_gmp=no
 
 # Check for debuginfod
 
-
 # Enable debuginfod
 
 # Check whether --with-debuginfod was given.
@@ -7466,6 +7568,12 @@ extrasub_target="$extrasub_target
 /^@unless /d
 /^@endunless /d"
 
+if test "$enable_pgo_build" != "no"; then
+  extrasub_build="$extrasub_build
+/^@if pgo-build\$/d
+/^@endif pgo-build\$/d"
+fi
+
 # Create the serialization dependencies.  This uses a temporary file.
 
 # Check whether --enable-serial-configure was given.
--- a/configure.ac
+++ b/configure.ac
@@ -1376,6 +1376,65 @@ if test "$enable_bootstrap:$GXX" = "yes:
   CXX="$CXX -std=gnu++98"
 fi
 
+AC_ARG_ENABLE([pgo-build],
+[AS_HELP_STRING([--enable-pgo-build[[=lto]]],
+		[enable the PGO build])],
+[enable_pgo_build=$enableval],
+[enable_pgo_build=no])
+
+# Issue errors and warnings for invalid/strange PGO build combinations.
+case "$have_compiler:$host:$target:$enable_pgo_build" in
+  *:*:*:no) ;;
+
+  # Allow the PGO build only if we aren't building a compiler and
+  # we are in a native configuration.
+  no:$build:$build:yes | no:$build:$build:lto) ;;
+
+  # Disallow the PGO bootstrap if we are building a compiler.
+  yes:*:*:yes | yes:*:*:lto)
+    AC_MSG_ERROR([cannot perform the PGO bootstrap when building a compiler]) ;;
+
+  *)
+    AC_MSG_ERROR([invalid option for --enable-pgo-build])
+    ;;
+esac
+
+if test "$enable_pgo_build" != "no"; then
+  AC_MSG_CHECKING([whether the compiler supports -fprofile-generate])
+  old_CFLAGS="$CFLAGS"
+  PGO_BUILD_GEN_CFLAGS="-fprofile-generate"
+  CFLAGS="$CFLAGS $PGO_BUILD_CFLAGS"
+  AC_COMPILE_IFELSE([AC_LANG_SOURCE([int foo;])],,
+		    [PGO_BUILD_GEN_CFLAGS=])
+  CFLAGS="$old_CFLAGS"
+  if test -n "$PGO_BUILD_GEN_CFLAGS"; then
+    AC_MSG_RESULT([yes])
+    PGO_BUILD_USE_CFLAGS="-fprofile-use"
+  else
+    AC_MSG_RESULT([no])
+    AC_MSG_ERROR([cannot perform the PGO build without -fprofile-generate])
+  fi
+
+  if test "$enable_pgo_build" = "lto"; then
+    AC_MSG_CHECKING([whether the compiler supports -flto=jobserver])
+    old_CFLAGS="$CFLAGS"
+    PGO_BUILD_LTO_CFLAGS="-flto=jobserver"
+    CFLAGS="$CFLAGS $PGO_BUILD_LTO_CFLAGS"
+    AC_COMPILE_IFELSE([AC_LANG_SOURCE([int foo;])],,
+		      [PGO_BUILD_LTO_CFLAGS=])
+    CFLAGS="$old_CFLAGS"
+    if test -n "$PGO_BUILD_LTO_CFLAGS"; then
+      AC_MSG_RESULT([yes])
+    else
+      AC_MSG_RESULT([no])
+      AC_MSG_WARN([LTO is disabled for the PGO build])
+    fi
+  fi
+fi
+AC_SUBST(PGO_BUILD_GEN_CFLAGS)
+AC_SUBST(PGO_BUILD_USE_CFLAGS)
+AC_SUBST(PGO_BUILD_LTO_CFLAGS)
+
 # Used for setting $lt_cv_objdir
 _LT_CHECK_OBJDIR
 
@@ -2851,6 +2910,12 @@ extrasub_target="$extrasub_target
 /^@unless /d
 /^@endunless /d"
 
+if test "$enable_pgo_build" != "no"; then
+  extrasub_build="$extrasub_build
+/^@if pgo-build\$/d
+/^@endif pgo-build\$/d"
+fi
+
 # Create the serialization dependencies.  This uses a temporary file.
 
 AC_ARG_ENABLE([serial-configure],
