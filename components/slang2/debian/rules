#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)
export DEB_HOST_MULTIARCH

# Add -O0 to workaround �675125, a bug that appeared with gcc-4.7.
# Probably to do with uninitialized memory somewhere ?
# "src/tests/sltest ./utf8" triggers the bug; works fine in UTF-8 mode, fails otherwise.
# - amck, 2012-06-30
CPPFLAGS+= -D__EXTENSIONS__
CFLAGS:= $(CFLAGS) -D_XOPEN_SOURCE=500 $(CPPFLAGS) -fsigned-char

# #890545:
OBJDIR:=src/objs
ifeq ($(ARCH), x32)
  OBJDIR:=src/x32objs
endif

DEB_HOST_ARCH ?= $(shell dpkg-architecture -qDEB_HOST_ARCH)
CFLAGS += -g -gdwarf-2 -fno-omit-frame-pointer
ifeq (,$(filter-out solaris-i386 dilos-amd64 argoos-amd64,$(DEB_HOST_ARCH)))
CFLAGS += -msave-args
endif

# Magic debhelper rule
%:
	dh $@ --no-parallel

override_dh_auto_configure:
	cd autoconf \
        && autoconf \
        && mv configure ..
	dh_auto_configure -- --libdir=\$${prefix}/lib/${DEB_HOST_MULTIARCH}

override_dh_auto_build:
	dh_auto_build
	${MAKE} -C src static
	ar cqv libslang_pic.a `LC_ALL=C ls src/elfobjs/*.o`
	INSTANT_OPT=" " docbook-to-man debian/slsh.sgml > slsh.1

override_dh_auto_clean:
	dh_auto_clean
	rm -f slsh.1 libslang_pic.a src/test/sltest

override_dh_auto_install:
	dh_auto_install --no-parallel
	cp  /usr/share/unicode/EastAsianWidth.txt utf8/tools
	cp /usr/share/unicode/UnicodeData.txt utf8/tools

	mkdir -p debian/libslang2-pic/usr/lib/${DEB_HOST_MULTIARCH}
	cp src/slang.ver debian/libslang2-pic/usr/lib/${DEB_HOST_MULTIARCH}/libslang_pic.map

	mkdir -p debian/tmp/usr/lib/${DEB_HOST_MULTIARCH}
	cp -a ${OBJDIR}/libslang.a debian/tmp/usr/lib/${DEB_HOST_MULTIARCH}
	ln -sf /usr/lib/${DEB_HOST_MULTIARCH}/libslang.so.2 debian/tmp/usr/lib/${DEB_HOST_MULTIARCH}/libslang.so
#	chrpath -d debian/tmp/usr/lib/${DEB_HOST_MULTIARCH}/slang/v2/modules/*.so
#	chrpath -d debian/tmp/usr/bin/slsh
	mkdir -p debian/libslang2-modules/usr/lib/${DEB_HOST_MULTIARCH}/slang/v2/modules
	mv debian/tmp/usr/lib/*/slang/v2/modules/* debian/libslang2-modules/usr/lib/${DEB_HOST_MULTIARCH}/slang/v2/modules

override_dh_installchangelogs:
	dh_installchangelogs -a changes.txt

override_dh_makeshlibs:
	dh_makeshlibs -a -V #--add-udeb="libslang2-udeb"

override_dh_gencontrol:
	dh_gencontrol -- -VBuilt-Using="`dpkg-query -f'$${source:Package} (= $${source:Version})' -W unicode-data`"

override_dh_compress:
	dh_compress -a -X.sl -X.c -X.h
