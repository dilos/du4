# translation of exim4_debian_po_mk.po to Macedonian
# translation of exim_exim_debian_po_mk.po to
# translation of exim_exim_debian_po_mk.po to
# translation of exim_exim_debian_po_mk.po to
#
#    Translators, if you are not familiar with the PO format, gettext
#    documentation is worth reading, especially sections dedicated to
#    this format, e.g. by running:
#         info -n '(gettext)PO Files'
#         info -n '(gettext)Header Entry'
#    Some information specific to po-debconf are available at
#            /usr/share/doc/po-debconf/README-trans
#         or http://www.debian.org/intl/l10n/po-debconf/README-trans#
#    Developers do not need to manually edit POT or PO files.
# Georgi Stanojevski <glisha@gmail.com>, 2004.
# Georgi Stanojevski <gogo@stavre>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: exim4_debian_po_mk\n"
"Report-Msgid-Bugs-To: pkg-exim4-maintainers@lists.alioth.debian.org\n"
"POT-Creation-Date: 2007-07-18 08:29+0200\n"
"PO-Revision-Date: 2005-07-26 21:11+0200\n"
"Last-Translator: Georgi Stanojevski <gogo@stavre>\n"
"Language-Team: Macedonian <ossm-members@hedona.on.net.mk>\n"
"Language: mk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.9.1\n"

#. Type: boolean
#. Description
#: ../exim4-base.templates:1001
#, fuzzy
msgid "Remove undelivered messages in spool directory?"
msgstr ""
"Отстрани ги пораките кои не се доставени а сеуште се во директорумот за "
"праќање?"

#. Type: boolean
#. Description
#: ../exim4-base.templates:1001
#, fuzzy
msgid ""
"There are e-mail messages in the Exim spool directory /var/spool/exim4/"
"input/ which have not yet been delivered. Removing Exim will cause them to "
"remain undelivered until Exim is re-installed."
msgstr ""
"Има пораки во директорумот за праќање на exim /var/spool/exim4/input кои "
"сеуште не биле испратени. Може да ги задржиш во случај да одлучиш да го "
"инсталираш Exim подоцна или можеш да ги избришеш."

#. Type: boolean
#. Description
#: ../exim4-base.templates:1001
msgid ""
"If this option is not chosen, the spool directory is kept, allowing the "
"messages in the queue to be delivered at a later date after Exim is re-"
"installed."
msgstr ""

#. Type: error
#. Description
#: ../exim4-base.templates:2001 ../exim4-daemon-heavy.templates:1001
#: ../exim4-daemon-light.templates:1001 ../exim4.templates:1001
msgid "Reconfigure exim4-config instead of this package"
msgstr ""

#. Type: error
#. Description
#: ../exim4-base.templates:2001 ../exim4-daemon-heavy.templates:1001
#: ../exim4-daemon-light.templates:1001 ../exim4.templates:1001
msgid ""
"Exim4 has its configuration factored out into a dedicated package, exim4-"
"config. To reconfigure Exim4, use 'dpkg-reconfigure exim4-config'."
msgstr ""

#. Type: select
#. Choices
#. Translators beware! the following six strings form a single
#. Choices menu. - Every one of these strings has to fit in a standard
#. 80 characters console, as the fancy screen setup takes up some space
#. try to keep below ~71 characters.
#. DO NOT USE commas (,) in Choices translations otherwise
#. this will break the choices shown to users
#: ../exim4-config.templates:1001
msgid "internet site; mail is sent and received directly using SMTP"
msgstr "интернет страна - поштата се праќа и прима преку СМТП"

#. Type: select
#. Choices
#. Translators beware! the following six strings form a single
#. Choices menu. - Every one of these strings has to fit in a standard
#. 80 characters console, as the fancy screen setup takes up some space
#. try to keep below ~71 characters.
#. DO NOT USE commas (,) in Choices translations otherwise
#. this will break the choices shown to users
#: ../exim4-config.templates:1001
msgid "mail sent by smarthost; received via SMTP or fetchmail"
msgstr "пошта пратена преку друг домаќин - примена преку СМТП или fetchmail"

#. Type: select
#. Choices
#. Translators beware! the following six strings form a single
#. Choices menu. - Every one of these strings has to fit in a standard
#. 80 characters console, as the fancy screen setup takes up some space
#. try to keep below ~71 characters.
#. DO NOT USE commas (,) in Choices translations otherwise
#. this will break the choices shown to users
#: ../exim4-config.templates:1001
msgid "mail sent by smarthost; no local mail"
msgstr "пошта пратена преку друг домаќин - нема локална пошта"

#. Type: select
#. Choices
#. Translators beware! the following six strings form a single
#. Choices menu. - Every one of these strings has to fit in a standard
#. 80 characters console, as the fancy screen setup takes up some space
#. try to keep below ~71 characters.
#. DO NOT USE commas (,) in Choices translations otherwise
#. this will break the choices shown to users
#: ../exim4-config.templates:1001
msgid "local delivery only; not on a network"
msgstr "само локално испорачување - не е на мрежа"

#. Type: select
#. Choices
#. Translators beware! the following six strings form a single
#. Choices menu. - Every one of these strings has to fit in a standard
#. 80 characters console, as the fancy screen setup takes up some space
#. try to keep below ~71 characters.
#. DO NOT USE commas (,) in Choices translations otherwise
#. this will break the choices shown to users
#: ../exim4-config.templates:1001
msgid "no configuration at this time"
msgstr "нема конфигурација во моментов"

#. Type: select
#. Description
#: ../exim4-config.templates:1002
msgid "General type of mail configuration:"
msgstr "Главен тип на конфигурација за пошта:"

#. Type: select
#. Description
#: ../exim4-config.templates:1002
#, fuzzy
msgid ""
"Please select the mail server configuration type that best meets your needs."
msgstr "Избери го типот на конфигурација кој најдобро ти одговара."

#. Type: select
#. Description
#: ../exim4-config.templates:1002
#, fuzzy
msgid ""
"Systems with dynamic IP addresses, including dialup systems, should "
"generally be configured to send outgoing mail to another machine, called a "
"'smarthost' for delivery because many receiving systems on the Internet "
"block incoming mail from dynamic IP addresses as spam protection."
msgstr ""
"Системите со динамички ИП адреси треба главно да бидат конфигурирани да ја "
"праќаат надворешната пошта преку друга машина. Може да обереш да примаш "
"пошта на таков систем или да немаш испорака на локална пошта, освен пошта за "
"root и за postmaster."

#. Type: select
#. Description
#: ../exim4-config.templates:1002
msgid ""
"A system with a dynamic IP address can receive its own mail, or local "
"delivery can be disabled entirely (except mail for root and postmaster)."
msgstr ""

#. Type: boolean
#. Description
#: ../exim4-config.templates:2001
msgid "Really leave the mail system unconfigured?"
msgstr "Навистина остави го системо за пошта неконфигуриран?"

#. Type: boolean
#. Description
#: ../exim4-config.templates:2001
#, fuzzy
msgid ""
"Until the mail system is configured, it will be broken and cannot be used. "
"Configuration at a later time can be done either by hand or by running 'dpkg-"
"reconfigure exim4-config' as root."
msgstr ""
"Се додека твојот систем за пошта не е конфигуриран, ќе биде расипан и не ќе "
"може да се користи. Се разбира подоцна можеш да го конфигурираш или рачно "
"или со извршување на „dpkg-reconfigure exim4-config“ како root."

#. Type: string
#. Description
#: ../exim4-config.templates:3001
msgid "System mail name:"
msgstr "Поштенско име:"

#. Type: string
#. Description
#: ../exim4-config.templates:3001
msgid ""
"The 'mail name' is the domain name used to 'qualify' mail addresses without "
"a domain name."
msgstr ""

#. Type: string
#. Description
#: ../exim4-config.templates:3001
#, fuzzy
msgid ""
"This name will also be used by other programs. It should be the single, "
"fully qualified domain name (FQDN)."
msgstr ""
"Името исто така ќе биде користено од други програми, треба да биде единечен "
"и правилен име на домејн (FQDN) од кој ќе изгледа дека поштата потекнува."

#. Type: string
#. Description
#: ../exim4-config.templates:3001
msgid ""
"Thus, if a mail address on the local host is foo@example.org, the correct "
"value for this option would be example.org."
msgstr ""

#. Type: string
#. Description
#: ../exim4-config.templates:3001
#, fuzzy
msgid ""
"This name won't appear on From: lines of outgoing messages if rewriting is "
"enabled."
msgstr ""
"Ова име нема да се прикажува во Од: линииите на поштата за надвор ако "
"овозможиш препишување."

#. Type: string
#. Description
#: ../exim4-config.templates:4001
msgid "Other destinations for which mail is accepted:"
msgstr "Други дестинации за кои поштата се прима:"

#. Type: string
#. Description
#: ../exim4-config.templates:4001
#, fuzzy
msgid ""
"Please enter a semicolon-separated list of recipient domains for which this "
"machine should consider itself the final destination. These domains are "
"commonly called 'local domains'. The local hostname (${fqdn}) and "
"'localhost' are always added to the list given here."
msgstr ""
"Те молам внеси листа на домејни за кои оваа машина треба да се смета како "
"крајна дестинација, освен локалната адреса (${fqdn}) и „localhost“."

#. Type: string
#. Description
#: ../exim4-config.templates:4001
#, fuzzy
msgid ""
"By default all local domains will be treated identically. If both a.example "
"and b.example are local domains, acc@a.example and acc@b.example will be "
"delivered to the same final destination. If different domain names should be "
"treated differently, it is necessary to edit the config files afterwards."
msgstr ""
"Предефинирано кон сите домејни се однесува исто, ако сакаш кон различни "
"домејни да се однесува различно, ќе треба да ја смениш конфигурациската "
"датотека потоа."

#. Type: string
#. Description
#: ../exim4-config.templates:5001
msgid "Domains to relay mail for:"
msgstr "Домејни за кои поштата се препраќа:"

#. Type: string
#. Description
#: ../exim4-config.templates:5001
msgid ""
"Please enter a semicolon-separated list of recipient domains for which this "
"system will relay mail, for example as a fallback MX or mail gateway. This "
"means that this system will accept mail for these domains from anywhere on "
"the Internet and deliver them according to local delivery rules."
msgstr ""

#. Type: string
#. Description
#: ../exim4-config.templates:5001
msgid "Do not mention local domains here. Wildcards may be used."
msgstr ""

#. Type: string
#. Description
#: ../exim4-config.templates:6001
msgid "Machines to relay mail for:"
msgstr "Машини за кои да препраќаш пошта:"

#. Type: string
#. Description
#: ../exim4-config.templates:6001
msgid ""
"Please enter a semicolon-separated list of IP address ranges for which this "
"system will unconditionally relay mail, functioning as a smarthost."
msgstr ""

#. Type: string
#. Description
#: ../exim4-config.templates:6001
#, fuzzy
msgid ""
"You should use the standard address/prefix format (e.g. 194.222.242.0/24 or "
"5f03:1200:836f::/48)."
msgstr ""
"Ако има, внеси ги тука одвоени со две точки. Треба да го користиш "
"стандардниот формат за адреси (пр. 192.222.242.0/24)."

#. Type: string
#. Description
#: ../exim4-config.templates:6001
msgid ""
"If this system should not be a smarthost for any other host, leave this list "
"blank."
msgstr ""

#. Type: string
#. Description
#: ../exim4-config.templates:7001
msgid "Visible domain name for local users:"
msgstr "Видливи имиња на домејни за локалните корисници:"

#. Type: string
#. Description
#: ../exim4-config.templates:7001
msgid ""
"The option to hide the local mail name in outgoing mail was enabled. It is "
"therefore necessary to specify the domain name this system should use for "
"the domain part of local users' sender addresses."
msgstr ""

#. Type: string
#. Description
#: ../exim4-config.templates:8001
msgid "IP address or host name of the outgoing smarthost:"
msgstr ""

#. Type: string
#. Description
#: ../exim4-config.templates:8001
msgid ""
"Please enter the IP address or the host name of a mail server that this "
"system should use as outgoing smarthost. If the smarthost only accepts your "
"mail on a port different from TCP/25, append two colons and the port number "
"(for example smarthost.example::587 or 192.168.254.254::2525). Colons in "
"IPv6 addresses need to be doubled."
msgstr ""

#. Type: string
#. Description
#: ../exim4-config.templates:8001
#, fuzzy
msgid ""
"If the smarthost requires authentication, please refer to the Debian-"
"specific README files in /usr/share/doc/exim4-base for notes about setting "
"up SMTP authentication."
msgstr ""
"Погледни во /usr/share/doc/exim4-base/README.Debian.gz за забелешки за "
"поставување на СМТП автентикација."

#. Type: string
#. Description
#: ../exim4-config.templates:9001
msgid "Root and postmaster mail recipient:"
msgstr "Примач на поштата на root и postmaster:"

#. Type: string
#. Description
#: ../exim4-config.templates:9001
msgid ""
"Mail for the 'postmaster', 'root', and other system accounts needs to be "
"redirected to the user account of the actual system administrator."
msgstr ""

#. Type: string
#. Description
#: ../exim4-config.templates:9001
msgid ""
"If this value is left empty, such mail will be saved in /var/mail/mail, "
"which is not recommended."
msgstr ""

#. Type: string
#. Description
#: ../exim4-config.templates:9001
#, fuzzy
msgid ""
"Note that postmaster's mail should be read on the system to which it is "
"directed, rather than being forwarded elsewhere, so (at least one of) the "
"users listed here should not redirect their mail off this machine. A 'real-' "
"prefix can be used to force local delivery."
msgstr ""
"Поштата за „postmaster“, „root“ и други системски сметки најчесто се "
"препраќа на корисничката сметка на вистинскиот администратор. Ако ја оствиш "
"оваа вредност празна, ваквите пораки ќе бидат снимани во /var/mail/mail што "
"не е препорачливо. Имај на ум дека поштата за postmaster треба да се чита на "
"машината за која е насочена, отколку да се препраќа на друго место. Така "
"барем за еден од корисниците кои ќе ги одбереш поштата не треба да се "
"препраќа на друго место. Користи го „real-“ префиксот за да форсираш локална "
"испорака."

#. Type: string
#. Description
#: ../exim4-config.templates:9001
#, fuzzy
msgid "Multiple user names need to be separated by spaces."
msgstr "Внеси едно или повеќе кориснички имиња одвоени од празни места."

#. Type: string
#. Description
#: ../exim4-config.templates:10001
msgid "IP-addresses to listen on for incoming SMTP connections:"
msgstr "ИП адреса за која ќе слушаат дојдовните СМТП конекции:"

#. Type: string
#. Description
#: ../exim4-config.templates:10001
msgid ""
"Please enter a semicolon-separated list of IP addresses. The Exim SMTP "
"listener daemon will listen on all IP addresses listed here."
msgstr ""

#. Type: string
#. Description
#: ../exim4-config.templates:10001
#, fuzzy
msgid ""
"An empty value will cause Exim to listen for connections on all available "
"network interfaces."
msgstr ""
"Ако ја оставиш оваа вредност празна, Exim ќе слуша за конекции на СМТП "
"портот на сите достапни мрежни интерфејси."

#. Type: string
#. Description
#: ../exim4-config.templates:10001
#, fuzzy
msgid ""
"If this system only receives mail directly from local services (and not from "
"other hosts), it is suggested to prohibit external connections to the local "
"Exim daemon. Such services include e-mail programs (MUAs) which talk to "
"localhost only as well as fetchmail. External connections are impossible "
"when 127.0.0.1 is entered here, as this will disable listening on public "
"network interfaces."
msgstr ""
"Ако твојот компјутер не прима пошта директно преку СМТП од ДРУГИ компјутери, "
"туку само од локални сервиси како fetchmail или твојот клиент за е-пошта "
"(MUA) треба да ги забраниш надворешните  конекции кон Exim тука внесувајќи "
"127.0.0.1. Со ова забрануваш слушање на јавните мрежни интерфејси."

#. Type: boolean
#. Description
#: ../exim4-config.templates:11001
msgid "Keep number of DNS-queries minimal (Dial-on-Demand)?"
msgstr "Држи го минимален бројот на ДНС прашувања (Вртење-по-потреба)?"

#. Type: boolean
#. Description
#: ../exim4-config.templates:11001
#, fuzzy
msgid ""
"In normal mode of operation Exim does DNS lookups at startup, and when "
"receiving or delivering messages. This is for logging purposes and allows "
"keeping down the number of hard-coded values in the configuration."
msgstr ""
"Во нормален режим на работа Exim прави ДНС-пребарувања при пуштање, кога "
"прима или праќа порака и сл. за логирање или за да го држи бројот на цврсто "
"внесени вредности во конфигурационата датотека мал."

#. Type: boolean
#. Description
#: ../exim4-config.templates:11001
#, fuzzy
msgid ""
"If this system does not have a DNS full service resolver available at all "
"times (for example if its Internet access is a dial-up line using dial-on-"
"demand), this might have unwanted consequences. For example, starting up "
"Exim or running the queue (even with no messages waiting) might trigger a "
"costly dial-up-event."
msgstr ""
"Ако ова е компјутер без постојан пристап до ДНС сервер кој користи вртење-по-"
"потреба ова може да предизвика несакани последици така што при стартување на "
"exim или работа со поштата(и без пораки) ќе се прават непотребни вртења кон "
"интернет провајдерот."

#. Type: boolean
#. Description
#: ../exim4-config.templates:11001
msgid ""
"This option should be selected if this system is using Dial-on-Demand. If it "
"has always-on Internet access, this option should be disabled."
msgstr ""

#. Type: title
#. Description
#: ../exim4-config.templates:12001
#, fuzzy
msgid "Mail Server configuration"
msgstr "Главен тип на конфигурација за пошта:"

#. Type: boolean
#. Description
#: ../exim4-config.templates:13001
msgid "Split configuration into small files?"
msgstr "Подели ја конфигурацијата во помали датотеки?"

#. Type: boolean
#. Description
#: ../exim4-config.templates:13001
#, fuzzy
msgid ""
"The Debian exim4 packages can either use 'unsplit configuration', a single "
"monolithic file (/etc/exim4/exim4.conf.template) or 'split configuration', "
"where the actual Exim configuration files are built from about 50 smaller "
"files in /etc/exim4/conf.d/."
msgstr ""
"Exim4 пакетите на Дебиан може да користат една голема монолитна датотека (/"
"etc/exim4/exim4.conf.template) или околу 40 мали датотеки во /etc/exim4/conf."
"d/ од кои се генерира крајната конфигурација."

#. Type: boolean
#. Description
#: ../exim4-config.templates:13001
#, fuzzy
msgid ""
"Unsplit configuration is better suited for large modifications and is "
"generally more stable, whereas split configuration offers a comfortable way "
"to make smaller modifications but is more fragile and might break if "
"modified carelessly."
msgstr ""
"Првото е подоборо за големи промени и најчесто е постабилно, додека второто "
"овозможува убав начин да се прават помали промени, но е понежно и може да се "
"расипе ако се менуваат многу работи."

#. Type: boolean
#. Description
#: ../exim4-config.templates:13001
msgid ""
"A more detailed discussion of split and unsplit configuration can be found "
"in the Debian-specific README files in /usr/share/doc/exim4-base."
msgstr ""

#. Type: boolean
#. Description
#: ../exim4-config.templates:14001
msgid "Hide local mail name in outgoing mail?"
msgstr "Сокриј го локалното поштенско име во надворешната пошта?"

#. Type: boolean
#. Description
#: ../exim4-config.templates:14001
#, fuzzy
msgid ""
"The headers of outgoing mail can be rewritten to make it appear to have been "
"generated on a different system. If this option is chosen, '${mailname}', "
"'localhost' and '${dc_other_hostnames}' in From, Reply-To, Sender and Return-"
"Path are rewritten."
msgstr ""
"Заглавјата во надворешната пошта можат да бидат препишани така што би "
"изгледале како да се создадени на друг систем. Сменувајќи го "
"„${mailname}“ „localhost“ и „${dc_other_hostnames}“ во From, Reply-To, "
"Sender и Return-Path полињата."

#. Type: select
#. Choices
#: ../exim4-config.templates:15001
msgid "mbox format in /var/mail/"
msgstr ""

#. Type: select
#. Choices
#: ../exim4-config.templates:15001
msgid "Maildir format in home directory"
msgstr ""

#. Type: select
#. Description
#: ../exim4-config.templates:15002
#, fuzzy
msgid "Delivery method for local mail:"
msgstr "пошта пратена преку друг домаќин - нема локална пошта"

#. Type: select
#. Description
#: ../exim4-config.templates:15002
msgid ""
"Exim is able to store locally delivered email in different formats. The most "
"commonly used ones are mbox and Maildir. mbox uses a single file for the "
"complete mail folder stored in /var/mail/. With Maildir format every single "
"message is stored in a separate file in ~/Maildir/."
msgstr ""

#. Type: select
#. Description
#: ../exim4-config.templates:15002
msgid ""
"Please note that most mail tools in Debian expect the local delivery method "
"to be mbox in their default."
msgstr ""

#, fuzzy
#~ msgid "Move undelivered mails from exim 3 to exim4 spool?"
#~ msgstr ""
#~ "Премести ги сеуште недоставените пораки од exim(в3) во exim4 редот на "
#~ "праќање?"

#, fuzzy
#~ msgid ""
#~ "Choosing this option will move these messages to exim4's spool (/var/"
#~ "spool/exim4/input/) where they will be handled by exim4."
#~ msgstr ""
#~ "Има некои неиспратени порарки во директорумот за праќање на exim или exim-"
#~ "ls во /var/spool/exim/input. Тие сега може да бидат преместени во "
#~ "директорумот за праќање на exim4 (/var/spool/exim4/input/) каде ќе бидат "
#~ "обработени од exim4."

#~ msgid ""
#~ "Be aware that this works only one-way, exim4 can handle exim(v3)'s spool "
#~ "but not the other way round."
#~ msgstr ""
#~ "Имај на ум дека ова работи само во еден правец, exim4 може да работи со "
#~ "exim(v3) директорумот за праќање но не обратно."

#~ msgid ""
#~ "Move the mails only if you don't plan to go back to exim(v3), otherwise "
#~ "the mail shouldn't be moved now but manually once you've converted your "
#~ "setup."
#~ msgstr ""
#~ "Премести ги пораките само ако не планираш да се вратиш на exim(в3), во "
#~ "друг случај поштата не треба да биде преместена сега туку рачно откога ќе "
#~ "ги преместиш поставките."

#, fuzzy
#~ msgid ""
#~ "If there are any more, enter them here, separated by semicolons. You may "
#~ "leave this blank if there are none."
#~ msgstr ""
#~ "Ако има повеќе, внеси ги тука одвоени со две точки. Може да го оставиш "
#~ "ова празно ако нема повеќе."

#, fuzzy
#~ msgid ""
#~ "Please enter here the domains for which this system will relay mail, for "
#~ "example as a fallback MX or mail gateway."
#~ msgstr ""
#~ "Те молам тука внеси ги домејните за кои прифаќаш да ја препраќаш поштата."

#~ msgid ""
#~ "Such domains are domains for which you are prepared to accept mail from "
#~ "anywhere on the Internet. Do not mention local domains here."
#~ msgstr ""
#~ "Такви домејни се домејни за кои си подготвен да прифаќаш пошта од било "
#~ "каде на интернет. Не ги спомнувај локалните домејни тука."

#, fuzzy
#~ msgid ""
#~ "The domains entered here should be separated by semicolons. Wildcards may "
#~ "be used."
#~ msgstr ""
#~ "Домејните кои ќе ги внесеш тука треба да бидат одбоени со две точки. "
#~ "Џокери може да се користат."

#~ msgid ""
#~ "Since you enabled hiding the local mailname in outgoing mail, you must "
#~ "specify the domain name to use for mail from local users; typically this "
#~ "is the machine on which you normally receive your mail."
#~ msgstr ""
#~ "Бидејќи си овозможил криење на поштенското име во надворешна пошта, мора "
#~ "да специфицираш домејн име кое ќе се користи за пошта од локалните "
#~ "корисници. Типично ова е машината на која нормално примаш пошта."

#~ msgid "Where will your users read their mail?"
#~ msgstr "Каде твоите корисници ќе ја читаат нивната пошта?"

#~ msgid "Machine handling outgoing mail for this host (smarthost):"
#~ msgstr "Машини кои ја обработуваат поштата за праќање за оваа машина:"

#~ msgid "Enter the hostname of the machine to which outgoing mail is sent."
#~ msgstr ""
#~ "Внеси го хостнејмот на машината на која ќе и биде праќана надворешната "
#~ "пошта."

#~ msgid ""
#~ "Enable this feature if you are using Dial-on-Demand; otherwise, disable "
#~ "it."
#~ msgstr ""
#~ "Овомозжи ја оваа опција ако користиш вретење-по-потреба, во друг случај "
#~ "исклучи ја."

#, fuzzy
#~ msgid "Select the mail server configuration type that best fits your needs."
#~ msgstr "Избери го типот на конфигурација кој најдобро ти одговара."

#~ msgid "If you are unsure then you should not use split configuration."
#~ msgstr ""
#~ "Ако не си сигурен тогаш не треба ја користиш поделената конфигурација."

#~ msgid "manually convert from handcrafted Exim v3 configuration"
#~ msgstr "рачно конвертирај од рачно поставената конфигурација на exim в3"

#~ msgid "Configure Exim4 manually?"
#~ msgstr "Конфигурирај Exim4 рачно?"

#~ msgid ""
#~ "You indicated that you have a handcrafted Exim 3 configuration. To "
#~ "convert this to Exim 4, you can use the exim_convert4r4(8) tool after the "
#~ "installation. Consult /usr/share/doc/exim4-base/examples/example.conf.gz "
#~ "and /usr/share/doc/exim4-base/README.Debian.gz!"
#~ msgstr ""
#~ "Индицираше дека имаш рачно изработенa Exim3 конфигурација. За да ја "
#~ "префрлиш оваа во Exim4, може да ја користиш exim_convert4r4(8)  алатката "
#~ "по инсталација. Консултирај се со /usr/share/doc/exim4-base/examples/"
#~ "example.conf.gz и /usr/share/doc/exim4-base/README.Debian.gz!"

#~ msgid ""
#~ "Until your mail system is configured, it will be broken and cannot be "
#~ "used."
#~ msgstr ""
#~ "Се додека твојот систем за пошта не е конфигуриран ќе биде расипан и не "
#~ "ќе може да се користи."

#~ msgid ""
#~ "Your \"mail name\" is the hostname portion of the address to be shown on "
#~ "outgoing news and mail messages (following the username and @ sign) "
#~ "unless hidden with rewriting."
#~ msgstr ""
#~ "Твоето „поштенско име“ е хостнејм делот од адресата која ќе биде "
#~ "прикажана на пораките (следувајќи по корисничкото име и @ знакот) освен "
#~ "ако не е сокриено при препишување."

#~ msgid ""
#~ "Please enter here the networks of local machines for which you accept to "
#~ "relay the mail."
#~ msgstr ""
#~ "Те молам тука внеси ги мрежите за локалните машини за кои прифаќаш да "
#~ "препраќаш пошта."

#~ msgid ""
#~ "This should include a list of all machines that will use us as a "
#~ "smarthost."
#~ msgstr ""
#~ "Ова треба да содржи листа на сите машини кои ќе не користат нас како "
#~ "препраќач на поштата."

#~ msgid ""
#~ "You need to double the colons in IPv6 addresses (e.g. "
#~ "5f03::1200::836f::::/48)"
#~ msgstr ""
#~ "Треба да ги дуплираш двете точки во IPv6 адресите (пр. "
#~ "5f03::1200::836f::::/48)"

#~ msgid ""
#~ "Enter a colon-separated list of IP-addresses to listen on.  You need to "
#~ "double the colons in IPv6 addresses (e.g. 5f03::1200::836f::::)."
#~ msgstr ""
#~ "Внеси листа на ИП адреси одвоени со две точки за кои ќе слуша. Треба да "
#~ "ги удвоиш двете точки во IPv6 адреси (пр. 5f03::1200::836f::::)."

#~ msgid "Configuring Exim v4 (exim4-config)"
#~ msgstr "Конфигурација на Exim в4 (exim4-config)"
