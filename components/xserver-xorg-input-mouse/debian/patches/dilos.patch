Index: xserver-xorg-input-mouse-1.9.3/src/sun_mouse.c
===================================================================
--- xserver-xorg-input-mouse-1.9.3.orig/src/sun_mouse.c
+++ xserver-xorg-input-mouse-1.9.3/src/sun_mouse.c
@@ -1,6 +1,5 @@
 /*
- * Copyright (c) 2004-2005, 2008-2010, Oracle and/or its affiliates.
- * All rights reserved.
+ * Copyright (c) 2004, 2013, Oracle and/or its affiliates. All rights reserved.
  *
  * Permission is hereby granted, free of charge, to any person obtaining a
  * copy of this software and associated documentation files (the "Software"),
@@ -51,6 +50,8 @@
 #include <xorg-config.h>
 #endif
 
+#include <sys/param.h>
+
 #include "xorg-server.h"
 #include "xf86.h"
 #include "xf86_OSlib.h"
@@ -58,7 +59,7 @@
 #include "xisb.h"
 #include "mipointer.h"
 #include "xf86Crtc.h"
-#include <sys/stropts.h>
+#include <stropts.h>
 #include <sys/vuid_event.h>
 #include <sys/msio.h>
 
@@ -70,6 +71,8 @@
 # include <sys/vuid_wheel.h>
 #endif
 
+#include <libdevinfo.h>
+
 /* Support for scaling absolute coordinates to screen size in
  * Solaris 10 updates and beyond */
 #if !defined(HAVE_ABSOLUTE_MOUSE_SCALING)
@@ -105,6 +108,9 @@ typedef struct _VuidMseRec {
     Ms_screen_resolution         absres;
 #endif
     OsTimerPtr          remove_timer;   /* Callback for removal on ENODEV */
+    int                 relToAbs;
+    int                 absX;
+    int                 absY;
 } VuidMseRec, *VuidMsePtr;
 
 static VuidMsePtr       vuidMouseList = NULL;
@@ -118,6 +124,9 @@ static void vuidReadInput(InputInfoPtr p
 static void vuidMouseSendScreenSize(ScreenPtr pScreen, VuidMsePtr pVuidMse);
 static void vuidMouseAdjustFrame(ADJUST_FRAME_ARGS_DECL);
 
+static int CheckRelToAbs(InputInfoPtr pInfo);
+static int CheckRelToAbsWalker(di_node_t node, void *arg);
+
 static int vuidMouseGeneration = 0;
 
 #if HAS_DEVPRIVATEKEYREC
@@ -220,6 +229,11 @@ vuidPreInit(InputInfoPtr pInfo, const ch
 
     pVuidMse->buffer = (unsigned char *)&pVuidMse->event;
     pVuidMse->strmod = xf86SetStrOption(pInfo->options, "StreamsModule", NULL);
+    pVuidMse->relToAbs = xf86SetIntOption(pInfo->options, "RelToAbs", -1);
+    if (pVuidMse->relToAbs == -1)
+	pVuidMse->relToAbs = CheckRelToAbs(pInfo);
+    pVuidMse->absX = 0;
+    pVuidMse->absY = 0;
 
     /* Setup the local procs. */
     pVuidMse->wrapped_device_control = pInfo->device_control;
@@ -241,6 +255,120 @@ vuidPreInit(InputInfoPtr pInfo, const ch
     return TRUE;
 }
 
+/*
+ * It seems that the mouse that is presented by the Emulex ILOM
+ * device (USB 0x430, 0xa101 and USB 0x430, 0xa102) sends relative
+ * mouse movements.  But relative mouse movements are subject to
+ * acceleration.  This causes the position indicated on the ILOM
+ * window to not match what the Xorg server actually has.  This
+ * makes the mouse in this environment rather unusable.  So, for the
+ * Emulex ILOM device, we will change all relative mouse movements
+ * into absolute mouse movements, making it appear more like a
+ * tablet.  This will not be subject to acceleration, and this
+ * should keep the ILOM window and Xorg server with the same values
+ * for the coordinates of the mouse.
+ */
+
+typedef struct reltoabs_match {
+	int matched;
+	char const *matchname;
+	} reltoabs_match_t;
+
+/* Sun Microsystems, keyboard / mouse */
+#define	RELTOABS_MATCH1	"usbif430,a101.config1.1"
+/* Sun Microsystems, keyboard / mouse / storage */
+#define	RELTOABS_MATCH2	"usbif430,a102.config1.1"
+
+static int
+CheckRelToAbsWalker(di_node_t node, void *arg)
+{
+	di_minor_t minor;
+	char *path;
+	int numvalues;
+	int valueon;
+	char const *stringptr;
+	int status;
+	reltoabs_match_t *const matchptr = (reltoabs_match_t *)arg;
+	char *stringvalues;
+
+	for (minor = di_minor_next(node, DI_MINOR_NIL);
+	  minor != DI_MINOR_NIL;
+	  minor = di_minor_next(node, minor)) {
+	    path = di_devfs_minor_path(minor);
+	    status = path != NULL && strcmp(path, matchptr -> matchname) == 0;
+	    di_devfs_path_free(path);
+	    if (status)
+		break;
+	    }
+
+	if (minor == DI_MINOR_NIL)
+	    return (DI_WALK_CONTINUE);
+
+	numvalues = di_prop_lookup_strings(DDI_DEV_T_ANY, node,
+	  "compatible", &stringvalues);
+	if (numvalues <= 0)
+	    return (DI_WALK_CONTINUE);
+
+	for (valueon = 0, stringptr = stringvalues; valueon < numvalues;
+	  valueon++, stringptr += strlen(stringptr) + 1) {
+	    if (strcmp(stringptr, RELTOABS_MATCH1) == 0) {
+		matchptr -> matched = 1;
+		return (DI_WALK_TERMINATE);
+	    }
+	    if (strcmp(stringptr, RELTOABS_MATCH2) == 0) {
+		matchptr -> matched = 2;
+		return (DI_WALK_TERMINATE);
+	    }
+	}
+	return (DI_WALK_CONTINUE);
+}
+
+static int
+CheckRelToAbs(InputInfoPtr pInfo)
+{
+	char const *device;
+	char const *matchname;
+	ssize_t readstatus;
+	di_node_t node;
+	struct stat statbuf;
+	char linkname[512];
+	reltoabs_match_t reltoabs_match;
+
+	device = xf86CheckStrOption(pInfo->options, "Device", NULL);
+	if (device == NULL)
+	    return (0);
+
+	matchname = device;
+
+	if (lstat(device, &statbuf) == 0 &&
+	  (statbuf.st_mode & S_IFMT) == S_IFLNK) {
+	    readstatus = readlink(device, linkname, sizeof(linkname));
+	    if (readstatus > 0 && readstatus < sizeof(linkname)) {
+		linkname[readstatus] = 0;
+		matchname = linkname;
+		if (strncmp(matchname, "../..", sizeof("../..") - 1) == 0)
+		    matchname += sizeof("../..") - 1;
+	    }
+	}
+
+	if (strncmp(matchname, "/devices", sizeof("/devices") - 1) == 0)
+	    matchname += sizeof("/devices") - 1;
+
+	reltoabs_match.matched = 0;
+	reltoabs_match.matchname = matchname;
+
+	node = di_init("/", DINFOCPYALL);
+	if (node == DI_NODE_NIL)
+	    return (0);
+
+	di_walk_node(node, DI_WALK_CLDFIRST, (void *)&reltoabs_match,
+	  CheckRelToAbsWalker);
+
+	di_fini(node);
+
+	return (reltoabs_match.matched != 0);
+}
+
 static void
 vuidFlushAbsEvents(InputInfoPtr pInfo, int absX, int absY,
                    Bool *absXset, Bool *absYset)
@@ -276,20 +404,29 @@ vuidFlushAbsEvents(InputInfoPtr pInfo, i
 static void
 vuidReadInput(InputInfoPtr pInfo)
 {
-    MouseDevPtr pMse;
-    VuidMsePtr pVuidMse;
-    int buttons;
+    MouseDevPtr pMse = pInfo->private;
+    VuidMsePtr pVuidMse = getVuidMsePriv(pInfo);
+    int buttons = pMse->lastButtons;
     int dx = 0, dy = 0, dz = 0, dw = 0;
-    unsigned int n;
-    unsigned char *pBuf;
+    unsigned int n = 0;
+    unsigned char *pBuf = pVuidMse->buffer;
     int absX = 0, absY = 0;
+    int hdisplay = 0, vdisplay = 0;
     Bool absXset = FALSE, absYset = FALSE;
+    int relToAbs = pVuidMse->relToAbs;
 
-    pMse = pInfo->private;
-    pVuidMse = getVuidMsePriv(pInfo);
-    buttons = pMse->lastButtons;
-    pBuf = pVuidMse->buffer;
-    n = 0;
+    if (relToAbs) {
+       ScreenPtr   pScreen = miPointerGetScreen(pInfo->dev);
+       ScrnInfoPtr pScr = XF86SCRNINFO(pScreen);
+       if (pScr->currentMode) {
+           hdisplay = pScr->currentMode->HDisplay;
+           vdisplay = pScr->currentMode->VDisplay;
+       }
+       absX = pVuidMse->absX;
+       absY = pVuidMse->absY;
+    }
+
+    errno = 0;
 
     do {
         n = read(pInfo->fd, pBuf, sizeof(Firm_event));
@@ -298,6 +435,7 @@ vuidReadInput(InputInfoPtr pInfo)
             break;
         } else if (n == -1) {
             switch (errno) {
+                case 0:      /* no errors */
                 case EAGAIN: /* Nothing to read now */
                     n = 0;   /* End loop, go on to flush events & return */
                     continue;
@@ -349,10 +487,34 @@ vuidReadInput(InputInfoPtr pInfo)
             int delta = pVuidMse->event.value;
             switch(pVuidMse->event.id) {
             case LOC_X_DELTA:
-                dx += delta;
+		if (!relToAbs)
+		    dx += delta;
+		else {
+		    if (absXset)
+			vuidFlushAbsEvents(pInfo, absX, absY, &absXset, &absYset);
+		    absX += delta;
+		    if (absX < 0)
+			absX = 0;
+		    else if (absX >= hdisplay && hdisplay > 0)
+			absX = hdisplay - 1;
+		    pVuidMse->absX = absX;
+		    absXset = TRUE;
+		}
                 break;
             case LOC_Y_DELTA:
-                dy -= delta;
+		if (!relToAbs)
+		    dy -= delta;
+		else {
+		    if (absYset)
+			vuidFlushAbsEvents(pInfo, absX, absY, &absXset, &absYset);
+		    absY -= delta;
+		    if (absY < 0)
+			absY = 0;
+		    else if (absY >= vdisplay && vdisplay > 0)
+			absY = vdisplay - 1;
+		    pVuidMse->absY = absY;
+		    absYset = TRUE;
+		}
                 break;
             case LOC_X_ABSOLUTE:
                 if (absXset) {
@@ -481,7 +643,7 @@ vuidMouseProc(DeviceIntPtr pPointer, int
 {
     InputInfoPtr pInfo;
     MouseDevPtr pMse;
-    VuidMsePtr pVuidMse;
+    VuidMsePtr pVuidMse, m;
     int ret = Success;
     int i;
 
@@ -568,8 +730,20 @@ vuidMouseProc(DeviceIntPtr pPointer, int
         }
         break;
 
-    case DEVICE_OFF:
     case DEVICE_CLOSE:
+	m = vuidMouseList;
+
+	if (m == pVuidMse)
+	    vuidMouseList = m->next;
+	else {
+		while ((m != NULL) && (m->next != pVuidMse)) {
+		    m = m->next;
+		}
+
+		if (m != NULL)
+		    m->next = pVuidMse->next;
+	}
+    case DEVICE_OFF:
         if (pInfo->fd != -1) {
             if (pVuidMse->strmod) {
                 SYSCALL(i = ioctl(pInfo->fd, I_POP, pVuidMse->strmod));
Index: xserver-xorg-input-mouse-1.9.3/src/mouse.c
===================================================================
--- xserver-xorg-input-mouse-1.9.3.orig/src/mouse.c
+++ xserver-xorg-input-mouse-1.9.3/src/mouse.c
@@ -1763,18 +1763,19 @@ MouseProc(DeviceIntPtr device, int what)
         ErrorF("assigning %p name=%s\n", device, pInfo->name);
 #endif
         MouseInitProperties(device);
-        break;
-
-    case DEVICE_ON:
         pInfo->fd = xf86OpenSerial(pInfo->options);
         if (pInfo->fd == -1)
             xf86Msg(X_WARNING, "%s: cannot open input device\n", pInfo->name);
-        else {
+
+        break;
+
+    case DEVICE_ON:
 #if defined(__NetBSD__) && defined(WSCONS_SUPPORT) && defined(WSMOUSEIO_SETVERSION)
             int version = WSMOUSE_EVENT_VERSION;
             if (ioctl(pInfo->fd, WSMOUSEIO_SETVERSION, &version) == -1)
                 xf86Msg(X_WARNING, "%s: cannot set version\n", pInfo->name);
 #endif
+	if (pInfo->fd != -1) {
             if (pMse->xisbscale)
                 pMse->buffer = XisbNew(pInfo->fd, pMse->xisbscale * 4);
             else
@@ -1830,8 +1831,6 @@ MouseProc(DeviceIntPtr device, int what)
                 XisbFree(pMse->buffer);
                 pMse->buffer = NULL;
             }
-            xf86CloseSerial(pInfo->fd);
-            pInfo->fd = -1;
             if (pMse->emulate3Buttons || pMse->emulate3ButtonsSoft)
             {
                 RemoveBlockAndWakeupHandlers (MouseBlockHandler,
@@ -1842,6 +1841,10 @@ MouseProc(DeviceIntPtr device, int what)
         device->public.on = FALSE;
         break;
     case DEVICE_CLOSE:
+        if (pInfo->fd != -1) {
+            xf86CloseSerial(pInfo->fd);
+            pInfo->fd = -1;
+        }
         free(pMse->mousePriv);
         pMse->mousePriv = NULL;
         break;
