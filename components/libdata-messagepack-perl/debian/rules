#!/usr/bin/make -f
# -*- mode: makefile; coding: utf-8 -*-
# Copyright 2013, Vasudev Kamath <kamathvasudev@gmail.com>
# Copyright 2013-2017, Jonas Smedegaard <dr@jones.dk>
# Description: Main Debian packaging script for Data::MessagePack
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
DPKG_EXPORT_BUILDFLAGS = 1

include /usr/share/cdbs/1/rules/debhelper.mk
include /usr/share/cdbs/1/rules/utils.mk
include /usr/share/cdbs/1/class/perl-makemaker.mk

pkg = $(DEB_SOURCE_PACKAGE)

# suppress checking license during build
DEB_COPYRIGHT_CHECK_LICENSECHECK =
CDBS_BUILD_DEPENDS_rules_utils_copyright-check =

# Needed by upstream build
bdeps = libmsgpack-dev (>= 3.1.0), libfile-copy-recursive-perl

# Needed by upstream testsuite
bdeps-test = libtest-requires-perl, libtest-leaktrace-perl

CDBS_BUILD_DEPENDS +=, $(bdeps), $(bdeps-test)

DEB_UPSTREAM_CRUFT_MOVE = include
