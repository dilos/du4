#!/usr/bin/make -f

# Enable this export to get more detailed information about what debhelper does
#export DH_VERBOSE=1

## Define the names of the resulting binary packages
PKG_AGENT = zabbix-agent
PKG_AGENT2 = zabbix-agent2
PKG_WEB_SERVICE = zabbix-web-service
PKG_FRONTEND = zabbix-frontend-php
PKG_GATEWAY_JAVA = zabbix-java-gateway
PKG_PROXY_MYSQL = zabbix-proxy-mysql
PKG_PROXY_PGSQL = zabbix-proxy-pgsql
PKG_PROXY_SQLITE3 = zabbix-proxy-sqlite3
PKG_SERVER_MYSQL = zabbix-server-mysql
PKG_SERVER_PGSQL = zabbix-server-pgsql
#PKG_SERVER_SQLITE3 = zabbix-server-sqlite3

# These are used for cross-compiling and for saving the configure script
# from having to guess our platform (since we know it already)
export DEB_HOST_GNU_TYPE  ?= $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
export DEB_BUILD_GNU_TYPE ?= $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)

# DilOS update: disable go*
### Build Agent2 on some architectures:
##ifeq ($(DEB_HOST_ARCH),$(filter $(DEB_HOST_ARCH),amd64 i386))
#AGENT2:=true
##endif
### "GO111MODULE=on" is needed to fix: `cannot find package "zabbix.com/cmd/zabbix_agent2"`.
#export GO111MODULE=on
export GO111MODULE=off
export GOPROXY=off
export GOCACHE=$(CURDIR)/debian/tmp/cache/go-build

# Squeeze backporters: --export=configure is not available on Squeeze
#DPKG_BUILDFLAGS := $(shell DEB_CFLAGS_MAINT_APPEND=-Wall dpkg-buildflags --export=configure)

#export DEB_BUILD_MAINT_OPTIONS=hardening=+all
export DEB_CFLAGS_MAINT_APPEND= -D__dilos__
export CC = gcc

CONFFLAGS_COMMON  = --host=$(DEB_HOST_GNU_TYPE) \
                    --build=$(DEB_BUILD_GNU_TYPE) \
                    --datadir=/etc \
                    --sysconfdir=/etc/zabbix \
                    --prefix=/usr \
                    --infodir=\$${prefix}/share/info \
                    --mandir=\$${prefix}/share/man \
                    --enable-agent \
                    --enable-proxy \
                    --enable-ipv6 \
                    --with-gnutls \
                    --with-jabber \
                    --with-ldap \
                    --with-libcurl \
                    --with-libmodbus \
                    --with-libpcre2 \
                    --with-libxml2-libs="$(shell $(DEB_HOST_GNU_TYPE)-pkg-config --libs libxml-2.0)" \
                    --with-libxml2-cflags="$(shell $(DEB_HOST_GNU_TYPE)-pkg-config --cflags libxml-2.0)" \
                    --with-net-snmp \
                    --with-ssh2 \
                    --with-unixodbc

## Hurd and kFreeBSD only have Java 1.5 which can't compile java-gateway sources...
#ifneq ($(DEB_BUILD_ARCH_OS),hurd)
#ifneq ($(DEB_BUILD_ARCH_OS),kfreebsd)
#CONFFLAGS_COMMON += --enable-java
#endif
#endif
CONFFLAG_IPMI    = $(if $(shell dpkg --status libopenipmi-dev &>/dev/null),--with-openipmi)
CONFFLAG_MYSQL   = --with-mysql --enable-server
CONFFLAG_PGSQL   = --with-postgresql --enable-server
CONFFLAG_SQLITE3 = --with-sqlite3
BUILD_PREFIX := $(CURDIR)/debian/tmp-build

# The Zabbix sources are compiled once for each database backends (VARIANT).
# The proxy and server packages always contain one kind of database support.
# The zabbix-agent and zabbix-frontend-php package do not have any kind of
# database support and are created from one of the above mentioned builds.
VARIANTS := MYSQL PGSQL SQLITE3

ifdef AGENT2
CONFFLAG_AGENT2 = --without-gnutls --with-openssl --with-sqlite3 --enable-agent2 --enable-webservice
VARIANTS := AGENT2 $(VARIANTS)
endif

%:
	dh $@ --max-parallel=4 --with javahelper,linktree,apache2

override_dh_clean:
	@echo "Removing build directories in $(BUILD_PREFIX)"
	$(RM) -r debian/tmp* _build src/go/vendor
	dh_clean \
            config.log debian/*.debhelper \
            ui/js/vendors/jquery-ui.js \
            *.jar
	## Remove Files-Excluded (when built from checkout or non-DFSG tarball):
	$(RM) -rv `perl -0nE 'say $$1 if m{^Files\-Excluded(?:\-\w+)?\:\s*(.*?)(?:\n\n|Files:|Comment:)}sm;' debian/copyright`
	-find vendor -type f -name '*.pb.go' -delete -printf 'removed %p\n'            ## delete generated .pb.go files
	-find vendor -type d -empty -delete -print

# use --as-needed only if supported by dh-autoreconf (to simplify backporting)
DH_AS_NEEDED=$(shell dpkg --compare-versions $$(dpkg --status dh-autoreconf | grep Version | cut -d' ' -f2) ge 6 && echo --as-needed)
override_dh_autoreconf:
	dh_autoreconf $(DH_AS_NEEDED)

override_dh_auto_configure:
ifdef AGENT2
# Populate system Golang dependencies in "src/go/vendor":
# `dh_auto_configure --buildsystem=golang` populates "{builddirectory}/src"
# so we trick it with symlink that is later removed.
	cp -r vendor src/go/    ## Put MUT "vendor" component in place.
	ln -srv src/go/vendor src/go/vendor/src
	DH_GOPKG="__ZX__" dh_auto_configure --buildsystem=golang --builddirectory=src/go/vendor --sourcedirectory=src/go/
	-$(RM) -r src/go/vendor/src
	-$(RM) -r src/go/vendor/__ZX__
endif
	## Copy the source directory (without the 'debian' directory) to build locations
	## to configure and compile the sources there for each database flavor.
	for i in $(VARIANTS); do \
            mkdir $(BUILD_PREFIX)-$$i ;\
            tar -cSf - --exclude=./.svn --exclude=./debian . \
            | tar -xSpf - -C $(BUILD_PREFIX)-$$i ;\
        done
	set -e ;\
$(foreach i,$(VARIANTS), \
    dh_auto_configure --sourcedirectory=$(BUILD_PREFIX)-$i --max-parallel=4 -- \
      $(CONFFLAGS_COMMON) $(CONFFLAG_IPMI) $(CONFFLAG_$i) ;\
)

build-indep: override_dh_autoreconf
	## build styles:
	dh_auto_configure -- --enable-java
	$(MAKE) css
	## build translations:
	cd ui/locale && ./update_po.sh
	cd ui/locale && ./make_mo.sh
	## build zabbix-java-gateway.jar
	## override_jh_build:
	JAVA_HOME=/usr/lib/jvm/oracle-default-jdk \
	CLASSPATH=/usr/share/java/com.android.json.jar:/usr/share/java/logback-core.jar:/usr/share/java/logback-classic.jar:/usr/share/java/slf4j-api.jar \
	jh_build --no-javadoc --main=com.zabbix.gateway.JavaGateway zabbix-java-gateway.jar src/zabbix_java/src/com

override_dh_auto_build:
	set -e ;\
	$(foreach i,$(VARIANTS), \
        dh_auto_build --sourcedirectory=$(BUILD_PREFIX)-$i --max-parallel=4 \
    ;)

override_dh_auto_test:
#	set -e ;\
#	$(foreach i,$(VARIANTS), \
#        dh_auto_test --sourcedirectory=$(BUILD_PREFIX)-$i --max-parallel=4 \
#    ;)

override_dh_auto_install:
	@echo "Replacing sourceless (minified) bundled libraries..."
#	cp -vfH debian/missing-sources/jquery.js  ui/js/vendors/jquery.js
#~	cp -vfH debian/missing-sources/leaflet.js    ui/js/vendors/Leaflet/Leaflet/
	cp -vfH debian/missing-sources/jquery-ui.js  ui/js/vendors/jquery-ui.js
	## build man pages
	if [ -d "$(BUILD_PREFIX)-MYSQL/man" ]; then \
            dh_auto_install --sourcedirectory=$(BUILD_PREFIX)-MYSQL/man \
              --destdir=$(CURDIR)/debian/tmp ;\
        fi

override_dh_install:
	dh_install
	for i in debian/zabbix-*-*; do \
            find $$i -type f -name '*.sql' -printf 'compressing %p\n' \
                -exec gzip -n -9 '{}' \;;\
        done
	## Those files need to be renamed:
	if [ -d "$(CURDIR)/debian/$(PKG_GATEWAY_JAVA)" ]; then \
            cp -v src/zabbix_java/lib/logback.xml  $(CURDIR)/debian/$(PKG_GATEWAY_JAVA)/etc/zabbix/zabbix_java_gateway.logback.xml ;\
            cp -v src/zabbix_java/settings.sh  $(CURDIR)/debian/$(PKG_GATEWAY_JAVA)/etc/zabbix/zabbix_java_gateway.conf ;\
        fi
        ## Directories will be replaced with symlinks:
	if [ -d "$(CURDIR)/debian/$(PKG_FRONTEND)" ]; then \
            rmdir -v $(CURDIR)/debian/$(PKG_FRONTEND)/usr/share/zabbix/local/app/controllers ;\
            rmdir -v $(CURDIR)/debian/$(PKG_FRONTEND)/usr/share/zabbix/local/app/views ;\
        fi
        ## Remove .po files to save space in frontend-php:
	if [ -d "$(CURDIR)/debian/$(PKG_FRONTEND)" ]; then \
            $(RM) -v $(CURDIR)/debian/$(PKG_FRONTEND)/usr/share/zabbix/locale/*/LC_MESSAGES/*.po ;\
        fi

override_jh_depends:
	## make sure java-gateway.jar is executable. This is needed for jh_depends to generate correct {java:Depends}
	if [ -d "$(CURDIR)/debian/$(PKG_GATEWAY_JAVA)" ]; then \
            chmod -v +x $(CURDIR)/debian/$(PKG_GATEWAY_JAVA)/usr/sbin/*.jar ;\
        fi
	jh_depends -v -p$(PKG_GATEWAY_JAVA) -jheadless

## jh_linkjars override is necessary with DH-11 due to the following error:
##   jh_linkjars -O--max-parallel=4 -Nzabbix-frontend-php -Nzabbix-java-gateway
##   Invalid option: N
override_jh_linkjars:
	jh_linkjars -v

# DilOS update: disable go*
#override_dh_gencontrol:
#	if [ -d "debian/$(PKG_AGENT2)" ]; then DH_GOPKG="..." dh_golang --package=$(PKG_AGENT2) --sourcedirectory=src/go/ ;fi
#	if [ -d "debian/$(PKG_WEB_SERVICE)" ]; then DH_GOPKG="..." dh_golang --package=$(PKG_WEB_SERVICE) --sourcedirectory=src/go/ ;fi
#	dh_gencontrol

override_dh_apache2:
	dh_apache2 --noenable

# DilOS update: disable systemd services
#override_dh_installinit:
#	dh_installinit -p $(PKG_AGENT)
#	if [ -d "debian/$(PKG_AGENT2)" ]; then dh_installinit -p $(PKG_AGENT2) ;fi
#	if [ -d "debian/$(PKG_WEB_SERVICE)" ]; then dh_installinit -p $(PKG_WEB_SERVICE) ;fi
#	dh_installinit -p $(PKG_GATEWAY_JAVA)
#	dh_installinit -p $(PKG_PROXY_MYSQL) --name zabbix-proxy --no-start
#	dh_installinit -p $(PKG_PROXY_PGSQL) --name zabbix-proxy --no-start
#	dh_installinit -p $(PKG_PROXY_SQLITE3) --name zabbix-proxy --no-start
#	dh_installinit -p $(PKG_SERVER_MYSQL) --name zabbix-server --no-start
#	dh_installinit -p $(PKG_SERVER_PGSQL) --name zabbix-server --no-start
#ifdef PKG_SERVER_SQLITE3
#	dh_installinit -p $(PKG_SERVER_SQLITE3) --name zabbix-server --no-start
#endif

# DilOS update: disable systemd
#override_dh_installsystemd:
#	dh_installsystemd -p $(PKG_AGENT)
#	if [ -d "debian/$(PKG_AGENT2)" ]; then dh_installsystemd -p $(PKG_AGENT2) ;fi
#	if [ -d "debian/$(PKG_WEB_SERVICE)" ]; then dh_installsystemd -p $(PKG_WEB_SERVICE) ;fi
#	dh_installsystemd -p $(PKG_GATEWAY_JAVA)
#	dh_installsystemd -p $(PKG_PROXY_MYSQL) --name zabbix-proxy --no-start
#	dh_installsystemd -p $(PKG_PROXY_PGSQL) --name zabbix-proxy --no-start
#	dh_installsystemd -p $(PKG_PROXY_SQLITE3) --name zabbix-proxy --no-start
#	dh_installsystemd -p $(PKG_SERVER_MYSQL) --name zabbix-server --no-start
#	dh_installsystemd -p $(PKG_SERVER_PGSQL) --name zabbix-server --no-start
#ifdef PKG_SERVER_SQLITE3
#	dh_installsystemd -p $(PKG_SERVER_SQLITE3) --name zabbix-server --no-start
#endif

override_dh_compress:
	dh_compress -X.xml -X.yaml

override_dh_dwz:
	dh_dwz -v -Xassets/fonts/DejaVuSans.ttf -Xzabbix/conf/COPYING -Xzabbix/local/app/controllers
# -Xbin/zabbix_agent2 -Xbin/zabbix_web_service
