#!/bin/bash
: <<=cut

=head1 DESCRIPTION

This script is called by uscan(1) as per "debian/watch" to populate
"templates" directory from upstream git repository.

 See also https://support.zabbix.com/browse/ZBX-17784

=head1 COPYRIGHT

Copyright: 2018-2022 Dmitry Smirnov <onlyjob@member.fsf.org>

=head1 LICENSE

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

=cut

set -e
set -u

if [ "$1" = '--upstream-version' ]; then
    version="$2"
else
    printf "E: missing argument '--upstream-version'.\n" 1>&2
    exit 1
fi

GOBIN=$(command -v go)
export XZ_OPT="-6v"
DEB_SOURCE="$( dpkg-parsechangelog -SSource )"
#DEB_VERSION="$( dpkg-parsechangelog -SVersion )"
filename="$( readlink -f ../${DEB_SOURCE}_${version}.orig.tar.xz )"
[ -s "${filename}" ] || exit 1

uversion="${version%%?dfsg*}"

## tarpack() takes two arguments:
##  1. directory to compress
##  2. tarball path/name
tarpack() {
    ( find -L "$1" -xdev -type f -print | LC_ALL=C sort \
      | XZ_OPT="-6v" tar -caf "$2" -T- --owner=root --group=root --mode=a+rX \
    )
}

work_dir="$( mktemp -d -t get-orig-source_${DEB_SOURCE}_XXXXXXXX )"
trap "rm -rf '${work_dir}'" EXIT
(
    git clone --branch ${uversion} --depth 1 https://git.zabbix.com/scm/zbx/zabbix.git "${work_dir}"

    cd "${work_dir}/src/go"
    #tar -xf "${filename}" --strip-components=1
    ${GOBIN} version
    set -x
    ${GOBIN} mod vendor -v
    ${GOBIN} mod tidy -v || true
    set +x
    mv vendor ../../
)

## pack component(s):
  for component in templates vendor; do
    FN="$( readlink -f ../${DEB_SOURCE}_${version}.orig-${component}.tar.gz )"
    if [ ! -s "${FN}" ]; then
        ( cd "${work_dir}" && tarpack "${component}" "${FN}" )

        mk-origtargz --package ${DEB_SOURCE} --version ${version} \
          --rename --repack --compression xz --directory .. \
          --component ${component} --copyright-file debian/copyright \
        "${FN}"
    fi
  done
#####

rm -rf "${work_dir}"
