#!/bin/sh
#

#
# Copyright (c) 2012-2018, DilOS.
#
# Permission is hereby granted, free of charge, to any person obtaining a  copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the  rights
# to use, copy, modify, merge, publish,  distribute,  sublicense,  and/or  sell
# copies of the Software, and  to  permit  persons  to  whom  the  Software  is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall  be  included  in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY  KIND,  EXPRESS  OR
# IMPLIED, INCLUDING BUT NOT LIMITED  TO  THE  WARRANTIES  OF  MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT  SHALL  THE
# AUTHORS OR COPYRIGHT HOLDERS BE  LIABLE  FOR  ANY  CLAIM,  DAMAGES  OR  OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

. /lib/svc/share/smf_include.sh

# SMF_FMRI is the name of the target service. This allows multiple instances 
# to use the same script.

# Read command line arguments
method="$1"             # %m
#instance="$2"           # %i

# Set defaults; SMF_FMRI should have been set, but just in case.
if [ -z "$SMF_FMRI" ]; then
	SMF_FMRI="svc:/network/zabbix-agent:default"
fi

ZUSER=zabbix
ZGROUP=zabbix

SBINDIR=/usr/sbin

DAEMON=$SBINDIR/zabbix_agentd

getproparg() {
	val=`svcprop -p $1 $SMF_FMRI`
	[ -n "$val" ] && echo $val
}


zagent_start()
{

	CONFIG=`getproparg config/file`
	if [ ${CONFIG} = '""' ]; then
		CONFIG=""
	else
		CONFIG="-c ${CONFIG}"
	fi

	mkdir -p /var/run/zabbix
	chown -R ${ZUSER}:${ZGROUP} /var/log/zabbix-agent /var/run/zabbix
	${DAEMON} ${CONFIG}
}


zagent_stop()
{

	pkill -f ${DAEMON}
}


zagent_refresh()
{

	${DAEMON} -R config_cache_reload
}


case "$1" in
'start')
	zagent_start 
	;;

'stop')
	zagent_stop
	;;

'refresh')
	zagent_refresh
	;;

*)
	echo "Usage: $0 {start|stop|refresh}"
	exit 1
	;;

esac
exit $SMF_EXIT_OK
