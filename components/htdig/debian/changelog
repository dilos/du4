htdig (1:3.2.0b6-18+dilos1) unstable; urgency=medium

  * Build for DilOS.

 -- DilOS Team <dilos@dilos.org>  Sun, 14 May 2023 14:08:55 +0300

htdig (1:3.2.0b6-18) unstable; urgency=medium

  * QA Upload.
  * Pass MV=/bin/mv to configure
    - this makes the build reproducible between merged-usr and non-merged.

 -- Andreas Henriksson <andreas@fatal.se>  Sat, 01 Dec 2018 19:06:11 +0100

htdig (1:3.2.0b6-17) unstable; urgency=medium

  * QA upload.
  * Fix FTBFS with gcc-7.  Closes: #853445.
  * dh 10.
  * Drop redundant build-depends on autoreconf.

 -- Adam Borowski <kilobyte@angband.pl>  Tue, 22 Aug 2017 03:47:57 +0200

htdig (1:3.2.0b6-16) unstable; urgency=medium

  * QA upload.
  * Fix FTBFS with gcc-6.  Closes: #811738.
  * Add a watch file.
  * Use debhelper 9 (no reworking of debian/rules yet, though).
  * Remove some autoconf files shipped in upstream tarball.
  * Fix a typo in README.Debian.
  * Replace remote-tracking SourceForge logos with a text link.

 -- Adam Borowski <kilobyte@angband.pl>  Thu, 04 Aug 2016 07:05:44 +0200

htdig (1:3.2.0b6-15) unstable; urgency=high

  * QA upload
  * Change dependency on perl5 to perl following the removal of
    the former (Closes: #808215)

 -- Dominic Hargreaves <dom@earth.li>  Thu, 17 Dec 2015 11:12:30 +0000

htdig (1:3.2.0b6-14) unstable; urgency=medium

  * QA upload
  * Convert to source format 3.0 (closes: #775965)
  * Add build-dependency on autotools-dev
  * Add patch kfreebsd: fix FTBFS on kfreebsd-*. Thanks to Steven Chamberlain
    for the patch! (closes: #765284)
  * debian/rules: remove the useless Makefiles from the html docs dir, to
    make builds reproducable. Thanks to Chris Lamb for the patch!
    (closes: #793648)

 -- Ralf Treinen <treinen@debian.org>  Mon, 10 Aug 2015 17:23:47 +0200

htdig (1:3.2.0b6-13) unstable; urgency=medium

  * QA upload.
  * Run dh-autoreconf to update config.guess/sub and libtoo.  Closes:
    #744593.
  * Update Brazilian Portuguese debconf templates translation (Adriano Rafael
    Gomes).  Closes: #693376.

 -- Aurelien Jarno <aurel32@debian.org>  Sun, 24 Aug 2014 05:59:08 +0200

htdig (1:3.2.0b6-12) unstable; urgency=low

  * QA upload
  * Add hardened build flags through dpkg-buildflags

 -- Moritz Muehlenhoff <jmm@debian.org>  Fri, 30 Dec 2011 18:08:23 +0100

htdig (1:3.2.0b6-11) unstable; urgency=low

  * QA upload
  * Fix pending debconf translations:
    - Danish (Joe Hansen).  Closes: #613446
    - Dutch (Jeroen Schot).  Closes: #651830
  * Modernize the package:
    - Add build-arch and build-indep build targets

 -- Christian Perrier <bubulle@sesostris.kheops.frmug.org>  Thu, 29 Dec 2011 22:54:57 +0100

htdig (1:3.2.0b6-10) unstable; urgency=low

  * QA upload.
  * Get rid of unneeded *.la files (Closes: #621608).

 -- Alessio Treglia <alessio@debian.org>  Fri, 03 Jun 2011 11:12:12 +0200

htdig (1:3.2.0b6-9.1) unstable; urgency=low

  * Non-maintainer upload.
  * Resolve RC-bug 'dir-or-file-in-var-www'. (Closes: #553540)
    + debian/rules: 
      o Use '--with-image-dir=/var/lib/htdig/www'.
      o Use parent option 'mkdir -p' to avoid errors.
    + debian/htdig.install: Exchange 'var/www/htdig' for
        'var/lib/htdig/www'.
    + debian/NEWS: New file documenting the changes caused by FHS.

 -- Mats Erik Andersson <mats.andersson@gisladisker.se>  Tue, 19 Jan 2010 16:10:36 +0100

htdig (1:3.2.0b6-9) unstable; urgency=low

  * QA upload.
  * Update config.{sub,guess} files in clean target. (Closes: #535724)
  * Add ${misc:Depends} to all binary packages as we use debhelper.
  * Call dh_makeshlibs before dh_installdeb.
  * Bump Standards-Version to 3.8.2.

 -- Chris Lamb <lamby@debian.org>  Mon, 27 Jul 2009 04:02:56 +0200

htdig (1:3.2.0b6-8) unstable; urgency=low

  * QA upload.
  * Fix pending l10n bugs. Debconf translations
    - Swedish. Closes: #487372

 -- Christian Perrier <bubulle@debian.org>  Tue, 23 Sep 2008 22:23:03 +0200

htdig (1:3.2.0b6-7) unstable; urgency=low

  * QA upload.
  * Fix pending l10n issues
  * Debconf translations:
    - Georgian. Closes: #498311
    - Basque. Closes: #498334
    - Spanish. Closes: #498433

 -- Christian Perrier <bubulle@debian.org>  Sun, 14 Sep 2008 16:26:36 +0200

htdig (1:3.2.0b6-6) unstable; urgency=medium

  * QA upload.
  * Bump Standards-Version to 3.7.3. No changes.
  * Clean libhtdigphp/config.{log,status} in debian/rules clean target.
  * Fix "warning: `Use' not defined" manpage error.
  * Rename nested "examples" directory in /usr/share/doc/htdig-doc to "htdoc"
    to avoid confusion.
  * Create {root2word,word2root,synonyms}.db in /var/lib/htdig instead of
    /etc/htdig (Closes: #74523, #475885).
  * Override 'verbose' environment variable when calling htdigconfig in
    postinst to avoid collision with FAI's use of that variable.
    (Closes: #443850, #443866)
  * Reduce cronjob noise:
    - By not printing the output from "kill" (Closes: #406949)
    - By checking whether htdig is installed. Patch by Ted Percival
      <ted@midg3t.net> (Closes: #435201, #435316, #435993)
  * Fix various typos in manpages. Thanks to A. Costa <agcosta@gis.net>
    (Closes: #428712, #428713, #428714, #428715)

 -- Chris Lamb <chris@chris-lamb.co.uk>  Wed, 23 Apr 2008 21:23:38 +0100

htdig (1:3.2.0b6-5) unstable; urgency=low

  * QA upload to fix English usage and translations
  * Debconf templates and debian/control reviewed by the debian-l10n-
    english team as part of the Smith review project. Closes: #461014
  * [Debconf translation updates]
  * Galician. Closes: #461748
  * Japanese. Closes: #461941
  * Vietnamese. Closes: #462380
  * Finnish. Closes: #462684
  * Norwegian Bokmål. Closes: #462797
  * German. Closes: #462806
  * Russian. Closes: #463726
  * Portuguese. Closes: #463960
  * Czech. Closes: #464038
  * French. Closes: #464119
  * Lintian fixes:
  * Remove some installed empty directories in htdig-doc:
    - examples/xmlsearch
    - examples/examples/xmlsearch
    - examples/php-wrapper/conf
    - examples/htwrapper/.sniffdir
  * No longer ignore errors from "make distclean" in the clean target

 -- Christian Perrier <bubulle@debian.org>  Sat, 09 Feb 2008 11:04:31 +0100

htdig (1:3.2.0b6-4) unstable; urgency=high

  * QA upload by the testing-security team
  * Fix XSS in htsearch by not displaying the sort type in
    htsearch/Display.cc and libhtdig/ResultFetch.cc anymore, if it is
    unrecognised (Closes: #453278) Thanks to William Grant
    Fixes: CVE-2007-6110

 -- Steffen Joeris <white@debian.org>  Sun, 02 Dec 2007 08:21:04 +0000

htdig (1:3.2.0b6-3.1) unstable; urgency=low

  * Non-maintainer upload to fix pending l10n issues.
  * Debconf translations:
    - German. Closes: #407180
    - Czech. Closes: #408723

 -- Christian Perrier <bubulle@debian.org>  Mon, 26 Feb 2007 18:25:25 +0100

htdig (1:3.2.0b6-3) unstable; urgency=low

  * QA upload.
  * Fix formatting typo in htdig.1 (Closes: #401589)
  * Add updated japanese po file (Closes: #400009)
  * Add russian po file (Closes: #404428)
  * Adjust priority to match override file (disparities)

 -- Michael Ablassmeier <abi@debian.org>  Mon,  8 Jan 2007 10:18:50 +0100

htdig (1:3.2.0b6-2) unstable; urgency=low

  * QA upload.
  * Update French debconf translation (Closes: #384009).
  * Update Dutch debconf translation (Closes: #387657).

 -- Luk Claes <luk@debian.org>  Sun,  1 Oct 2006 17:11:53 +0200

htdig (1:3.2.0b6-1) unstable; urgency=medium

  * QA upload.
  * Set maintainer to the QA Group.
  * New upstream release.
  * Urgency medium, since this resolves RC bugs.
  * Upload to unstable a package based on a never-uploaded package available
    at http://users.linuxbourg.ch/ribnitz/debian, created by the original
    htdig maintainer Robert Ribnitz.
  * Revert renaming of the never-uploaded package to htdig3.2. The htdig 3.1
    series is hopelessly uninstallable, so there is no need to maintain
    separate branches.
  * Apply various patches (including security fixes) borrowed from the Fedora
    htdig package.
  * Revamp and simplify debconf and packaging scripts. Also remove much
    pre-Sarge upgrade cruft.
  * Don't use debconf as a registry; keep using /etc/default/htdig instead.
  * This upload should resolve the follwing issues:
    + htdig no longer depends on libdb2.
      (Closes: #357642, #361578, #308447, #360475)
    + htdig now depends on lockfile-progs. (Closes: #364022)
    + rundig doesn't break when using an alt config file. (Closes: #284467)
    + The manpages now point to the right HTML directory. (Closes: #171243)
    + Fix debconf template typos. (Closes: #311905)
    + Don't recommend a webserver. (Closes: #122698)
    + Update Russian translation, add Vietnamese translation.
      (Closes: #311904, #361651)
    + Don't install search.html to /var/www in postinst anymore. Since this
      file has a generic name, users of older packages will have to clean it
      up themselves. (Closes: #348445)
    + Rewrite debconf template to make clear that the question is not whether
      or not to run htnotify, but whether or not to run rundig. htnotify is
      automatically run from rundig. However, I've patched rundig so that this
      behaviour can be changed. See the README.Debian.
      (Closes: #289661, #295963, #299648)
    + Document how to prevent rundig from sending the admin mail every day.
      (Closes: #51979)
  * Close bugs fixed in past NMUs:
    (Closes: #242807, #340994, #231534, #235779)

 -- Christopher Martin <chrsmrtn@debian.org>  Sun, 23 Jul 2006 20:41:45 -0400

htdig (1:3.2.0b6-1) UNRELEASED; urgency=low

  * New upstream relese

  * Package renamed to htdig3.2 in a move to provide a stable, reliable
    package for debian Sarge (Which will again be called 'htdig', and be a
    package of the 3.1.6 release).
  * Problem of robots.txt compliance has been solved by upstream
   (Closes: #242807)
  * Added check whether user wants to run htnotify in the cron script. Not the
    best solution, but should work till a better one is found.
    (Closes: #230214)
  * Applied the htmerge patch for htdig 3.2.0b6, supplied by D. van der Vliet.
    This should result in much better memory usage on merging databases with
    htmerge (4% compared to 90% before the patch)
  * Thanks to Jan Outrata we now have an up to date Czech translation
    (Closes: #260380)
  * Removed the unnecessary dependency on the newt libraries.
  * Fixed a condition allowing the removal of htdig databases in the
    /etc/htdig directory (in postrm) when the script is called with the
    'purge' option.
  * Corrected french debconf localisation (.po) file. shuhaitez->souhaitez.
    Thanks to Benoit Sibaud for discovering the typo.
  * Added Russian and Japanese Debconf translations from the stable version.
  * Renamed Spanish po-file to es.po

 -- Robert Ribnitz <ribnitz@linuxbourg.ch>  Mon, 14 Feb 2005 23:46:28 +0100

htdig (1:3.2.0b5-6) UNRELEASED; urgency=low

  * Fixed a typo in the manual page for htsearch (Closes: #139926)

  * Added Spanish debconf translation (Thanks to Francesc Gordillo i Cortinez
    and Eduard Hawlitschek)

  * Added Italian debconf translation (Thanks to Alessandro Rimoldi)

  * Added Czech debconf translation (Thanks to Martin Kerbert)
  
  * Patched Debconf Script to check for the existence of the database files
    before issuing a warning about their changed location ( Thanks to James
    Cameron, closes #248460) 

 -- Robert Ribnitz <ribnitz@linuxbourg.ch>  Mon, 24 May 2004 12:47:28 +0200

htdig (1:3.2.0b5-5) UNRELEASED; urgency=low

  * Enabled support for GNU LD

  * Added Danish debconf translation (Thanks to Morten Brix Petersen) 
    (Closes: #234228)

  * Added German debconf translation (Thanks to Alexander List)
    (Closes: #243701)
  
  * Added Russian debconf translation (Thanks to Denis Barbier) 
    (Closes: #137658)
  
  * Updated French debconf translation (Thanks to Christian Perrier and the
    French Internationalisation Mailinglist) (Closes: #244057)

 -- Robert Ribnitz <ribnitz@linuxbourg.ch>  Tue, 20 Apr 2004 12:11:34 +0200

htdig (1:3.2.0b5-4) UNRELEASED; urgency=low

  * Fixed CPPFLAGS in rules (now CXXFLAGS), should get rid of a few warnings
  
 -- Robert Ribnitz <ribnitz@linuxbourg.ch>  Sat, 27 Mar 2004 18:42:04 +0100

htdig (1:3.2.0b5-3) UNRELEASED; urgency=low

  * Removed unneded depends on libstc++

  * Added build depends for flex and bison

  * Added Conflicts on libmifluz0 (Closes: #232076). Since htdig convers the
    same focus of interest than mifluz, no one should be using both at the
    same time.

  * Cleanup of lintian problems (.cvsignore in documentation)

  * Adapted cron scripts (removed now illegal rundig option -i and added option
    to generate statistics -s) (Closes: #233466)

  * Corrected a typo in a manpage
  
  * Enabled PIC mode to try to get rid of some notorious RC bugs (and bulid
    problems for some architectures)

 -- Robert Ribnitz <ribnitz@linuxbourg.ch>  Wed, 24 Mar 2004 23:40:53 +0100

htdig (1:3.2.0b5-2) UNRELEASED; urgency=medium

  * Added  build-dependecy on zlib1g-dev and dependency on zlib1g. Their absence
    caused build errors. (Closes: #231985)

  * Major overhaul of manpages. Rewrote htdig, and htfuzzy, added htstat.
  

 -- Robert Ribnitz <ribnitz@linuxbourg.ch>  Wed, 11 Feb 2004 19:30:57 +0100

htdig (1:3.2.0b5-1) UNRELEASED; urgency=low

  * New upstream relase (beta, but should me better than the current 'stable')

  * Adaptations and french translation of the debconf templates (Thanks to
    Philippe Batailler for translating) (Closes: #226970)

  * Race condition / memory leak from 3.1.6-7 seems to no longer be present.
    Indexing of a 'large' (150k documents) collection did not show unusual
    memory consuption or core dumping. (Closes: #200876, #200734)

  * Removed build-dependecy on libdb2-dev, since htdig comes with its own
    version of the db libraries.
  
  * Added proper build dependecies to libtool, autoconf and automake 

 -- Robert Ribnitz <ribnitz@linuxbourg.ch>  Tue, 20 Jan 2004 14:04:50 +0100

htdig (1:3.1.6-11.1) unstable; urgency=low

  * Non-Maintainer Upload.
  * Fixed "FTBFS: new, more restrictive coreutils cp", closes: #340994.
    Patch by Roland Stigge <stigge@antcom.de>.

 -- Anibal Monsalve Salazar <anibal@debian.org>  Wed, 11 Jan 2006 20:16:04 +1100

htdig (1:3.1.6-11) unstable; urgency=high

  * Applied patch by Phil Knirsch to fix cross-site scripting
    vulnerability [htsearch/htsearch.cc, htfuzzy/htfuzzy.cc,
    htmerge/htmerge.cc, CAN-2005-0085]. Patch supplied by the
    Debian Security Team.
  * Added dependecy on sed (Closes: #276398). Thanks to
    Matus Uhlar <uhlar@fantomas.sk>.
  * Updated Debian Standards Version to 3.6.1 

 -- Robert Ribnitz <ribnitz@linuxbourg.ch>  Thu, 10 Feb 2005 23:58:28 +0100

htdig (1:3.1.6-10.2) unstable; urgency=low

  * Non-Maintainer Upload.
  * Fixed "cron scripts are broken"
    Closes: #268870, #269080, #271971, #278315, #281822, #290840
    Patches by:
    Robert Grimm <rob@robgri.de>,
    C.Y.M. <syphir@syphir.sytes.net>,
    Carsten Luedtke <acid_man@web.de>,
    Michael Schnyder <m.schnyder@tr51.org>,
    John Summerfield <summer@ComputerDatasafe.com.au> and
    Sanjoy Mahajan <sanjoy@mrao.cam.ac.uk>
  * Fixed "FTBFS (amd64/gcc-4.0): cast from 'Object*' to 'int' loses
    precision" (Closes: #286418).
    Patch by Andreas Jochens <aj@andaco.de>
  * Fixed "Spanish debconf translation should be named es.po"
    (Closes: #275290).
  * Czech translation of htdig debconf messages.
    Closes: #260380, #266577, #271461
    Patches by Jan Outrata <outrataj@upcase.inf.upol.cz>
  * Japanese po-debconf template translation (Closes: #281152).
    Patch by Hideki Yamane <henrich@samba.gr.jp>

 -- Anibal Monsalve Salazar <anibal@debian.org>  Sat, 05 Feb 2005 19:43:11 +1100

htdig (1:3.1.6-10.1) unstable; urgency=low

  * Non-Maintainer Upload by Gunnar Wolf <gwolf@debian.org>
  * Substituted tabs by sets of spaces in description (Closes: #269330)
  * Re-wrapped the extended description so it would fit in 80-column
    terminals
  * Updated db/dist/config.{guess,sub} with the current version

 -- Gunnar Wolf <gwolf@debian.org>  Wed,  1 Sep 2004 01:14:22 -0500

htdig (1:3.1.6-10) unstable; urgency=low

  * Updated Danish debconf translation, in time for Sarge inclusion.
    Thanks go to Morten Brix Pedersen for translating. (Closes: #263834)
  * Updated the pdf-parser script to find the correct parser (now using awk,
    instead of grep). Also updated the call syntax for those people who want
    to use Adobe Acrobat, instead of XPdf. Checks have also been integrated,
    so the script now checks for the existence of the parser specified in the
    config file.(Thanks go to Anders Eriksson, Gabriele Gallacci, and Ben
    Finney, closes: #158431, #158431, #149664)
  * Finetuning of postrm script, now checks for the existence of /etc/default
    when purging the installation.
  * Philippe Batailler provided an updated French debconf translation.
    Thanks for translating (Closes: #265438)
  * Minor adaptations of debconf config script so that changed values in the
    file '/etc/default/htdig' get fed back to debconf. This should bring the
    package more in line with policy requirements.
  * Token location for indicating the need of a full refresh changed. The
    token is set by cron.weekly, and if it is present, the first cron.daily
    run will do a full refresh instead of an incremental one. Previously,
    the token file was located in '/etc' where it did not really belong. Thanks
    to Brian McGroarty for drawing attention to this problem.(Closes: #199276)
  * Since version 1:3.1.6-8, htdig again links against the debian-provided
    libdb2(-dev) libraries. (cf. changelog entry for 1:3.1.6-8). Upgrading to
    a more recent version of the Berkeley DB libraries is impossible, since it
    would imply re-testing and re-optimising by upstream. (Closes: #215240)
  * Rewrote the blurb as to include approxoximate disk space requirements.
    Thanks to Ben Darnel for the tip. (Closes: #77283)
  * Cron scripts have been adapted to no longer notify the postmaster every
    day of their running. (Closes: #52760)
  * htdig now recommends its documentation rather than suggesting it. This was
    changed because the html documentation is often more up to date than the
    manpages.
  * Added dependency on GNU awk since awk is used at various places in the
    package. (thanks to Morten Brix Pedersen for noticing, closes: #267640) 

 -- Robert Ribnitz <ribnitz@linuxbourg.ch>  Mon, 23 Aug 2004 21:23:12 +0200

htdig (1:3.1.6-9) unstable; urgency=low

  * Cleaned up Depends, removed 'debconf' as this is now handled by
    ${misc:Depends}.
  * Appended zlib1g-dev to the list of build depends. This fixes
    a warning with the automatic package bulider.
  * Fixed time-skew issues in the buildscript. Hopefully it will allow a
    build on m68k architecture now.
  * Debconf compatibilty level 4, since 3.1.6-8.
  * Fixed error in rundig manpage.
  * Fixed an error in cron.daily and cron.weekly scripts. (Closes: #263389)
  * Modifications in the cron scripts starting with 1:3.1.6-8 make the problem
    described in bug #60633 no longer apply. If htdig is removed (but not
    purged), the file /etc/default/htdig will no longer be present on the
    system, so the cronscripts will drop out early on. (Thanks to Andrew Gray,
    Closes: #60633)
  * Adapted postrm to remove cronjobs if they were left over. (Thanks to Mark
    Sysmonds, closes: #79270)
  * Fun with man pages. Brought the htsearch man page up to date. (Closes:
    #139926)
  * Running htnotify can now be disabled. Since 3.1.6-8 the results of the
    question are used and queried by the cron jobs. Thanks to Johannes Rohr
    for reporting (Closes: #230214)
  * Included Dutch debconf translation provided by Luk Claes and the Dutch
    l10n Team. Thank them for the translation. (Closes: #263723)
  * Minor adaptations to cronjobs and postrm.

 -- Robert Ribnitz <ribnitz@linuxbourg.ch>  Fri,  6 Aug 2004 11:46:16 +0200


htdig (1:3.1.6-8) unstable; urgency=low

  * The htdig package has been split into a stable release featuring htdig
    3.1.6 (this package), which should be fit for production use, and an
    unstable release based on the 3.2.0 beta code ('htdig3.2'). This was done
    as it became apparent that htdig 3.2.0 would probably not be ready for
    Sarge release, and even then it was doubtful it would be as fast as the
    old stable release. (Closes: #244867)
  * Patched htfuzzy so that it does not die on words of non-alpha characters
    only when searching for numbers is enabled and a soundex search is
    performed. Thanks to Alex Kiesel for this patch. This is a patch provided
    by the Ht://Dig developer team.
  * Patched htnotify race condition. Htnotify would sit there eating up
    memory, appending newlines to an empty string. As Martin Kraemer
    discovered, an additional check fixes the problem. This patch is also
    provided by the Ht://Dig developer team.
  * Added a patch that allows the correct skipping of JavaScript code. Work
    attributed to Gilles Detillieux. Patch provided by upstream.
  * Have external parsers check for the maximal Document size (max_doc_size).
    Work attributed to Gilles Detillieux. Patch provided by upstream.
  * Fixed the rating of percent values. For some unknown reason, including a
    percent value would rate the document higher than it should. Patch
    provided by upstream (and not attributable to a person).
  * A set of patches brings the HTML documentation up to date, fixing errors
    and clearing ambinguities. Thanks to Gilles Detillieux for these patches.
    Also provided by the upstream developers.
  * Updated the scripts to honor the decision whether htnotify should be run.
  * Removed unnecessary dependency on the newt libraries.
  * Took the debconf translations from the unstable package (they have not
    changed much), and debugged the italian translation so that it no longer
    produced errors.
  * Fixed 'rundig' so that the '-i' option is only passed to htdig, and no
    longer to 'htpurge' and 'htnotify' where it would produce an error.
  * More fun with 'rundig', adapted it to the suggestion of Emmanuel Decaen so
    it would only recognise database_dir at the start of a line, preventing it
    from erroneously using a commented out setting. (Closes: #139922)
  * Brought the rundig manpage up to date to reflect the '-i' option, as it
    works now. Thanks to Francesco Potorti and Ludovic Drolez for reporting.
    (Closes: #162397, #138304)
  * Fixed a condition whereby the databases in /etc/htdig would not be
    removed when the package was uninstalled. (Closes: #244774)
  * Corrected a typo in the french debconf localisation (.po) file. Thanks to
    Benoit Sibaud for discovering the misspelled word. (Closes: #249919)
  * Added a check for file existence in the config script. Thanks to Frank
    Pavageau and James Cameron for their effort (Closes: #248460)
  * Fixed recommends to get more lintian-clean: 'wwwoffle, apache | httpd'
    instead of 'wwwoffle | httpd', esp. since wwwoffle does not provide httpd.
  * Removed 'debian/conffiles', as scripts and files in /etc are automatically
    treated as configuration files.
  * Adapted the copyright file to reflect the current licensing situation.
  * Externally linked against the Berkeley DB libraries, rather than using the
    old libraries ht://Dig came with (2.6.4). This resolves some licensing
    worries. It adds a build dependency of the package against 'libdb2-dev',
    and a runtime dependency against 'libdb2', however.
  * Cleaned up the templates file, removing unused  entries.
  * Adapted cron scripts to no longer use the debconf-gathered values
    directly. Instead, an interim file is used (/etc/default/htdig).

 -- Robert Ribnitz <ribnitz@linuxbourg.ch>  Mon,  2 Aug 2004 01:55:28 +0200

htdig (3.1.6-7) unstable; urgency=low

  * A patch provided by Christian Perrier fixes some dependency issues and
    improves on gettext support. Christian Perrier also proofread the French
    Translation (Closes: #202725)
  
  * The package no longer suggests word2x, since catdoc can handle the
    functionality, and that word2x is a candidate for removal from debian.
    (Closes: #214204)

 -- Robert Ribnitz <ribnitz@linuxbourg.ch>  Sat, 18 Oct 2003 17:25:15 +0200

htdig (3.1.6-6) unstable; urgency=low

  * A patch supplied by Adam Heath now allows for filenames to contain spaces
    (Closes: #188256)
  
  * Changed dependency from 'libnewt0.51' to 'libnewt0.51 | libnewt0' to ease
    problems for 'woody' users.
  
  * Integrated French translation for debconf. Credits for the translation go to
    Phillippe Batailler. Had it not been for the help of Denis Barbier who
    submitted it to the BTS, we would not have the French translation now, so
    thanks go to him as well. (Closes: #151726)
  
  * Proofread control file, to get rid of 'optinal', which I didn't find. So I
    guess it must have been fixed, and the bug not yet closed.
    (Closes: #155964)

 -- Robert Ribnitz <ribnitz@linuxbourg.ch>  Fri, 11 Jul 2003 16:36:26 +0200

htdig (3.1.6-5) unstable; urgency=low

  * Amended the htdig-pdfparser script as outlined in pod's idea, eliminating an
    (unnecessary) 'grep' in the script (Closes: #196916) 

  * renamed dependency from libnewt0 to libnewt 0.51 to fix packaging errors
    (libnewt0 no longer in sarge/sid, as this is named libnewt0.51)

 -- Robert Ribnitz <ribnitz@linuxbourg.ch>  Wed, 25 Jun 2003 08:29:40 +0200

htdig (3.1.6-4) unstable; urgency=low

  * New maintainer
  * Patch provided by Norman Jordan fixes local indexing problem
    (Closes: #113857)
  * Dependency on libnewt0 added (Closes: #196202)
 
 -- Robert Ribnitz <ribnitz@linuxbourg.ch>  Tue, 24 Jun 2003 21:54:56 +0200

htdig (3.1.6-3) unstable; urgency=low

  * Fixed typo in postrm, quick-and-dirty fix for debconf 
    problem (Closes: #135663)
  * Updated database location in htdigconfig script (Closes: #135259)
  * Updated rundig script for DBDIR and TMPDIR locations
    (Closes: #81039, #117887)

 -- Stijn de Bekker <stijn@debian.org>  Mon, 25 Feb 2002 19:56:03 +0100

htdig (3.1.6-2) unstable; urgency=low

  * Added debconf support for interaction (Closes: #55212)
  * Added a debconf warning for the change in database path when
    upgrading from version <3.1.5-3  (Closes: #133867)
  * Marked /etc/htdig/long.shtml, /etc/htdig/short.shtml and
    /etc/htdig/wrapper.html as conffiles (Closes: #110574, #132132)
  * Fixed /usr/local/acroread problems with a special htdig pdf 
    script (Closes: #64385, #67341, #107504, #123398, #124345)

 -- Stijn de Bekker <stijn@debian.org>  Sat, 16 Feb 2002 12:11:28 +0100

htdig (3.1.6-1) unstable; urgency=low

  * New upstream release (fixes Debian version, closes: #125387)
  * Fixed spelling errors (except RPM fixes, closes: #126964)

 -- Stijn de Bekker <stijn@debian.org>  Wed,  6 Feb 2002 21:27:21 +0100

htdig (3.1.6-0.snapshot011118) unstable; urgency=low

  * New upstream release (Closes: #52945, #70700, #105045)
  * Changed location of index files from /var/spool to /var/lib to
    reflect the FHS (Closes: #119111)
  * Added .bz2 to list of bad_extensions (Closes: #118552)
  * Changed location of htdig images to /var/www/htdig
  * Changed behaviour to a create a full index weekly and an 
    incremental index daily (Closes: #114979)
  * Package now recommends httpd or wwwoffle (Closes: #85204)

 -- Stijn de Bekker <stijn@debian.org>  Sun, 25 Nov 2001 16:47:17 +0100

htdig (3.1.5-3) unstable; urgency=high

  * New maintainer
  * Updated Standards-Version: 3.5.2
  * Fixes security bug; disable -c config option for cgi usage
    (Closes: #113682)
  * Changed the location of htdig documents and config files in manpages
    (Closes: #95991, #67388, #71133)

 -- Stijn de Bekker <stijn@debian.org>  Sat,  6 Oct 2001 13:45:11 +0200

htdig (3.1.5-2.1) frozen unstable; urgency=low

  * debian/control: s/perl5/perl/
  * debian/cron.daily: use locking strategy proposed by Ingo Saitz (closes:
    bug#70700)
  * debian/control: added dependency on lockfile-progs

 -- Marcelo E. Magallon <mmagallo@debian.org>  Sat, 17 Mar 2001 20:07:24 +0100

htdig (3.1.5-2) frozen unstable; urgency=low

  * Comment out the external_parsers line in the default htdig.conf
    (Closes: #59228)

 -- Gergely Madarasz <gorgo@sztaki.hu>  Tue, 29 Feb 2000 18:31:32 +0100

htdig (3.1.5-1) frozen unstable; urgency=high

  * New upstream security bugfix release (Closes: #59047, #59056)

 -- Gergely Madarasz <gorgo@sztaki.hu>  Mon, 28 Feb 2000 01:41:58 +0100

htdig (3.1.4-2) frozen unstable; urgency=medium

  * Test for the existence of /var/www before copying files there in the
    postinst. In certain cases htdig could install a /var/www _file_

 -- Gergely Madarasz <gorgo@sztaki.hu>  Wed, 23 Feb 2000 01:24:38 +0100

htdig (3.1.4-1) unstable; urgency=low

  * New upstream version

 -- Gergely Madarasz <gorgo@sztaki.hu>  Fri, 10 Dec 1999 16:52:52 +0100

htdig (3.1.3-1) unstable; urgency=low

  * New upstream version
  * Split htdig-doc package (Closes: #45684)
  * Install search.html when first installing the package to
    /var/www (Closes: #45897)

 -- Gergely Madarasz <gorgo@sztaki.hu>  Tue,  7 Dec 1999 16:04:22 +0100

htdig (3.1.2-9) unstable; urgency=low

  * Small bugfix (Closes: #45680)

 -- Gergely Madarasz <gorgo@sztaki.hu>  Tue, 21 Sep 1999 18:48:38 +0200

htdig (3.1.2-8) unstable; urgency=low

  * Fix postinst script (debhelper part was not included)

 -- Gergely Madarasz <gorgo@sztaki.hu>  Tue,  7 Sep 1999 21:20:34 +0200

htdig (3.1.2-7) unstable; urgency=low

  * FHS compliance
  * Standards: 3.0.1

 -- Gergely Madarasz <gorgo@sztaki.hu>  Tue,  7 Sep 1999 20:44:48 +0200

htdig (3.1.2-6) unstable; urgency=low

  * Change maintainer address
  * Build with libstdc++2.10
  * Remove -s from cron.daily run (Closes: #43075)
  * Fix inclusion of correct db.h header

 -- Gergely Madarasz <gorgo@sztaki.hu>  Sat, 28 Aug 1999 17:38:35 +0200

htdig (3.1.2-5) unstable; urgency=low

  * No changes, just build with tar 1.12-7

 -- Gergely Madarasz <gorgo@caesar.elte.hu>  Tue,  3 Aug 1999 18:08:47 +0200

htdig (3.1.2-4) unstable; urgency=low

  * /etc/cron.daily/htdig should be a conffile

 -- Gergely Madarasz <gorgo@caesar.elte.hu>  Tue,  3 Aug 1999 12:16:54 +0200

htdig (3.1.2-3) unstable; urgency=low

  * Add perl5 dependency for parse_doc.pl

 -- Gergely Madarasz <gorgo@caesar.elte.hu>  Mon,  2 Aug 1999 19:27:30 +0200

htdig (3.1.2-2) unstable; urgency=low

  * Fix build for glibc 2.0, modified patch from Roman Hodek (Closes: #41409)
  * Add a cron.daily file (Closes: #37205)
  * Add support for .doc, .ps and .pdf files, patch from Ray Dassen
    (Closes: #37206)

 -- Gergely Madarasz <gorgo@caesar.elte.hu>  Mon,  2 Aug 1999 18:24:12 +0200

htdig (3.1.2-1) unstable; urgency=low

  * New upstream release
  * Compile with glibc2.1, link against libdb.so.3 instead of libdb.so.2
  * Fix the location of the word2root and synonyms databases in the
    default htdig.conf (#34827, #34244)
  * Move the configuration file to /etc/htdig (#33316)
  * Fix for wishlist bug #31804
  * Some manpage updates

 -- Gergely Madarasz <gorgo@caesar.elte.hu>  Fri, 23 Apr 1999 18:56:49 +0200

htdig (3.1.1-1) unstable; urgency=low

  * Suggest acroread for pdf parsing support
  * htlib/DB2_db.cc: patch back to db2 2.4.6 API
  * New upstream release

 -- Gergely Madarasz <gorgo@caesar.elte.hu>  Wed, 24 Feb 1999 21:51:47 +0100

htdig (3.1.0b4-1) unstable; urgency=low

  * New upstream release

 -- Gergely Madarasz <gorgo@caesar.elte.hu>  Tue,  5 Jan 1999 03:54:09 +0100

htdig (3.1.0b1-1) unstable; urgency=low

  * Moved /usr/lib/htdig to /etc/htdig since those should be conffiles
    (adresses #25412)
  * Fixed hardcoded path to sort (#24159, #26704)
  * Fixed README (#26807)
  * Converted debian/rules to debhelper
  * Link against libdb2 instead of libgdbm
  * New upstream version

 -- Gergely Madarasz <gorgo@caesar.elte.hu>  Thu,  1 Oct 1998 23:40:45 +0200

htdig (3.0.8b2-3) unstable; urgency=low

  * no longer refers to gzipped GPL
  * remove shlibs.local
  * recompile with librx1g instead of rx1g

 -- Gergely Madarasz <gorgo@caesar.elte.hu>  Thu,  5 Mar 1998 18:35:04 +0100

htdig (3.0.8b2-2) unstable; urgency=low

  * added shlibs.local because of (possibly) rx1g bug
  * Link against rx1g (fixes #16615)
  * New maintainer

 -- Gergely Madarasz <gorgo@caesar.elte.hu>  Mon,  5 Jan 1998 21:21:53 +0100

htdig (3.0.8b2-1) unstable; urgency=low

  * New upstream version
  * Don't generate word db's when package is upgraded
  * Check for arguments to postinst (fixes #12613)
  * Pristine source
  * Upgraded to standards version 2.3.0.0

 -- Christian Schwarz <schwarz@debian.org>  Fri, 12 Sep 1997 01:51:35 +0200

htdig (3.0.8b1-2) unstable; urgency=low

  * Link dynamically against libgdbm and librx (fixes bug#12152).

 -- Christian Schwarz <schwarz@debian.org>  Mon, 18 Aug 1997 19:45:09 +0200

htdig (3.0.8b1-1) unstable; urgency=low

  * Initial Release.
  * Wrote manual pages for ht* commands.

 -- Christian Schwarz <schwarz@debian.org>  Mon, 21 Jul 1997 10:16:13 +0200
