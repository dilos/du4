#!/usr/bin/make -f

# Disable -fstack-protector-strong
export DEB_BUILD_MAINT_OPTIONS=hardening=-stackprotectorstrong
export CFLAGS+= -D__EXTENSIONS__ -std=c99

%:
	dh $@ --builddirectory=debian/build --no-parallel

override_dh_auto_configure:
	FORCE_UNSAFE_CONFIGURE=1 ./configure debian/build \
		--prefix=/usr \
		--libdir=\$${prefix}/lib/$(DEB_HOST_MULTIARCH) \
		--fsstnd=debian \
		--with-dynamic-ffi \
		--with-dynamic-modules \
		--with-module=gdbm \
		--with-module=berkeley-db \
		--with-module=pcre \
		--with-module=rawsock \
		--with-module=clx/new-clx \
		--with-module=bindings/glibc \
		--with-module=postgresql \
		--with-module=zlib \
		--with-module=dbus \
		--with-module=asdf

override_dh_install:
	dh_install
	# remove unwanted upstream doc/ files
	rm -f debian/clisp-doc/usr/share/doc/clisp/doc/clisp.1 \
		debian/clisp-doc/usr/share/doc/clisp/doc/clisp-link.1 \
		debian/clisp-doc/usr/share/doc/clisp/COPYRIGHT \
		debian/clisp-doc/usr/share/doc/clisp/GNU-GPL
	# Remove unwanted demos in /usr/lib (they are already shipped under /usr/share/doc/)
	rm -rf debian/clisp-module-clx/usr/lib/$(DEB_HOST_MULTIARCH)/clisp-*/clx/new-clx/demos/

override_dh_installchangelogs:
	dh_installchangelogs src/ChangeLog

override_dh_gencontrol-arch:
	# add FAS file format version to substvars
	debian/clisp/usr/bin/clisp -Efile utf-8 -B debian/clisp/usr/lib/$(DEB_HOST_MULTIARCH)/clisp-* -M debian/clisp/usr/lib/$(DEB_HOST_MULTIARCH)/clisp-*/base/lispinit.mem debian/fasl-version.lisp
	# add memfile hash to substvars
	echo "clisp:memfile-hash=clisp-memfile-hash-$$(debian/clisp/usr/bin/clisp -B debian/clisp/usr/lib/$(DEB_HOST_MULTIARCH)/clisp-* -memfile-hash)" >> debian/clisp.substvars
	dh_gencontrol -a
