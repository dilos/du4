#
#    Translators, if you are not familiar with the PO format, gettext
#    documentation is worth reading, especially sections dedicated to
#    this format, e.g. by running:
#         info -n '(gettext)PO Files'
#         info -n '(gettext)Header Entry'
#
#    Some information specific to po-debconf are available at
#            /usr/share/doc/po-debconf/README-trans
#         or http://www.debian.org/intl/l10n/po-debconf/README-trans
#
#    Developers do not need to manually edit POT or PO files.
#
msgid ""
msgstr ""
"Project-Id-Version: mandb\n"
"Report-Msgid-Bugs-To: man-db@packages.debian.org\n"
"POT-Creation-Date: 2008-04-26 13:39+0100\n"
"PO-Revision-Date: 2007-10-23 13:16+0100\n"
"Last-Translator: Bart Cornelis <cobaco@skolelinux.no>\n"
"Language-Team: debian-l10n-dutch <debian-l10n-dutch@lists.debian..org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Dutch\n"

#  Type: boolean
#  Description
#. Type: boolean
#. Description
#: ../templates:1001
msgid "Should man and mandb be installed 'setuid man'?"
msgstr "Wilt u man en mandb installeren met 'setuid man'?"

#  Type: boolean
#  Description
#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"The man and mandb program can be installed with the set-user-id bit set, so "
"that they will run with the permissions of the 'man' user. This allows "
"ordinary users to benefit from the caching of preformatted manual pages "
"('cat pages'), which may aid performance on slower machines."
msgstr ""
"De man en mandb programma's kunnen geinstalleerd worden met de set-user-id "
"bit ingesteld zodat ze uitgevoerd worden als de gebruiker 'man'. Dit geeft "
"gewone gebruikers de gelegenheid om van de tijdelijke opslag van "
"voorgeformatteerde handleidingpagina's ('cat pages') gebruik te maken, wat "
"de prestaties op tragere systemen kan verbeteren."

#  Type: boolean
#  Description
#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"Cached man pages only work if you are using an 80-column terminal, to avoid "
"one user causing cat pages to be saved at widths that would be inconvenient "
"for other users. If you use a wide terminal, you can force man pages to be "
"formatted to 80 columns anyway by setting MANWIDTH=80."
msgstr ""
"Tijdelijk opgeslagen man-pagina's werken enkel wanneer u een terminal met "
"een breedte van 80 gebruikt. Dit vermijdt dat één gebruiker cat-pagina's "
"opslaat met breedtes die niet geschikt zijn voor andere gebruikers. Wanneer "
"u een bredere terminal gebruikt kunt u een breedte van 80 karakters "
"afdwingen voor man-pagina's door 'MANWIDTH=80' in te stellen als "
"omgevingsvariabele."

#  Type: boolean
#  Description
#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"Enabling this feature may be a security risk, so it is disabled by default. "
"If in doubt, you should leave it disabled."
msgstr ""
"Het activeren van deze optie kan een veiligheidsrisico zijn; daarom staat "
"deze optie standaard uit. Bij twijfel kunt u deze optie best uit laten."
