From: Carbo Kuo <byvoid@byvoid.com>
Date: Thu, 25 Feb 2021 20:48:50 +0900
Subject: Fix a bug in the calculation of DictGroup::keyMaxLength.

The length should be the maximum of all sub-dictionaries in the dictionary group.
---
 src/DictGroup.cpp     | 16 ++++++++++++++--
 src/DictGroupTest.cpp | 32 ++++++++++++++++++++++++--------
 2 files changed, 38 insertions(+), 10 deletions(-)

diff --git a/src/DictGroup.cpp b/src/DictGroup.cpp
index 4ca9e33..c682e96 100644
--- a/src/DictGroup.cpp
+++ b/src/DictGroup.cpp
@@ -1,7 +1,7 @@
 /*
  * Open Chinese Convert
  *
- * Copyright 2010-2014 Carbo Kuo <byvoid@byvoid.com>
+ * Copyright 2010-2021 Carbo Kuo <byvoid@byvoid.com>
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
@@ -24,8 +24,20 @@
 
 using namespace opencc;
 
+namespace {
+
+size_t GetKeyMaxLength(const std::list<DictPtr>& dicts) {
+  size_t keyMaxLength = 0;
+  for (const DictPtr& dict : dicts) {
+    keyMaxLength = std::max(keyMaxLength, dict->KeyMaxLength());
+  }
+  return keyMaxLength;
+}
+
+} // namespace
+
 DictGroup::DictGroup(const std::list<DictPtr>& _dicts)
-    : keyMaxLength(0), dicts(_dicts) {}
+    : keyMaxLength(GetKeyMaxLength(_dicts)), dicts(_dicts) {}
 
 DictGroup::~DictGroup() {}
 
diff --git a/src/DictGroupTest.cpp b/src/DictGroupTest.cpp
index 7003506..c91731e 100644
--- a/src/DictGroupTest.cpp
+++ b/src/DictGroupTest.cpp
@@ -1,7 +1,7 @@
 /*
  * Open Chinese Convert
  *
- * Copyright 2015 Carbo Kuo <byvoid@byvoid.com>
+ * Copyright 2015-2021 Carbo Kuo <byvoid@byvoid.com>
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
@@ -25,15 +25,31 @@ protected:
   DictGroupTest() {}
 };
 
-TEST_F(DictGroupTest, SimpleGroupTest) {
+TEST_F(DictGroupTest, KeyMaxLength) {
   const DictGroupPtr& dictGroup = CreateDictGroupForConversion();
-  const auto& entry = dictGroup->Dict::MatchPrefix(utf8("Unknown"));
-  EXPECT_TRUE(entry.IsNull());
+  EXPECT_EQ(6, dictGroup->KeyMaxLength());
+  EXPECT_EQ(6, dictGroup->GetDicts().front()->KeyMaxLength());
+  EXPECT_EQ(3, dictGroup->GetDicts().back()->KeyMaxLength());
+}
 
-  const auto& matches = dictGroup->Dict::MatchAllPrefixes(utf8("干燥"));
-  EXPECT_EQ(2, matches.size());
-  EXPECT_EQ(utf8("乾燥"), matches.at(0)->GetDefault());
-  EXPECT_EQ(utf8("幹"), matches.at(1)->GetDefault());
+TEST_F(DictGroupTest, SimpleGroupTest) {
+  const DictGroupPtr& dictGroup = CreateDictGroupForConversion();
+  {
+    const auto& entry = dictGroup->Dict::MatchPrefix(utf8("Unknown"));
+    EXPECT_TRUE(entry.IsNull());
+  }
+  {
+    const auto& entry = dictGroup->Dict::MatchPrefix(utf8("里面"));
+    EXPECT_FALSE(entry.IsNull());
+    EXPECT_EQ(3, entry.Get()->KeyLength());
+    EXPECT_EQ(utf8("裏"), entry.Get()->GetDefault());
+  }
+  {
+    const auto& matches = dictGroup->Dict::MatchAllPrefixes(utf8("干燥"));
+    EXPECT_EQ(2, matches.size());
+    EXPECT_EQ(utf8("乾燥"), matches.at(0)->GetDefault());
+    EXPECT_EQ(utf8("幹"), matches.at(1)->GetDefault());
+  }
 }
 
 TEST_F(DictGroupTest, TaiwanPhraseGroupTest) {
