marisa (0.2.6-2+dilos1) unstable; urgency=medium

  * Build for DilOS.

 -- DilOS Team <dilos@dilos.org>  Sun, 29 Jan 2023 14:33:33 +0300

marisa (0.2.6-2) unstable; urgency=medium

  [ Steve Langasek ]
  * Make autopkgtests cross-test-friendly.

  [ Boyuan Yang ]
  * Rebuild with python3.9 only setup.
  * Bump Standards-Version to 4.5.1.

 -- Boyuan Yang <byang@debian.org>  Tue, 08 Dec 2020 11:55:00 -0500

marisa (0.2.6-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Bump Standards-Version to 4.5.0.
    + Bump debhelper compat to v13.
  * debian/shlibs: Bump version accordingly.

 -- Boyuan Yang <byang@debian.org>  Tue, 30 Jun 2020 09:56:03 -0400

marisa (0.2.5-6) unstable; urgency=high

  * debian/tests/: Drop python2-related autopkgtest as well.

 -- Boyuan Yang <byang@debian.org>  Mon, 23 Dec 2019 12:23:38 -0500

marisa (0.2.5-5) unstable; urgency=medium

  * Drop python2 package. (Closes: #936993)
  * debian/rules: Use chrpath to delete all RPATH inside shared
    library files.

 -- Boyuan Yang <byang@debian.org>  Sun, 22 Dec 2019 21:42:20 -0500

marisa (0.2.5-4) unstable; urgency=medium

  * debian/control:
    + Bump Standards-Version to 4.4.1.
  * debian/rules: Fix build around perl and gcc-9. Patch provided
    by Niko Tyni. (Closes: #942073)
  * Recover usage of dh_dwz again since the issue has been solved.

 -- Boyuan Yang <byang@debian.org>  Tue, 15 Oct 2019 09:46:57 -0400

marisa (0.2.5-3) unstable; urgency=medium

  * debian/control:
    + Bump Standards-Version to 4.4.0.
    + Bump debhelper compat to v12.
  * debian/copyright: Fix incorrect license name. (Closes: #920663)
  * debian/patches: Backport upstream patches till 20181102.
  * debian/rules: Temporarily stop calling dh_dwz when building.

 -- Boyuan Yang <byang@debian.org>  Wed, 10 Jul 2019 11:46:44 -0400

marisa (0.2.5-2) unstable; urgency=medium

  * Rebuild against gcc 8.
  * debian/control:
    + Update my uploader email address and use the @debian.org one.
    + Bump Standards-Version to 4.2.1 (no changes needed).
    + Remove deprecated X-Python3-Version field.
    + Update Vcs-* fields and use the repo under input-method-team group.
  * debian/rules: Use architecture.mk to provide DEB_HOST_MULTIARCH var.

 -- Boyuan Yang <byang@debian.org>  Wed, 12 Sep 2018 16:02:52 -0400

marisa (0.2.5-1) unstable; urgency=medium

  [ Mitsuya Shibata ]
  * add dh-python to Build-Depends
  * remove no longer needed XS-Testsuite field
  * fix typo

  [ Boyuan Yang ]
  * New upstream release with different upstream on GitHub.
    Closes: #900680.
  * debian: Apply "wrap-and-sort -abst" for unified format.
  * debian/control:
    - Add myself into uploaders list.
    - Bump Standards-Version to 4.1.4.
    - Add X-Python3-Version field as recommended.
    - Replace "Extra" package priority with "Optional" to fit policy
      requirement.
    - Use debian-input-method@lists.debian.org in maintainer field.
      Closes: #899868.
    - Fix typo in package description. Closes: #808276.
  * debian/copyright:
    - Refresh upstream copyright information.
    - Add information about my contribution to debian/ dir.
    - Use GitHub repository for upstream source field.
  * debian/watch:
    - Rewrite in v4 format.
    - Use GitHub s-yata/marisa-trie as upstream.
  * debian/rules:
    - Use "dh_missing --fail-missing".
    - Avoid manual invocation of dpkg-parsechangelog.
    - Enable full hardening.
    - Explicitly remove .la files with file installation.
  * debian/docs: Refresh file list.
  * debian/doc-base: Register HTML document for libmarisa-dev.
  * debian/shlibs: Add a libmarisa0.shlibs file to restrict lib
    dependency (>= 0.2.5).
  * debian/patches: Refresh patches.
    - Fix Alpha wordsize detection. Closes: #836374.
    - Add support for riscv64 wordsize detection. Closes: #898018.
    - Backport various commits from upstream trunk.

 -- Boyuan Yang <073plan@gmail.com>  Tue, 05 Jun 2018 15:45:30 +0800

marisa (0.2.4-8) unstable; urgency=medium

  [debian/control]
  * add ruby as a new dependency for ruby-marisa (Closes: #759527)
    Thanks to Lucas Kanashiro for the report.
  * set Maintainer as pkg-ime and add Uploaders me
  * split multi lines Depends and Build-Depends

  [debian/copyright]
  * fix invalid copyright information

  [debian/patches/support-mips64el.patch]
  * fix ftbfs on mips64el (Closes: #762001)
    Thanks to YunQiang Su for the report.

 -- Mitsuya Shibata <mty.shibata@gmail.com>  Thu, 18 Sep 2014 01:16:29 +0900

marisa (0.2.4-7) unstable; urgency=medium

  * Fix missing link for libmarisa-perl
  * Fix FTBFS on s390x/sparc64 (Closes: #739126, #755560)
    Thanks to Aurelien Jarno and Guo Yixuan for the report.
  * Support DEP8/autopkgtest

 -- Mitsuya Shibata <mty.shibata@gmail.com>  Thu, 31 Jul 2014 02:32:22 +0900

marisa (0.2.4-6) unstable; urgency=medium

  * support building with Ruby 2.0/2.1 (Closes:  #743989)
    Thanks to Christian Hofstaedtler for the report.

 -- Mitsuya Shibata <mty.shibata@gmail.com>  Sun, 13 Apr 2014 23:27:56 +0900

marisa (0.2.4-5) unstable; urgency=medium

  * Use dh-autoreconf instead of autotools-dev to fix FTBFS on ppc64el.
    Thanks to Logan Rosen for the report. (Closes: #736696)

 -- Mitsuya Shibata <mty.shibata@gmail.com>  Sun, 26 Jan 2014 15:13:59 +0900

marisa (0.2.4-4) unstable; urgency=low

  * Initial upload to Debian archive (Closes: #714453)
  * Added Vcs-Browser and Vcs-Git fields
  * Bump standards version to 3.9.5

 -- Mitsuya Shibata <mty.shibata@gmail.com>  Thu, 21 Nov 2013 22:03:51 +0900

marisa (0.2.4-3) unstable; urgency=low

  * Updated patch for DEP3.
  * Auto generate version for python/setup.py.
  * Fixed install path for ruby bindings.
  * Exec extconf.rb after building libmarisa.

 -- Mitsuya Shibata <mty.shibata@gmail.com>  Sun, 17 Nov 2013 18:56:22 +0900

marisa (0.2.4-2) unstable; urgency=low

  * Fixed problems pointed out by sponsor.
    - Removed full stops from short descriptions.
    - Renamed perl package name as per Perl Policy §4.2.
    - Changed path of perl modules.
    - Refined compile options.
    - Fixed dpkg-gencontrol warnings.
    - Added manpages.
  * module-version-for-egg-inof.patch
    - For lintian4py warning, python module version in setup.py is specified.

 -- Mitsuya Shibata <mty.shibata@gmail.com>  Wed, 14 Aug 2013 19:29:20 +0900

marisa (0.2.4-1) unstable; urgency=low

  * Initial release (Closes: #714453)

 -- Mitsuya Shibata <mty.shibata@gmail.com>  Sat, 29 Jun 2013 10:53:00 +0900
