#!/usr/bin/make -f
# Copyright (C) 2009, 2016 Julian Andres Klode <jak@debian.org>
#
# Free Software, licensed under the GPL-2 or (at your option) any later version.
export DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)
export DEB_HOST_ARCH      ?= $(shell dpkg-architecture -qDEB_HOST_ARCH)

export SHELL = /bin/bash

#export DEB_LDFLAGS_MAINT_APPEND := -Wl,--as-needed
export DEB_BUILD_MAINT_OPTIONS := hardening=+all

DEB_CPPFLAGS_MAINT_APPEND := -D_REENTRANT -D_POSIX_PTHREAD_SEMANTICS
DEB_CPPFLAGS_MAINT_APPEND += -D__USE_GNU
# for GDB
#CFLAGS += -O0 -ggdb3
# end
#DEB_CXXFLAGS_MAINT_APPEND += -g -gdwarf-2 -fno-omit-frame-pointer
#DEB_CFLAGS_MAINT_APPEND += -g -gdwarf-2 -fno-omit-frame-pointer
#ifeq ($(DEB_HOST_ARCH),solaris-i386)
#DEB_CXXFLAGS_MAINT_APPEND += -msave-args
#DEB_CFLAGS_MAINT_APPEND += -msave-args
#endif
#ifeq ($(DEB_HOST_ARCH),solaris-sparc)
#endif

#export DEB_CPPFLAGS_MAINT_APPEND DEB_CXXFLAGS_MAINT_APPEND DEB_CFLAGS_MAINT_APPEND
export DEB_CPPFLAGS_MAINT_APPEND
#export DEB_LDFLAGS_MAINT_APPEND := -lresolv

# do not fail as we are just experimenting with symbol files for now
export DPKG_GENSYMBOLS_CHECK_LEVEL=0

export CTEST_OUTPUT_ON_FAILURE=1


ifneq ($(filter nocheck,$(DEB_BUILD_OPTIONS)),)
	configure_test_flags = -DWITH_TESTS=OFF
else
	configure_test_flags =
endif

%:
	dh $@ --parallel

override_dh_clean:
	cp COPYING debian/copyright
	dh_clean

override_dh_install-arch:
	dh_install -papt-utils -X/dump
	dh_install -papt -Xmethods/curl -Xmethods/curl+https -Xmethods/curl+http
	dh_install --remaining
	install -m 644 debian/apt.conf.autoremove debian/apt/etc/apt/apt.conf.d/01autoremove
#	install -m 755 debian/apt.auto-removal.sh debian/apt/etc/kernel/postinst.d/apt-auto-removal

#override_dh_gencontrol:
#	dh_gencontrol -- -Vapt:keyring="$(shell ./vendor/getinfo keyring-package)"

override_dh_installcron:
	dh_installcron --name=apt-compat

#override_dh_installsystemd:
#	# Do not restart "apt-daily.service" because this script runs
#	# unattended-upgrades. So if apt itself is upgraded as part of
#	# an unattended-upgrades run it would kill itself
#	dh_installsystemd -papt apt-daily.timer apt-daily-upgrade.timer
#	dh_installsystemd -papt --no-restart-on-upgrade --no-restart-after-upgrade --no-start apt-daily.service apt-daily-upgrade.service
#	dh_installsystemd --remaining-packages

override_dh_auto_configure-arch: flags=-DWITH_DOC=OFF -DCMAKE_INSTALL_LIBEXECDIR=/usr/lib -DCMAKE_INSTALL_LIBDIR=/usr/lib/$(DEB_HOST_MULTIARCH) -G"Unix Makefiles"
override_dh_auto_configure-indep: flags=-DWITH_DOC=ON
override_dh_auto_configure-arch override_dh_auto_configure-indep:
	dh_auto_configure -- $(flags) $(configure_test_flags)

override_dh_shlibdeps:
	dh_shlibdeps -ldebian/libapt-pkg6.0/usr/lib/$(DEB_HOST_MULTIARCH)

override_dh_strip:
	dh_strip -a --no-ctfconvert=libapt-private.so.0.0.0 \
		--no-ctfconvert=mirror --no-ctfconvert=copy \
		--no-ctfconvert=ftp --no-ctfconvert=http \
		--no-ctfconvert=file --no-ctfconvert=apt-cache \
		--no-ctfconvert=apt-ftparchive
