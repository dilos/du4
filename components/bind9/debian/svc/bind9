#!/bin/sh

#
# Copyright (c) 2007, 2012, Oracle and/or its affiliates. All rights reserved.
# Copyright 2016 Hans Rosenfeld <rosenfeld@grumpf.hope-2000.org>
#
# smf_method(5) start/stop script required for server DNS

. /lib/svc/share/smf_include.sh

[ -f /etc/default/bind9 ] && . /etc/default/bind9

ETCDIR=/etc/bind
RUNDIR=/var/run/named

get_config ()
{
    configuration_file=${ETCDIR}/named.conf
    rndc_config_file=${ETCDIR}/rndc.conf
    rndc_key_file=${ETCDIR}/rndc.key
    rndc_cmd_opts="-a"
    cmdopts=""
    checkopts=""
    properties="debug_level configuration_file server"

    for prop in $properties
    do
	value=`/usr/bin/svcprop -p options/${prop} ${SMF_FMRI}`
	if [ -z "${value}" -o "${value}" = '""' ]; then
	    continue;
	fi

	case $prop in
	'debug_level')
	    if [ ${value} -gt 0 ]; then
		cmdopts="${cmdopts} -d ${value}"
	    fi
	    ;;
	'configuration_file')
	    cmdopts="${cmdopts} -c ${value}"
	    checkopts="${checkopts} ${value}"
	    configuration_file=${value};
	    ;;
	'server')
	    set -- `echo ${value} | /usr/bin/sed -e  's/\\\\//g'`
	    server=$@
	    ;;
	esac
    done

    configuration_dir=$(sed -n -e \
	's,^[[:space:]]*directory.*"\(.*\)";,\1,p' \
	${configuration_file})
    [ -z "${configuration_dir}" ] && configuration_dir=${ETCDIR}

    configuration_files=$(sed -n -e \
        "s,^[[:space:]]*file.*\"\(.*\)\";,${configuration_dir}/\1,p" \
        ${configuration_file} | sort -u)
    configuration_files="${configuration_files} ${configuration_file}"    
}

result=${SMF_EXIT_OK}

# Read command line arguments
method="$1"		# %m
instance="$2" 		# %i
contract="$3"		# %{restarter/contract}

# Set defaults; SMF_FMRI should have been set, but just in case.
if [ -z "$SMF_FMRI" ]; then
    SMF_FMRI="svc:/network/bind9:${instance}"
fi
server="/usr/sbin/named"
checkconf="/usr/sbin/named-checkconf"
I=`/usr/bin/basename $0`
cmduser=bind

case "$method" in
'start')
    get_config

    # Check configuration file exists.
    if [ ! -f ${configuration_file} ]; then
        msg="$I: Configuration file ${configuration_file} does not exist!"
        echo ${msg} >&2
        /usr/bin/logger -p daemon.error ${msg}
        # dns-server should be placed in maintenance state.
        result=${SMF_EXIT_ERR_CONFIG}
    fi

    mkdir -p ${RUNDIR}
    chown ${cmduser}:${cmduser} ${RUNDIR}

    # Check if the rndc config file exists.
    if [ ! -f ${rndc_config_file} ]; then
        # If not, check if the default rndc key file exists.
        if [ ! -f ${rndc_key_file} ]; then
            echo "$I: Creating default rndc key file: ${rndc_key_file}." >&2
            /usr/sbin/rndc-confgen ${rndc_cmd_opts}
            if [ $? -ne 0 ]; then
                echo "$I : Warning: rndc configuration failed! Use of 'rndc' to" \
		    "control 'named' may fail and 'named' may report further error" \
		    "messages to the system log. This is not fatal. For more" \
		    "information see rndc(1M) and rndc-confgen(1M)." >&2
            fi
        fi
    fi

    if [ ${result} = ${SMF_EXIT_OK} ]; then
	${checkconf} -z ${checkopts}
	result=$?
	if [ $result -ne 0 ]; then
            msg="$I: named-checkconf failed to verify configuration"
            echo ${msg} >&2
            /usr/bin/logger -p daemon.error ${msg}
            # dns-server should be placed in maintenance state.
            exit ${SMF_EXIT_ERR_CONFIG}
	fi
    fi

    cmdopts="${OPTIONS} -u ${cmduser} ${cmdopts}"
    if [ ${result} = ${SMF_EXIT_OK} ]; then
	echo "$I: Executing: ${server} ${cmdopts}"
	# Change the current directory to the user's home directory
	HOMEDIR=$(grep ${cmduser} /etc/passwd | cut -d ":" -f6)
	mkdir -p ${HOMEDIR}
	chown -R ${cmduser}:${cmduser} ${HOMEDIR}
	cd ${HOMEDIR}

	# Execute named(1M) with relevant command line options.  Note
	# the server forks before reading named.conf(4) and so a
	# good exit code here does not mean the service is ready.
	ppriv -s A-all -s A+basic,net_privaddr,file_dac_read,file_dac_search,sys_resource,proc_chroot,proc_setid -e ${server} ${cmdopts}
	result=$?
	if [ $result -ne 0 ]; then
	    echo "$I : start failed! Check syslog for further information." >&2
        fi
    fi
    ;;
'stop')
    get_config

    smf_kill_contract ${contract} TERM 1
    [ $? -ne 0 ] && exit 1

    ;;
*)
    echo "Usage: $I [stop|start] <instance>" >&2
    exit 1
    ;;
esac
exit ${result}
