<?xml version="1.0"?>
<!--
Copyright (c) 2012-2022, DilOS.

Permission is hereby granted, free of charge, to any person obtaining a copy  of
this software and associated documentation files (the "Software"),  to  deal  in
the Software without restriction, including without  limitation  the  rights  to
use, copy, modify, merge, publish, distribute, sublicense,  and/or  sell  copies
of the Software, and to permit persons to whom the Software is furnished  to  do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included  in  all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT  WARRANTY  OF  ANY  KIND,  EXPRESS  OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE  AUTHORS  OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  LIABILITY,  WHETHER
IN AN ACTION OF CONTRACT,  TORT  OR  OTHERWISE,  ARISING  FROM,  OUT  OF  OR  IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
-->
<!DOCTYPE service_bundle SYSTEM "/usr/share/lib/xml/dtd/service_bundle.dtd.1">

<service_bundle type='manifest' name='network:bind9'>

<service
  name='network/bind9'
  type='service'
  version='1'>

    <dependency
      name='filesystem_minimal'
      grouping='require_all'
      restart_on='none'
      type='service'>
      <service_fmri value='svc:/system/filesystem/local' />
    </dependency>

    <dependency
      name='loopback'
      grouping='require_any'
      restart_on='error'
      type='service'>
      <service_fmri value='svc:/network/loopback' />
    </dependency>

    <dependency
      name='network'
      grouping='optional_all'
      restart_on='error'
      type='service'>
      <service_fmri value='svc:/milestone/network' />
    </dependency>

    <exec_method
      type='method'
      name='stop'
      exec='/var/svc/method/bind9 %m %i %{restarter/contract}'
      timeout_seconds='60' />

    <!--
	In order to run multiple named(8) processes with their own
	configuration file or properties each must have a unique
	instance.
    -->
    <instance name='default' enabled='false' >

      <exec_method
	type='method'
	name='start'
	exec='/var/svc/method/bind9 %m %i'
	timeout_seconds='60' >
	<method_context>
	  <!--
	      privileges: (see privileges(5) and /etc/security/priv_names)
	      file_dac_read, file_dac_search:
			Necessary for reading the configuration file
			even if it is restricted by the file
			permission.
	      net_privaddr:
			Allow Binding to privileged port-number/proto.
			*Port* | *Protocol* | *Comment*
			~~~~~~~|~~~~~~~~~~~~|~~~~~~~~~~~~~~~~~~~~~~~~~~
			53     | TCP/UDP    | Domain Queries
			80     | TCP        | Statistics channel
			921    | UDP        | Lightweight resolver
			953    | TCP        | Remote diagnostic control
		sys_resource:
			Permit the setting of resource limits
			(eg. stack size).
		proc_chroot:
			Permit use of chroot(2).
	  -->
	  <method_credential
	    user='root'
	    group='root'
	    />
	    <!-- privileges='basic,!proc_session,!proc_info,!file_link_any,{net_privaddr}:53/*,{net_privaddr}:80/tcp,{net_privaddr}:921/udp,{net_privaddr}:953/tcp,file_dac_read,file_dac_search,sys_resource,proc_chroot'-->
	</method_context>
      </exec_method>

      <!--
	  SIGHUP causes named to reread its configuration file, but not any
	  of the properties below.
      -->
      <exec_method
	type='method'
	name='refresh'
	exec=':kill -HUP'
	timeout_seconds='60'>
	<method_context/>
      </exec_method>

      <property_group name='general' type='framework'>
	<!-- manage DNS server state -->
	<propval name='action_authorization' type='astring'
	  value='solaris.smf.manage.bind' />
	<propval name='value_authorization' type='astring'
	  value='solaris.smf.manage.bind' />
      </property_group>

      <!-- Default property settings for named(8) instance. -->
      <property_group name='options' type='application'>

	<!--
		server: specifies an alternative server command.  If
		not specified the default /usr/sbin/named is used.
	-->
	<propval name='server' type='astring' value='' />

	<!--
		configuration_file: specifies an alternative
		configuration file to be used. The property is similar
		to named(8) command line option '-c'
	-->
	<propval name='configuration_file' type='astring' value='' />

	<!--
		debug_level: Specifies the default debug level.  The
		default is 0; no debugging. The Higher the number the
		more verbose debug information becomes.
		Equivalent command line option '-d <integer>'.
	-->
	<propval name='debug_level' type='integer' value='0' />

      </property_group>

	</instance>

	<stability value='Unstable' />

	<template>
		<common_name>
			<loctext xml:lang='C'>
				BIND DNS server
			</loctext>
		</common_name>
		<documentation>
			<manpage title='named' section='8'
			    manpath='/usr/share/man' />
		</documentation>
	</template>

</service>

</service_bundle>
