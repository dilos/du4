Source: librsvg
Section: libs
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: @GNOME_TEAM@
Build-Depends: debhelper-compat (= 12),
               dh-cargo,
               gnome-pkg-tools (>= 0.10),
               gobject-introspection (>= 0.10.8),
               gtk-doc-tools (>= 1.13),
               jq,
               libcairo2-dev (>= 1.2.0),
               libfreetype6-dev (>= 2.8.0),
               libgdk-pixbuf-2.0-dev (>= 2.23.5-2) | libgdk-pixbuf2.0-dev (>= 2.23.5-2),
               libgirepository1.0-dev (>= 0.10.8),
               libglib2.0-dev (>= 2.50.0),
               libharfbuzz-dev,
               libpango1.0-dev (>= 1.38.0),
               libxml2-dev (>= 2.9.0),
               locales [!solaris-any !dilos-any !argoos-any],
               rustc (>= 1.40),
               valac (>= 0.17.5)
Build-Depends-Indep: libcairo2-doc (>= 1.15.4) <!nodoc>,
                     libgdk-pixbuf2.0-doc (>= 2.23.5-2) <!nodoc>,
                     libglib2.0-doc (>= 2.52.0) <!nodoc>
Rules-Requires-Root: no
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/gnome-team/librsvg
Vcs-Git: https://salsa.debian.org/gnome-team/librsvg.git
Homepage: https://wiki.gnome.org/Projects/LibRsvg

Package: librsvg2-dev
Section: libdevel
Architecture: any
Depends: gir1.2-rsvg-2.0 (= ${binary:Version}),
         libcairo2-dev (>= 1.2.0),
         libgdk-pixbuf-2.0-dev (>= 2.23.5-2) | libgdk-pixbuf2.0-dev (>= 2.23.5-2),
         libglib2.0-dev (>= 2.50.0),
         librsvg2-2 (= ${binary:Version}),
         librsvg2-common (= ${binary:Version}),
         ${misc:Depends}
Suggests: librsvg2-doc
Multi-Arch: same
Description: SAX-based renderer library for SVG files (development)
 The rsvg library is an efficient renderer for Scalable Vector Graphics
 (SVG) pictures.
 .
 This package provides the necessary development libraries and include
 files to allow you to develop with librsvg.

Package: librsvg2-2
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Recommends: librsvg2-common
Suggests: librsvg2-bin
Multi-Arch: same
Description: SAX-based renderer library for SVG files (runtime)
 The rsvg library is an efficient renderer for Scalable Vector Graphics
 (SVG) pictures.
 .
 This package contains the runtime library, necessary to run
 applications using librsvg.

Package: librsvg2-common
Architecture: any
Depends: librsvg2-2 (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Description: SAX-based renderer library for SVG files (extra runtime)
 The rsvg library is an efficient renderer for Scalable Vector Graphics
 (SVG) pictures.
 .
 This package includes the gdk-pixbuf loader allowing
 to load SVG images transparently inside GTK+ applications.

Package: librsvg2-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: devhelp
Build-Profiles: <!nodoc>
Description: SAX-based renderer library for SVG files (documentation)
 The rsvg library is an efficient renderer for Scalable Vector Graphics
 (SVG) pictures.
 .
 This package provides the API documentation.

Package: librsvg2-bin
Section: graphics
Architecture: any
Depends: librsvg2-2 (>= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: foreign
Description: command-line utility to convert SVG files
 The rsvg library is an efficient renderer for Scalable Vector Graphics
 (SVG) pictures.
 .
 This package includes a command-line utility to convert the SVG files
 to the PNG format.

Package: gir1.2-rsvg-2.0
Section: introspection
Architecture: any
Depends: ${gir:Depends}, ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Description: gir files for renderer library for SVG files
 The rsvg library is an efficient renderer for Scalable Vector Graphics
 (SVG) pictures.
 .
 This package contains GObject-Introspection information.
