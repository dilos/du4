srt (1.4.2-1.3+dilos1) unstable; urgency=medium

  * Build for DilOS.

 -- DilOS Team <dilos@dilos.org>  Thu, 25 May 2023 18:08:16 +0300

srt (1.4.2-1.3) unstable; urgency=medium

  * Fix arch-indep-only builds by not trying to install the libraries that
    were not built.

 -- Sebastian Dröge <slomo@debian.org>  Tue, 19 Jan 2021 16:15:04 +0200

srt (1.4.2-1.2) unstable; urgency=medium

  * Source-only upload without changes.

 -- Sebastian Dröge <slomo@debian.org>  Tue, 19 Jan 2021 14:27:41 +0200

srt (1.4.2-1.1) unstable; urgency=medium

  [ Federico Ceratto ]
  * Pass flags to cmake (Closes: #971811)
    Thanks to Helmut Grohne <helmut@subdivi.de>

  [ Sebastian Dröge ]
  * Include upstream patch to make the major.minor version the soname. Between
    minor releases there won't be any further ABI changes. (Closes: #971754)

    As a result of this also rename the binary packages accordingly.

    Patch taken from https://github.com/Haivision/srt/pull/1741

 -- Sebastian Dröge <slomo@debian.org>  Sat, 16 Jan 2021 10:30:39 +0200

srt (1.4.2-1) unstable; urgency=medium

  * New upstream release (Closes: #963798)
    Thanks to Vasyl Gello <vasek.gello@gmail.com>

 -- Federico Ceratto <federico@debian.org>  Sat, 03 Oct 2020 11:40:07 +0100

srt (1.4.1-5) unstable; urgency=medium

  * Update Conflicts / Provides

 -- Federico Ceratto <federico@debian.org>  Sun, 14 Jun 2020 10:47:47 +0100

srt (1.4.1-4) unstable; urgency=medium

  * Add Break + Replaces (Closes: #962624)
    Thanks to Gianfranco Costamagna <locutusofborg@debian.org>

 -- Federico Ceratto <federico@debian.org>  Sat, 13 Jun 2020 00:17:35 +0100

srt (1.4.1-3) unstable; urgency=medium

  [ Jonas Smedegaard ]
  * fix multiarch paths (Closes: #962619)

 -- Federico Ceratto <federico@debian.org>  Wed, 10 Jun 2020 20:33:17 +0100

srt (1.4.1-2) unstable; urgency=medium

  * Split build against OpenSSL and GnuTLS (Closes: #933180)

 -- Federico Ceratto <federico@debian.org>  Tue, 19 May 2020 23:54:34 +0100

srt (1.4.1-1) unstable; urgency=medium

  * New upstream release
  * Add libssl-dev dep (Closes: #933178)

 -- Federico Ceratto <federico@debian.org>  Sat, 09 May 2020 23:39:04 +0100

srt (1.4.0-1) unstable; urgency=medium

  * New upstream release (Closes: #939040)

 -- Federico Ceratto <federico@debian.org>  Tue, 17 Sep 2019 10:38:25 +0100

srt (1.3.2-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs
  * d/control: Fix wrong Vcs-*

  [ Federico Ceratto ]
  * New upstream release
  * Split override_dh_installman (Closes: #914364)


 -- Federico Ceratto <federico@debian.org>  Fri, 17 May 2019 21:28:13 +0100

srt (1.3.1-1) unstable; urgency=medium

  * Initial release (Closes: #908333)

 -- Federico Ceratto <federico@debian.org>  Sun, 16 Sep 2018 17:27:04 +0100
