#!/usr/bin/make -f
export DH_VERBOSE = 1
export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export DEB_CFLAGS_MAINT_APPEND  = -Wall -pedantic
#export DEB_LDFLAGS_MAINT_APPEND = -Wl,--as-needed

# Standardized flags from /usr/share/perl5/Debian/Debhelper/Buildsystem/cmake.pm.
CMAKE_OPTS := -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=None -DCMAKE_INSTALL_SYSCONFDIR=/etc -DCMAKE_INSTALL_LOCALSTATEDIR=/var -DCMAKE_EXPORT_NO_PACKAGE_REGISTRY=ON -DCMAKE_FIND_PACKAGE_NO_PACKAGE_REGISTRY=ON

ifneq (,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
  NUMJOBS = $(patsubst parallel=%,%,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
  MAKEFLAGS += -j$(NUMJOBS)
endif

include /usr/share/dpkg/default.mk  # provides DEB_VERSION

#export USE_ENCLIB=openssl|gnutls
export USE_ENCLIB=gnutls

%:
	dh $@ --buildsystem=cmake+makefile

override_dh_auto_clean:
	dh_clean
	rm -rf CMakeFiles/ debian/html build-openssl build-gnutls
	rm -rf CMakeFiles/ debian/doctrees debian/html build-openssl build-gnutls

override_dh_auto_configure:
	dh_auto_configure --builddirectory=build-openssl -- $(CMAKE_OPTS) -DUSE_ENCLIB=openssl
	dh_auto_configure --builddirectory=build-gnutls -- $(CMAKE_OPTS) -DUSE_ENCLIB=gnutls -DTARGET_srt=srt-gnutls

override_dh_auto_build-arch:
	#dh_auto_build --builddirectory=debian/build/openssl
	#dh_auto_build --builddirectory=debian/build/gnutls
	mkdir -p build-openssl build-gnutls
	cd build-openssl && $(MAKE)
	cd build-gnutls && $(MAKE)

override_dh_auto_build-indep:
	http_proxy='http://127.0.0.1:9/' python3 -m sphinx -N -c debian -d debian/doctrees -bhtml docs debian/html

override_dh_auto_install-arch:
	#dh_auto_install
	#mkdir -p debian/libsrt1.4-openssl/usr/lib/$(DEB_HOST_MULTIARCH)
	#cp debian/build/openssl/libsrt*so* debian/libsrt1.4-openssl/usr/lib/$(DEB_HOST_MULTIARCH)/
	#mkdir -p debian/libsrt1.4-gnutls/usr/lib/$(DEB_HOST_MULTIARCH)
	#cp debian/build/gnutls/libsrt*so* debian/libsrt1.4-gnutls/usr/lib/$(DEB_HOST_MULTIARCH)/
	#mkdir -p ./debian/srt-tools/usr/bin
	#for n in srt-file-transmit srt-live-transmit srt-tunnel; do \
	#	cp debian/build/gnutls/srt-live-transmit ./debian/srt-tools/usr/bin/; \
	#done
	#test -f ./debian/srt-tools/usr/bin/srt-file-transmit
	install -d debian/tmp
	cd build-openssl && $(MAKE) install DESTDIR=../debian/tmp
	cd build-gnutls && $(MAKE) install DESTDIR=../debian/tmp   # Does not overwrite anything.
	test -f debian/tmp/usr/bin/stransmit || cp debian/tmp/usr/bin/srt-live-transmit debian/tmp/usr/bin/stransmit

override_dh_auto_install-indep:


override_dh_install:
	dh_install
	install -d debian/libsrt-dev/usr/lib/$(DEB_HOST_MULTIARCH)/pkgconfig/
	install -m 0644 build-openssl/*.pc debian/libsrt-dev/usr/lib/$(DEB_HOST_MULTIARCH)/pkgconfig/
	install -d debian/libsrt-gnutls-dev/usr/lib/$(DEB_HOST_MULTIARCH)/pkgconfig/
	install -m 0644 build-gnutls/*.pc debian/libsrt-gnutls-dev/usr/lib/$(DEB_HOST_MULTIARCH)/pkgconfig/

override_dh_installman-indep:

override_dh_installman-arch:
	help2man --no-discard-stderr --no-info --version-string="$(DEB_VERSION)" \
	--name "" \
	-o ./debian/srt-file-transmit.1 ./build-gnutls/srt-file-transmit
	help2man --no-discard-stderr --no-info --version-string="$(DEB_VERSION)" \
	--name "" \
	-o ./debian/srt-live-transmit.1 ./build-gnutls/srt-live-transmit
	dh_installman

override_dh_auto_test:
	true
