#!/usr/bin/make -f
# Sample debian/rules that uses debhelper.
# GNU copyright 1997 to 1999 by Joey Hess.
# Copyright 2002 David Mart�nez <ender@debian.org>

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

export DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)

# Honor DEB_BUILD_OPTIONS!!!
ifneq (,$(findstring noopt,$(DEB_BUILD_OPTIONS)))
CFLAGS += -g -Wall
else
CFLAGS += -g
endif

CPPFLAGS += $(shell dpkg-buildflags --get CPPFLAGS)
CFLAGS   += $(shell dpkg-buildflags --get CFLAGS)
LDFLAGS  += $(shell dpkg-buildflags --get LDFLAGS)

# shared library versions
version=1.8
major=2

export CPPFLAGS CFLAGS LDFLAGS version major

build: build-arch build-indep
build-arch: build-stamp
build-indep: build-stamp

build-stamp:
	dh_testdir

	dh_auto_build --buildsystem=makefile

	touch build-stamp

clean:
	dh_testdir
	dh_testroot
	rm -f build-stamp

	$(MAKE) clean

	dh_clean

install: build
	dh_testdir
	dh_testroot
	dh_prep
	dh_installdirs

	$(MAKE) install prefix=$(CURDIR)/debian/tmp/usr

	mkdir -p $(CURDIR)/debian/libmpeg3-dev/usr/lib/$(DEB_HOST_MULTIARCH)/pkgconfig
	sed "s,@VERSION@,$(version),;s,@libdir@,/usr/lib/$(DEB_HOST_MULTIARCH)," < debian/pkgconfig.in > $(CURDIR)/debian/libmpeg3-dev/usr/lib/$(DEB_HOST_MULTIARCH)/pkgconfig/libmpeg3.pc

	dh_install --sourcedir=debian/tmp

# Build architecture-independent files here.
binary-indep: build install
# We have nothing to do by default.

# Build architecture-dependent files here.
binary-arch: build install
	dh_testdir
	dh_testroot
	dh_installdocs
	dh_installman
	dh_installchangelogs 

# Needed link for libmpeg3.so.1 -> libmpeg3.so.$(major)
	dh_link -plibmpeg3-$(major) /usr/lib/$(DEB_HOST_MULTIARCH)/libmpeg3.so.$(major).$(version) /usr/lib/$(DEB_HOST_MULTIARCH)/libmpeg3.so.$(major)

# Needed link for libmpeg3.so -> libmpeg3.so.$(major)
	dh_link -plibmpeg3-dev /usr/lib/$(DEB_HOST_MULTIARCH)/libmpeg3.so.$(major).$(version) /usr/lib/$(DEB_HOST_MULTIARCH)/libmpeg3.so

	dh_link
	dh_strip
	dh_compress
	dh_fixperms
	dh_makeshlibs -V
	dh_installdeb
	dh_shlibdeps -ldebian/libmpeg3-$(major)/usr/lib/
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install configure

# The upstream tarball contain CSS, encryption unwanted in Debian 2014-05-28
# It also contain a copy of liba52, which we do not need.
make-source-tarball:
	tar jxf libmpeg3-$(version).tar.bz2
	cd libmpeg3-$(version); rm -rf a52dec-* mpeg3css.c
	tar jcf libmpeg3-$(version).dfsg.tar.bz2 libmpeg3-$(version)
	echo "Now run git-import-orig --pristine-tar libmpeg3-$(version).dfsg.tar.bz2"
