From: Antonio Terceiro <terceiro@debian.org>
Date: Thu, 24 Sep 2020 18:09:37 -0300
Subject: rspec-mocks: fix tests against diff-lcs 1.4.4

This is a backport of the following upstream commit:

From 82ac3cd50740e909559350be0521f85cd433211d Mon Sep 17 00:00:00 2001
From: Jon Rowe <hello@jonrowe.co.uk>
Date: Mon, 29 Jun 2020 15:34:12 +0100
Subject: [PATCH] Merge pull request #1332 from rspec/fix-differ-support

Fix differ support
---
 .travis.yml                         |  2 ++
 Gemfile                             | 14 ++++++++++++++
 spec/rspec/mocks/diffing_spec.rb    | 12 +++++++-----
---
 rspec-mocks/spec/rspec/mocks/diffing_spec.rb    | 12 +++++++-----
 rspec-mocks/spec/rspec/mocks/formatting_spec.rb |  3 ++-
 2 files changed, 9 insertions(+), 6 deletions(-)

diff --git a/rspec-mocks/spec/rspec/mocks/diffing_spec.rb b/rspec-mocks/spec/rspec/mocks/diffing_spec.rb
index e2a1b8a..3b1f91e 100644
--- a/rspec-mocks/spec/rspec/mocks/diffing_spec.rb
+++ b/rspec-mocks/spec/rspec/mocks/diffing_spec.rb
@@ -2,6 +2,8 @@ require "spec_helper"
 require "pp"
 
 RSpec.describe "Diffs printed when arguments don't match" do
+  include RSpec::Support::Spec::DiffHelpers
+
   before do
     allow(RSpec::Mocks.configuration).to receive(:color?).and_return(false)
   end
@@ -68,7 +70,7 @@ RSpec.describe "Diffs printed when arguments don't match" do
         expect(d).to receive(:foo).with(expected_hash)
         expect {
           d.foo(:bad => :hash)
-        }.to fail_with(/\A#<Double "double"> received :foo with unexpected arguments\n  expected: \(#{hash_regex_inspect expected_hash}\)\n       got: \(#{hash_regex_inspect actual_hash}\)\nDiff:\n@@ \-1\,2 \+1\,2 @@\n\-\[#{hash_regex_inspect expected_hash}\]\n\+\[#{hash_regex_inspect actual_hash}\]\n\z/)
+        }.to fail_with(/\A#<Double "double"> received :foo with unexpected arguments\n  expected: \(#{hash_regex_inspect expected_hash}\)\n       got: \(#{hash_regex_inspect actual_hash}\)\nDiff:\n@@ #{Regexp.escape one_line_header} @@\n\-\[#{hash_regex_inspect expected_hash}\]\n\+\[#{hash_regex_inspect actual_hash}\]\n\z/)
       end
     end
 
@@ -101,7 +103,7 @@ RSpec.describe "Diffs printed when arguments don't match" do
         expect(d).to receive(:foo).with([:a, :b, :c])
         expect {
           d.foo([])
-        }.to fail_with("#<Double \"double\"> received :foo with unexpected arguments\n  expected: ([:a, :b, :c])\n       got: ([])\nDiff:\n@@ -1,2 +1,2 @@\n-[[:a, :b, :c]]\n+[[]]\n")
+        }.to fail_with("#<Double \"double\"> received :foo with unexpected arguments\n  expected: ([:a, :b, :c])\n       got: ([])\nDiff:\n@@ #{one_line_header} @@\n-[[:a, :b, :c]]\n+[[]]\n")
       end
     end
 
@@ -117,7 +119,7 @@ RSpec.describe "Diffs printed when arguments don't match" do
             d.foo([])
           }.to fail_with("#<Double \"double\"> received :foo with unexpected arguments\n" \
             "  expected: (#{collab_inspect})\n" \
-            "       got: ([])\nDiff:\n@@ -1,2 +1,2 @@\n-[#{collab_inspect}]\n+[[]]\n")
+            "       got: ([])\nDiff:\n@@ #{one_line_header} @@\n-[#{collab_inspect}]\n+[[]]\n")
         end
       end
     end
@@ -136,7 +138,7 @@ RSpec.describe "Diffs printed when arguments don't match" do
             d.foo([:a, :b])
           }.to fail_with("#<Double \"double\"> received :foo with unexpected arguments\n" \
             "  expected: (#{collab_description})\n" \
-            "       got: ([:a, :b])\nDiff:\n@@ -1,2 +1,2 @@\n-[\"#{collab_description}\"]\n+[[:a, :b]]\n")
+            "       got: ([:a, :b])\nDiff:\n@@ #{one_line_header} @@\n-[\"#{collab_description}\"]\n+[[:a, :b]]\n")
         end
       end
     end
@@ -164,7 +166,7 @@ RSpec.describe "Diffs printed when arguments don't match" do
             d.foo([:a, :b])
           }.to fail_with("#<Double \"double\"> received :foo with unexpected arguments\n" \
             "  expected: (#{collab_inspect})\n" \
-            "       got: ([:a, :b])\nDiff:\n@@ -1,2 +1,2 @@\n-[#{collab_pp}]\n+[[:a, :b]]\n")
+            "       got: ([:a, :b])\nDiff:\n@@ #{one_line_header} @@\n-[#{collab_pp}]\n+[[:a, :b]]\n")
         end
       end
     end
diff --git a/rspec-mocks/spec/rspec/mocks/formatting_spec.rb b/rspec-mocks/spec/rspec/mocks/formatting_spec.rb
index a08b177..aed05be 100644
--- a/rspec-mocks/spec/rspec/mocks/formatting_spec.rb
+++ b/rspec-mocks/spec/rspec/mocks/formatting_spec.rb
@@ -3,6 +3,7 @@ require 'support/doubled_classes'
 
 RSpec.describe "Test doubles format well in failure messages" do
   include RSpec::Matchers::FailMatchers
+  include RSpec::Support::Spec::DiffHelpers
 
   RSpec::Matchers.define :format_in_failures_as do |expected|
     match do |dbl|
@@ -101,7 +102,7 @@ RSpec.describe "Test doubles format well in failure messages" do
     }.to fail_with(<<-EOS.gsub(/^\s+\|/, ''))
       |expected [#<Double "Foo">] to include #<Double "Bar">
       |Diff:
-      |@@ -1,2 +1,2 @@
+      |@@ #{one_line_header} @@
       |-[#<Double "Bar">]
       |+[#<Double "Foo">]
     EOS
