From 3f48e3ea37adf84aae7335b8367012d70bb3f132 Mon Sep 17 00:00:00 2001
From: NIIBE Yutaka <gniibe@fsij.org>
Date: Tue, 27 Apr 2021 17:24:16 +0900
Subject: [PATCH 8/8] ecc: Check the input length for the point.

* cipher/ecc-misc.c (_gcry_ecc_mont_decodepoint): Check the length
of valid point representation.

--

Backport the commit of master:

	060c378c050e7ec6206358c681a313d6e1967dcf

In the use case of GnuPG, ECDH decryption for anonymous recipient may
try to decrypt with different curves.  When the input data of
ephemeral key does not match one of the private key, it should return
GPG_ERR_INV_OBJ.

Signed-off-by: NIIBE Yutaka <gniibe@fsij.org>
---
 cipher/ecc-misc.c | 18 ++++++++++++++----
 1 file changed, 14 insertions(+), 4 deletions(-)

diff --git a/cipher/ecc-misc.c b/cipher/ecc-misc.c
index 34dd6804..b89dcfa6 100644
--- a/cipher/ecc-misc.c
+++ b/cipher/ecc-misc.c
@@ -294,6 +294,7 @@ _gcry_ecc_mont_decodepoint (gcry_mpi_t pk, mpi_ec_t ctx, mpi_point_t result)
 {
   unsigned char *rawmpi;
   unsigned int rawmpilen;
+  unsigned int nbytes = (ctx->nbits+7)/8;
 
   if (mpi_is_opaque (pk))
     {
@@ -305,27 +306,36 @@ _gcry_ecc_mont_decodepoint (gcry_mpi_t pk, mpi_ec_t ctx, mpi_point_t result)
         return GPG_ERR_INV_OBJ;
       rawmpilen = (rawmpilen + 7)/8;
 
-      if (rawmpilen > 1 && (rawmpilen%2) && buf[0] == 0x40)
+      if (rawmpilen == nbytes + 1
+          && (buf[0] == 0x00 || buf[0] == 0x40))
         {
           rawmpilen--;
           buf++;
         }
+      else if (rawmpilen > nbytes)
+        return GPG_ERR_INV_OBJ;
 
-      rawmpi = xtrymalloc (rawmpilen? rawmpilen:1);
+      rawmpi = xtrymalloc (nbytes);
       if (!rawmpi)
         return gpg_err_code_from_syserror ();
 
       p = rawmpi + rawmpilen;
       while (p > rawmpi)
         *--p = *buf++;
+
+      if (rawmpilen < nbytes)
+        memset (rawmpi + nbytes - rawmpilen, 0, nbytes - rawmpilen);
     }
   else
     {
-      unsigned int nbytes = (ctx->nbits+7)/8;
-
       rawmpi = _gcry_mpi_get_buffer (pk, nbytes, &rawmpilen, NULL);
       if (!rawmpi)
         return gpg_err_code_from_syserror ();
+      if (rawmpilen > nbytes + 1)
+        {
+          xfree (rawmpi);
+          return GPG_ERR_INV_OBJ;
+        }
       /*
        * It is not reliable to assume that 0x40 means the prefix.
        *
-- 
2.30.2

