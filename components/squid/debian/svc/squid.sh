#!/usr/bin/sh

#
#
#

. /lib/svc/share/smf_include.sh

CONFIG=`svcprop -c -p config/conffile $SMF_FMRI`
if [ -z $CONFIG ]
then
	CONFIG=/etc/squid/squid.conf
fi

grepconf ()
{
	w="     " # space tab
	res=`$SQUIDCMD -k parse -f $CONFIG 2>&1 |
		grep "Processing:" |
		sed s/.*Processing:\ // |
		sed -ne 's/^['"$w"']*'$1'['"$w"']\+\([^'"$w"']\+\).*$/\1/p;
			t end;
			d;
			:end q'`
        [ -n "$res" ] || res=$2
        echo "$res"
}

SQUIDCMD=/usr/sbin/squid

PIDFILE=`grepconf pid_filename /var/run/squid.pid`
LOGDIR=/var/log/squid
SPOOLDIR=/var/spool/squid
SQUID_ARGS="-YC -f $CONFIG"

case $1 in 
'start')
	if [ ! -d $LOGDIR ]
	then
		mkdir -p $LOGDIR
		chown -R proxy:proxy $LOGDIR
	fi
	if [ ! -d $SPOOLDIR ]
	then
		mkdir -p $SPOOLDIR
		chown -R proxy:proxy $SPOOLDIR
	fi
	if [ ! -d $POOLDIR/00 ]
	then
		$SQUIDCMD -z -f $CONFIG
	fi
	$SQUIDCMD $SQUID_ARGS
	;;

'stop')
	$SQUIDCMD -k shutdown -f $CONFIG
	;;

'refresh')
	$SQUIDCMD -k reconfigure -f $CONFIG
	;;

*)
	echo "Usage: $0 { start | restart }"
	exit 1
	;;
esac	

exit $?
