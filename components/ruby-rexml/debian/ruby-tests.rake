require 'gem2deb/rake/testtask'

Gem2Deb::Rake::TestTask.new do |t|
  t.libs << ['test']
  t.libs << ['test/lib']
  t.test_files = FileList['test/rexml/**/*test_*.rb']
end
