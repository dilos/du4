Description: fix endian issues with CMM
 The interface code to the CMM was corrected
 to indicate when a endian swap was needed on the data.
 This should only occur in the case
 when we are dealing with transparency buffers
 during the put image blending operation
 that may include a color conversion.
 The final blend bakes the data as BE
 so if we are on a LE machine,
 the CMM will need to know to swap the bytes
 (assuming the pdf14 device is using 16bit buffers).
 .
 The code was rewritten to make it clear
 that this setting is no BE vs LE
 but simply an endian swap.
 That was a source of confusion.
 .
 Revealed in this testing was
 the lack of some proper error reporting during buffer conversions,
 which were fixed.
Origin: upstream, https://git.ghostscript.com/?p=ghostpdl.git;h=bd48c43
Author: Michael Vrhel <michael.vrhel@artifex.com>
Reviewed-By: Stefano Rivera <stefano@rivera.za.net>
Bug: https://bugs.ghostscript.com/show_bug.cgi?id=703164
Bug-Debian: https://bugs.debian.org/976177
Last-Update: 2020-12-01
---
This patch header follows DEP-3: https://dep.debian.net/deps/dep3/
--- a/base/gdevp14.c
+++ b/base/gdevp14.c
@@ -887,13 +887,14 @@
    need to do the offset to our data in the buffer. Bug 700686: If we are in
    a softmask that includes a matte entry, then we need to undo the matte
    entry here at this time in the image's native color space not the parent
-   color space.   The big_endian term here is only set to true if the data
-   has been baked as such during the put_image blending operation.  */
+   color space.   The endian_swap term here is only set to true if the data
+   has been baked as BE during the put_image blending operation and we are
+   on a LE machine.  */
 static forceinline pdf14_buf*
 template_transform_color_buffer(gs_gstate *pgs, pdf14_ctx *ctx, gx_device *dev,
     pdf14_buf *src_buf, byte *src_data, cmm_profile_t *src_profile,
     cmm_profile_t *des_profile, int x0, int y0, int width, int height, bool *did_alloc,
-    bool has_matte, bool deep, bool big_endian)
+    bool has_matte, bool deep, bool endian_swap)
 {
     gsicc_rendering_param_t rendering_params;
     gsicc_link_t *icc_link;
@@ -913,6 +914,8 @@
     pdf14_buf *output = src_buf;
     pdf14_mask_t *mask_stack;
     pdf14_buf *maskbuf;
+    int code;
+
     *did_alloc = false;
 
     /* Same profile */
@@ -970,10 +973,8 @@
     gsicc_init_buffer(&des_buff_desc, des_profile->num_comps, 1<<deep, false,
                       false, true, des_planestride, des_rowstride, height, width);
 
-    if (big_endian) {
-        src_buff_desc.little_endian = false;
-        des_buff_desc.little_endian = false;
-    }
+    src_buff_desc.endian_swap = endian_swap;
+    des_buff_desc.endian_swap = endian_swap;
 
     /* If we have a matte entry, undo the pre-blending now.  Also set pdf14
        context to ensure that this is not done again during the group
@@ -989,9 +990,11 @@
     /* Transform the data. Since the pdf14 device should be using RGB, CMYK or
        Gray buffers, this transform does not need to worry about the cmap procs
        of the target device. */
-    (icc_link->procs.map_buffer)(dev, icc_link, &src_buff_desc, &des_buff_desc,
+    code = (icc_link->procs.map_buffer)(dev, icc_link, &src_buff_desc, &des_buff_desc,
         src_data, des_data);
     gsicc_release_link(icc_link);
+    if (code < 0)
+        return NULL;
 
     output->planestride = des_planestride;
     output->rowstride = des_rowstride;
@@ -1045,28 +1048,28 @@
 pdf14_transform_color_buffer_no_matte(gs_gstate *pgs, pdf14_ctx *ctx, gx_device *dev,
     pdf14_buf *src_buf, byte *src_data, cmm_profile_t *src_profile,
     cmm_profile_t *des_profile, int x0, int y0, int width, int height, bool *did_alloc,
-    bool deep, bool big_endian)
+    bool deep, bool endian_swap)
 {
     if (deep)
         return template_transform_color_buffer(pgs, ctx, dev, src_buf, src_data, src_profile,
-            des_profile, x0, y0, width, height, did_alloc, false, true, big_endian);
+            des_profile, x0, y0, width, height, did_alloc, false, true, endian_swap);
     else
         return template_transform_color_buffer(pgs, ctx, dev, src_buf, src_data, src_profile,
-            des_profile, x0, y0, width, height, did_alloc, false, false, big_endian);
+            des_profile, x0, y0, width, height, did_alloc, false, false, endian_swap);
 }
 
 static pdf14_buf*
 pdf14_transform_color_buffer_with_matte(gs_gstate *pgs, pdf14_ctx *ctx, gx_device *dev,
     pdf14_buf *src_buf, byte *src_data, cmm_profile_t *src_profile,
     cmm_profile_t *des_profile, int x0, int y0, int width, int height, bool *did_alloc,
-    bool deep, bool big_endian)
+    bool deep, bool endian_swap)
 {
     if (deep)
         return template_transform_color_buffer(pgs, ctx, dev, src_buf, src_data, src_profile,
-            des_profile, x0, y0, width, height, did_alloc, true, true, big_endian);
+            des_profile, x0, y0, width, height, did_alloc, true, true, endian_swap);
     else
         return template_transform_color_buffer(pgs, ctx, dev, src_buf, src_data, src_profile,
-            des_profile, x0, y0, width, height, did_alloc, true, false, big_endian);
+            des_profile, x0, y0, width, height, did_alloc, true, false, endian_swap);
 }
 
 /**
@@ -1908,6 +1911,7 @@
     gsicc_link_t *icc_link;
     gsicc_rendering_param_t render_cond;
     cmm_dev_profile_t *dev_profile;
+    int code = 0;
 
     dev_proc(dev, get_profile)(dev,  &dev_profile);
     gsicc_extract_profile(GS_UNKNOWN_TAG, dev_profile, &src_profile,
@@ -2058,7 +2062,7 @@
                     rendering_params.cmm = gsCMM_DEFAULT;
                     icc_link = gsicc_get_link_profile(pgs, dev, des_profile,
                         src_profile, &rendering_params, pgs->memory, false);
-                    smask_icc(dev, tos->rect.q.y - tos->rect.p.y,
+                    code = smask_icc(dev, tos->rect.q.y - tos->rect.p.y,
                               tos->rect.q.x - tos->rect.p.x, tos->n_chan,
                               tos->rowstride, tos->planestride,
                               tos->data, new_data_buf, icc_link, tos->deep);
@@ -2089,7 +2093,7 @@
             return gs_note_error(gs_error_VMerror);
         ctx->mask_stack->rc_mask->mask_buf = tos;
     }
-    return 0;
+    return code;
 }
 
 static pdf14_mask_t *
@@ -2407,7 +2411,7 @@
     cmm_profile_t* des_profile;
     gsicc_rendering_param_t render_cond;
     bool did_alloc;
-    bool big_endian;
+    bool endian_swap;
 
     gsicc_extract_profile(GS_UNKNOWN_TAG, dev_target_profile, &des_profile,
         &render_cond);
@@ -2419,17 +2423,26 @@
     global_index++;
 #endif
 
-    /* If we are doing a 16 bit buffer it will be big endian if we have already done the 
-       blend, otherwise it will be native endian */
+    /* If we are doing a 16 bit buffer it will be big endian if we have already done the
+       blend, otherwise it will be native endian. GS expects its 16bit buffers to be BE
+       but for sanity pdf14 device maintains 16bit buffers in native format.  The CMM
+       will need to know if it is dealing with native or BE data. */
     if (was_blended && (*buf)->deep) {
-        big_endian = true;
+        /* Data is in BE.  If we are in a LE machine, CMM will need to swap for
+           color conversion */
+#if ARCH_IS_BIG_ENDIAN
+        endian_swap = false;
+#else
+        endian_swap = true;
+#endif
     } else {
-        big_endian = false;
+        /* Data is in native format. No swap needed for CMM */
+        endian_swap = false;
     }
 
     cm_result = pdf14_transform_color_buffer_no_matte(pgs, dev->ctx, (gx_device*) dev, *buf,
         *buf_ptr, src_profile, des_profile, x, y, width,
-        height, &did_alloc, (*buf)->deep, big_endian);
+        height, &did_alloc, (*buf)->deep, endian_swap);
 
     if (cm_result == NULL)
         return_error(gs_error_VMerror);
--- a/base/gscms.h
+++ b/base/gscms.h
@@ -86,7 +86,7 @@
     unsigned char bytes_per_chan;
     bool has_alpha;
     bool alpha_first;
-    bool little_endian;
+    bool endian_swap;
     bool is_planar;
     int plane_stride;
     int row_stride;
--- a/base/gsicc_cache.c
+++ b/base/gsicc_cache.c
@@ -1788,12 +1788,7 @@
     buffer_desc->row_stride = row_stride;
     buffer_desc->num_rows = num_rows;
     buffer_desc->pixels_per_row = pixels_per_row;
-
-#if ARCH_IS_BIG_ENDIAN
-    buffer_desc->little_endian = false;
-#else
-    buffer_desc->little_endian = true;
-#endif
+    buffer_desc->endian_swap = false;
 }
 
 /* Return the proper component numbers based upon the profiles of the device.
--- a/base/gsicc_lcms2mt.c
+++ b/base/gsicc_lcms2mt.c
@@ -40,10 +40,10 @@
 #define LCMS_BYTES_MASK T_BYTES(-1)	/* leaves only mask for the BYTES (currently 7) */
 #define LCMS_ENDIAN16_MASK T_ENDIAN16(-1) /* similarly, for ENDIAN16 bit */
 
-#define gsicc_link_flags(hasalpha, planarIN, planarOUT, bigendianIN, bigendianOUT, bytesIN, bytesOUT) \
+#define gsicc_link_flags(hasalpha, planarIN, planarOUT, endianswapIN, endianswapOUT, bytesIN, bytesOUT) \
     ((hasalpha != 0) << 2 | \
      (planarIN != 0) << 5 | (planarOUT != 0) << 4 | \
-     (bigendianIN != 0) << 3 | (bigendianOUT != 0) << 2 | \
+     (endianswapIN != 0) << 3 | (endianswapOUT != 0) << 2 | \
      (bytesIN == 1) << 1 | (bytesOUT == 1))
 
 typedef struct gsicc_lcms2mt_link_list_s {
@@ -338,7 +338,7 @@
     gsicc_lcms2mt_link_list_t *link_handle = (gsicc_lcms2mt_link_list_t *)(icclink->link_handle);
     cmsHTRANSFORM hTransform = (cmsHTRANSFORM)link_handle->hTransform;
     cmsUInt32Number dwInputFormat, dwOutputFormat, num_src_lcms, num_des_lcms;
-    int  hasalpha, planarIN, planarOUT, numbytesIN, numbytesOUT, big_endianIN, big_endianOUT;
+    int  hasalpha, planarIN, planarOUT, numbytesIN, numbytesOUT, swap_endianIN, swap_endianOUT;
     int needed_flags = 0;
     unsigned char *inputpos, *outputpos;
     cmsContext ctx = gs_lib_ctx_get_cms_context(icclink->memory);
@@ -349,7 +349,7 @@
     /* Although little CMS does  make assumptions about data types in its
        transformations we can change it after the fact by cloning from any
        other transform. We always create [0] which is no_alpha, chunky IN/OUT,
-       little_endian IN/OUT, 2-bytes_per_component IN/OUT. */
+       no endian swap IN/OUT, 2-bytes_per_component IN/OUT. */
     /* Set us to the proper output type */
     /* Note, we could speed this up by passing back the encoded data type
         to the caller so that we could avoid having to go through this
@@ -368,8 +368,8 @@
         return_error(gs_error_rangecheck);	/* TODO: we don't support float */
 
     /* endian */
-    big_endianIN = !input_buff_desc->little_endian;
-    big_endianOUT = !output_buff_desc->little_endian;
+    swap_endianIN = input_buff_desc->endian_swap;
+    swap_endianOUT = output_buff_desc->endian_swap;
 
     /* alpha, which is passed through unmolested */
     /* TODO:  Right now we always must have alpha last */
@@ -377,7 +377,7 @@
     hasalpha = input_buff_desc->has_alpha;
 
     needed_flags = gsicc_link_flags(hasalpha, planarIN, planarOUT,
-                                    big_endianIN, big_endianOUT,
+                                    swap_endianIN, swap_endianOUT,
                                     numbytesIN, numbytesOUT);
     while (link_handle->flags != needed_flags) {
         if (link_handle->next == NULL) {
@@ -418,8 +418,8 @@
         dwOutputFormat = dwOutputFormat | EXTRA_SH(hasalpha);
         dwInputFormat = dwInputFormat | PLANAR_SH(planarIN);
         dwOutputFormat = dwOutputFormat | PLANAR_SH(planarOUT);
-        dwInputFormat = dwInputFormat | ENDIAN16_SH(big_endianIN);
-        dwOutputFormat = dwOutputFormat | ENDIAN16_SH(big_endianOUT);
+        dwInputFormat = dwInputFormat | ENDIAN16_SH(swap_endianIN);
+        dwOutputFormat = dwOutputFormat | ENDIAN16_SH(swap_endianOUT);
         dwInputFormat = dwInputFormat | BYTES_SH(numbytesIN);
         dwOutputFormat = dwOutputFormat | BYTES_SH(numbytesOUT);
 
@@ -608,9 +608,7 @@
       when we use the transformation. */
     src_data_type = (COLORSPACE_SH(lcms_src_color_space)|
                         CHANNELS_SH(src_nChannels)|BYTES_SH(2));
-#if 0
-    src_data_type = src_data_type | ENDIAN16_SH(1);
-#endif
+
     if (lcms_deshandle != NULL) {
         des_color_space  = cmsGetColorSpace(ctx, lcms_deshandle);
     } else {
@@ -623,10 +621,7 @@
     des_nChannels = cmsChannelsOf(ctx, des_color_space);
     des_data_type = (COLORSPACE_SH(lcms_des_color_space)|
                         CHANNELS_SH(des_nChannels)|BYTES_SH(2));
-    /* endian */
-#if 0
-    des_data_type = des_data_type | ENDIAN16_SH(1);
-#endif
+
     /* Set up the flags */
     flag = gscms_get_accuracy(memory);
     if (rendering_params->black_point_comp == gsBLACKPTCOMP_ON
@@ -679,7 +674,7 @@
             return NULL;
     }
     link_handle->next = NULL;
-    link_handle->flags = gsicc_link_flags(0, 0, 0, 0, 0,    /* no alpha, not planar, little-endian */
+    link_handle->flags = gsicc_link_flags(0, 0, 0, 0, 0,    /* no alpha, not planar, no endian swap */
                                           sizeof(gx_color_value), sizeof(gx_color_value));
     return link_handle;
     /* cmsFLAGS_HIGHRESPRECALC)  cmsFLAGS_NOTPRECALC  cmsFLAGS_LOWRESPRECALC*/
@@ -714,7 +709,7 @@
     if (link_handle == NULL)
          return NULL;
     link_handle->next = NULL;
-    link_handle->flags = gsicc_link_flags(0, 0, 0, 0, 0,    /* no alpha, not planar, little-endian */
+    link_handle->flags = gsicc_link_flags(0, 0, 0, 0, 0,    /* no alpha, not planar, no endian swap */
                                           sizeof(gx_color_value), sizeof(gx_color_value));
     /* Check if the rendering intent is something other than relative colorimetric
        and if we have a proofing profile.  In this case we need to create the
@@ -1028,7 +1023,7 @@
         cmsDeleteTransform(ctx, hTransformNew);
         return;					/* bail */
     }
-    link_handle->flags = gsicc_link_flags(0, 0, 0, 0, 0,    /* no alpha, not planar, little-endian */
+    link_handle->flags = gsicc_link_flags(0, 0, 0, 0, 0,    /* no alpha, not planar, no endian swap */
                                           sizeof(gx_color_value), sizeof(gx_color_value));
     link_handle->hTransform = hTransformNew;
     link_handle->next = NULL;
--- a/base/gxblend.c
+++ b/base/gxblend.c
@@ -402,7 +402,8 @@
     }
 }
 
-void smask_icc(gx_device *dev, int num_rows, int num_cols, int n_chan,
+int
+smask_icc(gx_device *dev, int num_rows, int num_cols, int n_chan,
                int row_stride, int plane_stride, byte *gs_restrict src, const byte *gs_restrict dst,
                gsicc_link_t *icclink, bool deep)
 {
@@ -427,7 +428,7 @@
                   false, false, true, plane_stride,
                   row_stride, num_rows, num_cols);
     /* Transform the data */
-    (icclink->procs.map_buffer)(dev, icclink, &input_buff_desc, &output_buff_desc,
+    return (icclink->procs.map_buffer)(dev, icclink, &input_buff_desc, &output_buff_desc,
                                 (void*) src, (void*) dst);
 }
 
--- a/base/gxblend.h
+++ b/base/gxblend.h
@@ -100,7 +100,7 @@
 
 void smask_copy(int num_rows, int num_cols, int row_stride,
                          byte *gs_restrict src, const byte *gs_restrict des);
-void smask_icc(gx_device *dev, int num_rows, int num_cols, int n_chan,
+int smask_icc(gx_device *dev, int num_rows, int num_cols, int n_chan,
                int row_stride, int plane_stride, byte *gs_restrict src, const byte *gs_restrict des,
                gsicc_link_t *icclink, bool deep);
 /* For spot colors, blend modes must be white preserving and separable */
--- a/base/gxblend1.c
+++ b/base/gxblend1.c
@@ -226,6 +226,7 @@
     int y0 = max(buf->rect.p.y, tos->rect.p.y);
     int y1 = min(buf->rect.q.y, tos->rect.q.y);
     bool deep = buf->deep;
+    int code;
 
     if (x0 < x1 && y0 < y1) {
         int width = x1 - x0;
@@ -283,9 +284,11 @@
                               false, true, buf->planestride, buf->rowstride, height,
                               width);
             /* Transform the data.  */
-            (icc_link->procs.map_buffer)(dev, icc_link, &input_buff_desc,
+            code = (icc_link->procs.map_buffer)(dev, icc_link, &input_buff_desc,
                                          &output_buff_desc, tos_plane, buf_plane);
             gsicc_release_link(icc_link);
+            if (code < 0)
+                return gs_throw(gs_error_unknownerror, "ICC transform failed.  Trans backdrop");
         }
         /* Copy the alpha data */
         buf_plane += buf->planestride * (buf->n_chan - 1);
--- a/base/gxi12bit.c
+++ b/base/gxi12bit.c
@@ -666,19 +666,23 @@
                                         (const unsigned short*) (psrc_decode+w),
                                          get_cie_range(penum->pcs));
                 }
-                (penum->icc_link->procs.map_buffer)(dev, penum->icc_link,
+                code = (penum->icc_link->procs.map_buffer)(dev, penum->icc_link,
                                                     &input_buff_desc,
                                                     &output_buff_desc,
                                                     (void*) psrc_decode,
                                                     (void*) psrc_cm);
                 gs_free_object(pgs->memory, (byte *)psrc_decode, "image_render_color_icc");
+                if (code < 0)
+                    return code;
             } else {
                 /* CM only. No decode */
-                (penum->icc_link->procs.map_buffer)(dev, penum->icc_link,
+                code = (penum->icc_link->procs.map_buffer)(dev, penum->icc_link,
                                                     &input_buff_desc,
                                                     &output_buff_desc,
                                                     (void*) psrc,
                                                     (void*) psrc_cm);
+                if (code < 0)
+                    return code;
             }
         }
     }
--- a/base/gxicolor.c
+++ b/base/gxicolor.c
@@ -566,19 +566,23 @@
                     decode_row_cie(penum, psrc, spp, psrc_decode,
                                     psrc_decode+w, get_cie_range(penum->pcs));
                 }
-                (penum->icc_link->procs.map_buffer)(dev, penum->icc_link,
+                code = (penum->icc_link->procs.map_buffer)(dev, penum->icc_link,
                                                     &input_buff_desc,
                                                     &output_buff_desc,
                                                     (void*) psrc_decode,
                                                     (void*) *psrc_cm);
                 gs_free_object(pgs->memory, psrc_decode, "image_color_icc_prep");
+                if (code < 0)
+                    return code;
             } else {
                 /* CM only. No decode */
-                (penum->icc_link->procs.map_buffer)(dev, penum->icc_link,
+                code = (penum->icc_link->procs.map_buffer)(dev, penum->icc_link,
                                                     &input_buff_desc,
                                                     &output_buff_desc,
                                                     (void*) psrc,
                                                     (void*) *psrc_cm);
+                if (code < 0)
+                    return code;
             }
         }
     }
--- a/base/gxipixel.c
+++ b/base/gxipixel.c
@@ -1133,6 +1133,7 @@
     gsicc_bufferdesc_t input_buff_desc;
     gsicc_bufferdesc_t output_buff_desc;
     gx_color_value conc[GX_DEVICE_COLOR_MAX_COMPONENTS];
+    int code;
 
     if (penum->icc_link == NULL) {
         return gs_rethrow(-1, "ICC Link not created during image render color");
@@ -1265,10 +1266,13 @@
         gsicc_init_buffer(&output_buff_desc, num_des_comp, 1, false, false, false,
                           0, num_entries * num_des_comp,
                       1, num_entries);
-        (penum->icc_link->procs.map_buffer)(penum->dev, penum->icc_link, 
-                                            &input_buff_desc, &output_buff_desc, 
+        code = (penum->icc_link->procs.map_buffer)(penum->dev, penum->icc_link,
+                                            &input_buff_desc, &output_buff_desc,
                                             (void*) temp_buffer,
                                             (void*) penum->color_cache->device_contone);
+        if (code < 0)
+            return gs_rethrow(code, "Failure to map color buffer");
+
         /* Check if we need to apply any transfer functions.  If so then do it now */
         if (has_transfer) {
             for (k = 0; k < num_entries; k++) {
--- a/base/gxiscale.c
+++ b/base/gxiscale.c
@@ -2171,9 +2171,12 @@
                           1, width_in);
             /* Do the transformation */
             psrc = (byte*) (stream_r.ptr + 1);
-            (penum->icc_link->procs.map_buffer)(dev, penum->icc_link, &input_buff_desc,
+            code = (penum->icc_link->procs.map_buffer)(dev, penum->icc_link, &input_buff_desc,
                                                 &output_buff_desc, (void*) psrc,
                                                 (void*) p_cm_buff);
+            if (code < 0)
+                return code;
+
             /* Re-set the reading stream to use the cm data */
             stream_r.ptr = p_cm_buff - 1;
             stream_r.limit = stream_r.ptr + num_bytes_decode * width_in * spp_cm;
@@ -2232,11 +2235,13 @@
                     pinterp += (pss->params.LeftMarginOut / abs_interp_limit) * spp_decode;
                     p_cm_interp = (unsigned short *) p_cm_buff;
                     p_cm_interp += (pss->params.LeftMarginOut / abs_interp_limit) * spp_cm;
-                    (penum->icc_link->procs.map_buffer)(dev, penum->icc_link,
+                    code = (penum->icc_link->procs.map_buffer)(dev, penum->icc_link,
                                                         &input_buff_desc,
                                                         &output_buff_desc,
                                                         (void*) pinterp,
                                                         (void*) p_cm_interp);
+                    if (code < 0)
+                        return code;
                 }
                 code = irii_core(penum, xo, xe, spp_cm, p_cm_interp, dev, abs_interp_limit, bpp, raster, yo, dy, lop);
                 if (code < 0)
@@ -2660,9 +2665,12 @@
                           1, width_in);
             /* Do the transformation */
             psrc = (byte*) (stream_r.ptr + 1);
-            (penum->icc_link->procs.map_buffer)(dev, penum->icc_link, &input_buff_desc,
+            code = (penum->icc_link->procs.map_buffer)(dev, penum->icc_link, &input_buff_desc,
                                                 &output_buff_desc, (void*) psrc,
                                                 (void*) p_cm_buff);
+            if (code < 0)
+                return code;
+
             /* Re-set the reading stream to use the cm data */
             stream_r.ptr = p_cm_buff - 1;
             stream_r.limit = stream_r.ptr + num_bytes_decode * width_in * spp_cm;
@@ -2733,11 +2741,13 @@
                     pinterp += (pss->params.LeftMarginOut / abs_interp_limit) * spp_decode;
                     p_cm_interp = (unsigned short *) p_cm_buff;
                     p_cm_interp += (pss->params.LeftMarginOut / abs_interp_limit) * spp_cm;
-                    (penum->icc_link->procs.map_buffer)(dev, penum->icc_link,
+                    code = (penum->icc_link->procs.map_buffer)(dev, penum->icc_link,
                                                         &input_buff_desc,
                                                         &output_buff_desc,
                                                         (void*) pinterp,
                                                         (void*) p_cm_interp);
+                    if (code < 0)
+                        return code;
                 }
                 p_cm_interp += (pss->params.LeftMarginOut / abs_interp_limit) * spp_cm;
                 for (x = xo; x < xe;) {
--- a/devices/vector/gdevxps.c
+++ b/devices/vector/gdevxps.c
@@ -2173,9 +2173,11 @@
             gsicc_init_buffer(&output_buff_desc, 3, bytes_comp,
                 false, false, false, 0, width * bytes_comp * 3,
                 1, width);
-            (pie->icc_link->procs.map_buffer)(pie->dev, pie->icc_link,
+            code = (pie->icc_link->procs.map_buffer)(pie->dev, pie->icc_link,
                 &input_buff_desc, &output_buff_desc, (void*)buffer,
                 (void*)pie->buffer);
+            if (code < 0)
+                return code;
             outbuffer = pie->buffer;
         }
         code = TIFFWriteScanline(pie->tif, outbuffer, pie->y, 0);
