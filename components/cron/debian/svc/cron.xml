<?xml version="1.0"?>
<!DOCTYPE service_bundle SYSTEM "/usr/share/lib/xml/dtd/service_bundle.dtd.1">
<!--
Copyright (c) 2012-2022, DilOS.

Permission is hereby granted, free of charge, to any person obtaining a copy  of
this software and associated documentation files (the "Software"),  to  deal  in
the Software without restriction, including without  limitation  the  rights  to
use, copy, modify, merge, publish, distribute, sublicense,  and/or  sell  copies
of the Software, and to permit persons to whom the Software is furnished  to  do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included  in  all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT  WARRANTY  OF  ANY  KIND,  EXPRESS  OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE  AUTHORS  OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  LIABILITY,  WHETHER
IN AN ACTION OF CONTRACT,  TORT  OR  OTHERWISE,  ARISING  FROM,  OUT  OF  OR  IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

    NOTE:  This service manifest is not editable; its contents will
    be overwritten by package or patch operations, including
    operating system upgrade.  Make customizations in a different
    file.
-->

<service_bundle type='manifest' name='system:cron'>

<service
	name='system/cron'
	type='service'
	version='1'>

	<single_instance />

	<dependency
		name='usr'
		type='service'
		grouping='require_all'
		restart_on='none'>
		<service_fmri value='svc:/system/filesystem/local' />
	</dependency>

	<dependency
		name='ns'
		type='service'
		grouping='require_all'
		restart_on='none'>
		<service_fmri value='svc:/milestone/name-services' />
	</dependency>

	<dependent
		name='cron_multi-user'
		grouping='optional_all'
		restart_on='none'>
		<service_fmri value='svc:/milestone/multi-user' />
	</dependent>

	<exec_method
		type='method'
		name='start'
		exec="/var/svc/method/cron"
		timeout_seconds='60'>
		<method_context>
			<method_credential user='root' group='root' />
		</method_context>
	</exec_method>

	<exec_method
		type='method'
		name='stop'
		exec=':kill'
		timeout_seconds='60'>
	</exec_method>

	<exec_method
		type='method'
		name='refresh'
		exec=':kill -THAW'
		timeout_seconds='60'>
	</exec_method>

	<property_group name='startd' type='framework'>
		<!-- sub-process core dumps shouldn't restart session -->
		<propval name='ignore_error' type='astring'
		    value='core,signal' />
	</property_group>

	<property_group name='general' type='framework'>
		<!-- to start stop cron -->
		<propval name='action_authorization' type='astring'
			value='solaris.smf.manage.cron' />
	</property_group>

	<property_group name='application' type='application'>
		<propval name='log_level' type='astring' value='1'/>
	</property_group>

	<instance name='default' enabled='true' />

	<stability value='Unstable' />

	<template>
		<common_name>
			<loctext xml:lang='C'>
			clock daemon (cron)
			</loctext>
		</common_name>
		<documentation>
			<manpage title='cron' section='8' manpath='/usr/share/man' />
			<manpage title='crontab' section='1' manpath='/usr/share/man' />
			<manpage title='crontab' section='5' manpath='/usr/share/man' />
		</documentation>
	</template>
</service>

</service_bundle>
