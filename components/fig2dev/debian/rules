#!/usr/bin/make -f
#
# (C) 1999-2021 Roland Rosenfeld <roland@debian.org>
#
# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1
#export DH_OPTIONS=-v

DEBDIR=`pwd`/debian/fig2dev

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export FIG2DEV_RGBFILE = $(shell pwd)/fig2dev/rgb.txt

%:
	dh $@

override_dh_autoreconf:
#	preserve files overwritten by dh_autoreconf:
	tar cf debian/autoreconf.tar Makefile.in aclocal.m4 ar-lib compile \
		configure depcomp fig2dev/Makefile.in fig2dev/dev/Makefile.in \
		fig2dev/i18n/Makefile.in fig2dev/tests/Makefile.in install-sh \
		man/Makefile.in missing transfig/Makefile.in
	dh_autoreconf

override_dh_auto_configure:
	dh_auto_configure -- --enable-transfig

override_dh_auto_build:
#	preserve some files from upstream tarball:
	tar cf debian/preserve.tar fig2dev/config.vc \
		fig2dev/tests/data/fillswclip.svg \
		fig2dev/tests/data/patterns.svg transfig/doc/manual.pdf \
		fig2dev/tests/testsuite

# 	rebuild testsuite:
	(cd fig2dev/tests; rm -f testsuite; make testsuite)

	dh_auto_build

ifeq (,$(filter nodoc,$(DEB_BUILD_OPTIONS)))
	(cd transfig/doc; \
	 ../../fig2dev/fig2dev -L latex trans.fig > trans.tex; \
	 latex manual; \
	 latex manual; \
	 dvips -o manual.ps manual.dvi; \
	 ps2pdf manual.ps)

#	uudecode fig2mpdf documentation:
	(cd fig2mpdf/doc && perl uudecode *.uue)
	(cd fig2mpdf/doc && $(MAKE))

# 	strip /ID from sample-presentation.pdf because this is includes current
#	directory and makes package non reproducible
	(cd fig2mpdf/doc \
	 && grep -av '^/ID \[\(<[0-9A-F]\{32\}>\) \1]$$' sample-presentation.pdf \
		> sample-presentation.pdf_without_id \
	 && mv -f sample-presentation.pdf_without_id sample-presentation.pdf)
endif

override_dh_auto_install:
	dh_auto_install --destdir=$(DEBDIR)
	install -m755 fig2mpdf/fig2mpdf $(DEBDIR)/usr/bin/

override_dh_installdocs:
	dh_installdocs
ifeq (,$(filter nodoc,$(DEB_BUILD_OPTIONS)))
	install -m644 fig2mpdf/doc/*.css \
		$(DEBDIR)/usr/share/doc/fig2dev/fig2mpdf/
	install -m644 fig2mpdf/doc/*.gif \
		$(DEBDIR)/usr/share/doc/fig2dev/fig2mpdf/
	install -m644 fig2mpdf/doc/*.html \
		$(DEBDIR)/usr/share/doc/fig2dev/fig2mpdf/
	install -m644 fig2mpdf/doc/*.jpg \
		$(DEBDIR)/usr/share/doc/fig2dev/fig2mpdf/
	install -m644 fig2mpdf/doc/*.lfig \
		$(DEBDIR)/usr/share/doc/fig2dev/fig2mpdf/
	install -m644 fig2mpdf/doc/sample-flat.pdf \
		$(DEBDIR)/usr/share/doc/fig2dev/fig2mpdf/
	install -m644 fig2mpdf/doc/sample-presentation.pdf \
		$(DEBDIR)/usr/share/doc/fig2dev/fig2mpdf/
endif

override_dh_clean:
	dh_clean

	rm -f transfig/doc/trans.tex transfig/doc/*.aux transfig/doc/*.log \
		transfig/doc/*.dvi transfig/doc/*.ps
	[ ! -f fig2mpdf/doc/Makefile ] || (cd fig2mpdf/doc && $(MAKE) clean)
	rm -f fig2mpdf/doc/*.gif fig2mpdf/doc/*.pdf fig2mpdf/doc/*.jpg \
		fig2mpdf/doc/sample-presentation.aux \
		fig2mpdf/doc/sample-presentation.nav \
		fig2mpdf/doc/sample-presentation.out \
		fig2mpdf/doc/sample-presentation.snm \
		fig2mpdf/doc/sample-presentation.toc \
		fig2mpdf/doc/sample-presentation.log

#	restore files overwritten by dh_autoreconf:
	[ ! -f debian/autoreconf.tar ] || tar xf debian/autoreconf.tar
	rm -f debian/autoreconf.tar

#	restore files from upstream tarball:
	[ ! -f debian/preserve.tar ] || tar xf debian/preserve.tar
	rm -f debian/preserve.tar

override_dh_installchangelogs:
	dh_installchangelogs CHANGES

override_dh_compress:
	dh_compress -X.pdf
