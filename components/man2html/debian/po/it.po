# man2html - Italian Debconf messages
# 
# This file is distributed under the same conditions of the man2html package.
# Andrea Bolognani <eof@kiyuko.org>, 2006.

msgid ""
msgstr ""
"Project-Id-Version: man2html 1.6c\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2004-03-14 21:37+0100\n"
"PO-Revision-Date: 2006-03-21 11:04+0100\n"
"Last-Translator: Andrea Bolognani <eof@kiyuko.org>\n"
"Language-Team: Italian <it@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../templates:4
msgid "Should post-installation script index your man pages?"
msgstr "Vuoi che lo script di post-installazione crei un'indice delle tue "
"pagine di manuale?"

#. Type: boolean
#. Description
#: ../templates:4
msgid "Swish++ will be run once a week to index your man pages."
msgstr "Swish++ verrà eseguito una volta alla settimana per creare un'indice "
"delle tue pagine di manuale."

#. Type: boolean
#. Description
#: ../templates:4
msgid ""
"The index can also be generated (in the background) by the post-installation "
"script. This process needs quite a lot of computer resources, and can take "
"several minutes so you can choose now if you would like to do this."
msgstr "L'indice può anche essere generato (in background) dallo script di "
"post-installazione. Questo processo richiede parecchie risorse del computer "
"e può impiegare diversi minuti, quindi puoi decidere ora se farlo o meno."
