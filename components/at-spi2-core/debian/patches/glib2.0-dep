commit f31a221f1129f6614d251d656657cca71a9f645b
Author: Federico Mena Quintero <federico@gnome.org>
Date:   Mon Dec 13 19:03:49 2021 -0600

    Remove set_bus_to_exit_if_this_process_dies()
    
    This was only used for dbus-broker.  Same as for dbus-daemon, I don't
    think we need to set the daemon to die anymore.

diff --git b/bus/at-spi-bus-launcher.c a/bus/at-spi-bus-launcher.c
index 4d73149..9ac6808 100644
--- b/bus/at-spi-bus-launcher.c
+++ a/bus/at-spi-bus-launcher.c
@@ -313,6 +313,15 @@ on_bus_exited (GPid     pid,
   g_main_loop_quit (app->loop);
 }
 
+static void
+set_bus_to_exit_if_this_process_dies (void)
+{
+#ifdef __linux__
+  /* Tell the bus process to exit if this process goes away */
+  prctl (PR_SET_PDEATHSIG, SIGTERM);
+#endif
+}
+
 #ifdef DBUS_DAEMON
 static gboolean
 ensure_a11y_bus_daemon (A11yBusLauncher *app, char *config_path)
@@ -429,6 +438,8 @@ setup_bus_child_broker (gpointer data)
   pid_str = g_strdup_printf("%u", getpid());
   g_setenv("LISTEN_PID", pid_str, TRUE);
   g_free(pid_str);
+
+  set_bus_to_exit_if_this_process_dies ();
 }
 
 static gboolean
commit d8f609035b2ed9ffd3443cfcb4b475f66cce3f1d
Author: Federico Mena Quintero <federico@gnome.org>
Date:   Mon Dec 13 16:41:55 2021 -0600

    Remove unused function setup_bus_child_daemon()
    
    The switch to G_SPAWN_LEAVE_DESCRIPTORS_OPEN makes
    g_spawn_async_with_pipes_and_fds() use the code path for
    posix_spawn(), which doesn't use a child setup func.
    
    I think setting the bus to exit when at-spi-bus-launcher exits is a
    leftover from when the latter was rewritten in 2011, and we didn't
    have reliable ways of ending session daemons.

diff --git b/bus/at-spi-bus-launcher.c a/bus/at-spi-bus-launcher.c
index 145f0ae..4a02c5a 100644
--- b/bus/at-spi-bus-launcher.c
+++ a/bus/at-spi-bus-launcher.c
@@ -323,6 +323,14 @@ set_bus_to_exit_if_this_process_dies (void)
 }
 
 #ifdef DBUS_DAEMON
+static void
+setup_bus_child_daemon (gpointer data)
+{
+  A11yBusLauncher *app = data;
+
+  set_bus_to_exit_if_this_process_dies ();
+}
+
 static gboolean
 ensure_a11y_bus_daemon (A11yBusLauncher *app, char *config_path)
 {
commit fda17a03a73da418c64f336ac921d523a707bb29
Author: Federico Mena Quintero <federico@gnome.org>
Date:   Mon Dec 13 11:48:13 2021 -0600

    Try the posix_spawn() code path from g_spawn_async_with_pipes_and_fds()

diff --git b/bus/at-spi-bus-launcher.c a/bus/at-spi-bus-launcher.c
index 4a02c5a..77d7164 100644
--- b/bus/at-spi-bus-launcher.c
+++ a/bus/at-spi-bus-launcher.c
@@ -364,8 +364,8 @@ ensure_a11y_bus_daemon (A11yBusLauncher *app, char *config_path)
   if (!g_spawn_async_with_pipes_and_fds (NULL,
                                          (const gchar * const *) argv,
                                          NULL,
-                                         G_SPAWN_SEARCH_PATH | G_SPAWN_DO_NOT_REAP_CHILD | G_SPAWN_LEAVE_DESCRIPTORS_OPEN,
-                                         NULL, /* child_setup */
+                                         G_SPAWN_SEARCH_PATH | G_SPAWN_DO_NOT_REAP_CHILD,
+                                         setup_bus_child_daemon,
                                          app,
                                          -1, /* stdin_fd */
                                          -1, /* stdout_fd */
commit d4fa3aaed31f515bd1986c8795c1ff52af26f202
Author: Philip Withnall <philip@tecnocode.co.uk>
Date:   Mon Dec 13 17:30:58 2021 +0000

    Future-proof the count of source_fds / target_fds
    
    Thanks to Philip Withnall for the suggestions.

diff --git b/bus/at-spi-bus-launcher.c a/bus/at-spi-bus-launcher.c
index 77d7164..23a258d 100644
--- b/bus/at-spi-bus-launcher.c
+++ a/bus/at-spi-bus-launcher.c
@@ -353,7 +353,6 @@ ensure_a11y_bus_daemon (A11yBusLauncher *app, char *config_path)
   char *argv[] = { DBUS_DAEMON, config_path, "--nofork", "--print-address", print_address_fd_param, address_param, NULL };
   gint source_fds[1] = { app->pipefd[1] };
   gint target_fds[1] = { app->pipefd[1] };
-  G_STATIC_ASSERT (G_N_ELEMENTS (source_fds) == G_N_ELEMENTS (target_fds));
   GPid pid;
   char addr_buf[2048];
   GError *error = NULL;
@@ -372,7 +371,7 @@ ensure_a11y_bus_daemon (A11yBusLauncher *app, char *config_path)
                                          -1, /* stdout_fd */
                                          source_fds,
                                          target_fds,
-                                         G_N_ELEMENTS (source_fds), /* n_fds in source_fds and target_fds */
+                                         1, /* n_fds in source_fds and target_fds */
                                          &pid,
                                          NULL, /* stdin_pipe_out */
                                          NULL, /* stdout_pipe_out */
commit 2baac0d54c4bec2c89de190b5df425826461ca86
Author: Federico Mena Quintero <federico@gnome.org>
Date:   Fri Dec 10 19:44:28 2021 -0600

    Don't close the read end of the pipe in the child setup func
    
    g_spawn_async_with_pipes_and_fds() will take care of that.

diff --git b/bus/at-spi-bus-launcher.c a/bus/at-spi-bus-launcher.c
index 23a258d..8df85c2 100644
--- b/bus/at-spi-bus-launcher.c
+++ a/bus/at-spi-bus-launcher.c
@@ -327,6 +327,11 @@ static void
 setup_bus_child_daemon (gpointer data)
 {
   A11yBusLauncher *app = data;
+  (void) app;
+
+  close (app->pipefd[0]);
+  dup2 (app->pipefd[1], 3);
+  close (app->pipefd[1]);
 
   set_bus_to_exit_if_this_process_dies ();
 }
commit d010e13360714670ba78729180207c33069f625d
Author: Federico Mena Quintero <federico@gnome.org>
Date:   Fri Dec 10 19:23:12 2021 -0600

    Use g_spawn_async_with_pipes_and_fds() to pass down a specific FD
    
    Otherwise, g_spawn_async() closes everything except
    stdin/stdout/stderr.  How very neat of it :)

diff --git b/bus/at-spi-bus-launcher.c a/bus/at-spi-bus-launcher.c
index 8df85c2..64b4ddd 100644
--- b/bus/at-spi-bus-launcher.c
+++ a/bus/at-spi-bus-launcher.c
@@ -356,8 +356,6 @@ ensure_a11y_bus_daemon (A11yBusLauncher *app, char *config_path)
   char *print_address_fd_param = g_strdup_printf ("%d", app->pipefd[1]);
 
   char *argv[] = { DBUS_DAEMON, config_path, "--nofork", "--print-address", print_address_fd_param, address_param, NULL };
-  gint source_fds[1] = { app->pipefd[1] };
-  gint target_fds[1] = { app->pipefd[1] };
   GPid pid;
   char addr_buf[2048];
   GError *error = NULL;
@@ -365,23 +363,14 @@ ensure_a11y_bus_daemon (A11yBusLauncher *app, char *config_path)
 
   g_clear_pointer (&app->a11y_launch_error_message, g_free);
 
-  if (!g_spawn_async_with_pipes_and_fds (NULL,
-                                         (const gchar * const *) argv,
-                                         NULL,
-                                         G_SPAWN_SEARCH_PATH | G_SPAWN_DO_NOT_REAP_CHILD,
-                                         setup_bus_child_daemon,
-                                         app,
-                                         -1, /* stdin_fd */
-                                         -1, /* stdout_fd */
-                                         -1, /* stdout_fd */
-                                         source_fds,
-                                         target_fds,
-                                         1, /* n_fds in source_fds and target_fds */
-                                         &pid,
-                                         NULL, /* stdin_pipe_out */
-                                         NULL, /* stdout_pipe_out */
-                                         NULL, /* stderr_pipe_out */
-                                         &error))
+  if (!g_spawn_async (NULL,
+                      argv,
+                      NULL,
+                      G_SPAWN_SEARCH_PATH | G_SPAWN_DO_NOT_REAP_CHILD,
+                      setup_bus_child_daemon,
+                      app,
+                      &pid,
+                      &error))
     {
       app->a11y_bus_pid = -1;
       app->a11y_launch_error_message = g_strdup (error->message);
commit 8a232a703779ca73356f4b1569971e3f01920dcd
Author: Federico Mena Quintero <federico@gnome.org>
Date:   Thu Dec 9 18:14:46 2021 -0600

    Don't pass a hardcoded file descriptor to --print-address
    
    Instead just use whatever the write end of pipe() is.  I don't like
    the idea of dup2() unconditionally closing fd=3.

diff --git b/bus/at-spi-bus-launcher.c a/bus/at-spi-bus-launcher.c
index 64b4ddd..59a6660 100644
--- b/bus/at-spi-bus-launcher.c
+++ a/bus/at-spi-bus-launcher.c
@@ -350,17 +350,15 @@ ensure_a11y_bus_daemon (A11yBusLauncher *app, char *config_path)
       address_param = NULL;
     }
 
-  if (pipe (app->pipefd) < 0)
-    g_error ("Failed to create pipe: %s", strerror (errno));
-
-  char *print_address_fd_param = g_strdup_printf ("%d", app->pipefd[1]);
-
-  char *argv[] = { DBUS_DAEMON, config_path, "--nofork", "--print-address", print_address_fd_param, address_param, NULL };
+  char *argv[] = { DBUS_DAEMON, config_path, "--nofork", "--print-address", "3", address_param, NULL };
   GPid pid;
   char addr_buf[2048];
   GError *error = NULL;
   char *error_from_read;
 
+  if (pipe (app->pipefd) < 0)
+    g_error ("Failed to create pipe: %s", strerror (errno));
+
   g_clear_pointer (&app->a11y_launch_error_message, g_free);
 
   if (!g_spawn_async (NULL,
@@ -376,12 +374,10 @@ ensure_a11y_bus_daemon (A11yBusLauncher *app, char *config_path)
       app->a11y_launch_error_message = g_strdup (error->message);
       g_clear_error (&error);
       g_free (address_param);
-      g_free (print_address_fd_param);
       goto error;
     }
 
   g_free (address_param);
-  g_free (print_address_fd_param);
   close (app->pipefd[1]);
   app->pipefd[1] = -1;
 
