commit 2878e91db0b63663e2059910acbb4064356b1baa
Author: Samuel Thibault <samuel.thibault@ens-lyon.org>
Date:   Sun Apr 10 15:40:09 2022 +0200

    Fix testsuite under various locales
    
    When the current locale doesn't match the current voice, grep would be
    surprised by the produced output and believe that this is not text, for
    instance with LC_ALL=ru_RU.CP1251 we get:
    
      TEST      tests/language-replace.test
    [...]
    testing mk
    grep: (standard input): binary file matches
    2d1
    < Translate 'пејзаж'
    
    But we can give -a to grep so it always considers its input as text.

diff --git a/tests/common b/tests/common
index 2d96569a..6c841817 100755
--- a/tests/common
+++ b/tests/common
@@ -125,7 +125,7 @@ test_replace() {
     ESPEAK_DATA_PATH=`pwd` LD_LIBRARY_PATH=src:${LD_LIBRARY_PATH} \
         $VALGRIND src/espeak-ng -Xq -v ${TEST_LANG} "${TEST_TEXT}" \
         > actual.txt || exit 1
-    < actual.txt grep -E "(Translate|Found:) " | sed -e 's/ \[.*][ ]*$//g' > filtered.txt
+    < actual.txt grep -a -E "(Translate|Found:) " | sed -e 's/ \[.*][ ]*$//g' > filtered.txt
     if [ "$MESSAGE" = "Ignore" ] ; then
         diff expected.txt filtered.txt || (echo "... ignoring error" && true)
     else
