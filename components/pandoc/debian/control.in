Source: pandoc
Section: text
Priority: optional
Maintainer: Debian Haskell Group <debian-haskell@lists.debian.org>
Uploaders: Jonas Smedegaard <dr@jones.dk>
 , Kiwamu Okabe <kiwamu@debian.or.jp>
 , Clint Adams <clint@debian.org>
Build-Depends: @cdbs@
Build-Depends-Indep: ghc-doc,
 libghc-aeson-doc,
 libghc-aeson-pretty-doc,
 libghc-ansi-terminal-doc,
 libghc-asn1-encoding-doc,
 libghc-asn1-parse-doc,
 libghc-asn1-types-doc,
 libghc-async-doc,
 libghc-attoparsec-doc,
 libghc-base-compat-batteries-doc,
 libghc-base-compat-doc,
 libghc-base-orphans-doc,
 libghc-base16-bytestring-doc,
 libghc-base64-bytestring-doc,
 libghc-basement-doc,
 libghc-bitarray-doc,
 libghc-blaze-builder-doc,
 libghc-blaze-html-doc,
 libghc-blaze-markup-doc,
 libghc-case-insensitive-doc,
 libghc-cereal-doc,
 libghc-cmark-gfm-doc,
 libghc-colour-doc,
 libghc-connection-doc,
 libghc-cookie-doc,
 libghc-cryptonite-doc,
 libghc-data-default-class-doc,
 libghc-data-default-doc,
 libghc-data-default-instances-containers-doc,
 libghc-data-default-instances-dlist-doc,
 libghc-data-default-instances-old-locale-doc,
 libghc-digest-doc,
 libghc-dlist-doc,
 libghc-doclayout-doc,
 libghc-doctemplates-doc,
 libghc-emojis-doc,
 libghc-errors-doc,
 libghc-exceptions-doc,
 libghc-glob-doc,
 libghc-haddock-library-doc,
 libghc-hashable-doc,
 libghc-hourglass-doc,
 libghc-hslua-doc,
 libghc-hslua-module-text-doc,
 libghc-hslua-module-system-doc,
 libghc-hsyaml-doc,
 libghc-http-doc,
 libghc-http-client-doc,
 libghc-http-client-tls-doc,
 libghc-http-types-doc,
 libghc-hxt-charproperties-doc,
 libghc-hxt-doc,
 libghc-hxt-regex-xmlschema-doc,
 libghc-hxt-unicode-doc,
 libghc-integer-logarithms-doc,
 libghc-ipynb-doc,
 libghc-jira-wiki-markup-doc,
 libghc-juicypixels-doc,
 libghc-memory-doc,
 libghc-mime-types-doc,
 libghc-network-doc,
 libghc-network-uri-doc,
 libghc-old-locale-doc,
 libghc-pandoc-types-doc,
 libghc-pem-doc,
 libghc-primitive-doc,
 libghc-quickcheck2-doc,
 libghc-random-doc,
 libghc-regex-base-doc,
 libghc-regex-pcre-doc,
 libghc-safe-doc,
 libghc-scientific-doc,
 libghc-sha-doc,
 libghc-skylighting-core-doc,
 libghc-skylighting-doc,
 libghc-socks-doc,
 libghc-split-doc,
 libghc-splitmix-doc,
 libghc-streaming-commons-doc,
 libghc-syb-doc,
 libghc-tagged-doc,
 libghc-tagsoup-doc,
 libghc-temporary-doc,
 libghc-texmath-doc,
 libghc-text-conversions-doc,
 libghc-th-abstraction-doc,
 libghc-time-compat-doc,
 libghc-tls-doc,
 libghc-transformers-compat-doc,
 libghc-unicode-transforms-doc,
 libghc-unordered-containers-doc,
 libghc-utf8-string-doc,
 libghc-uuid-types-doc,
 libghc-vector-doc,
 libghc-x509-doc,
 libghc-x509-store-doc,
 libghc-x509-system-doc,
 libghc-x509-validation-doc,
 libghc-xml-doc,
 libghc-yaml-doc,
 libghc-zip-archive-doc,
 libghc-zlib-doc,
Standards-Version: 4.5.0
Homepage: https://pandoc.org/
Vcs-Git: https://salsa.debian.org/haskell-team/pandoc.git
Vcs-Browser: https://salsa.debian.org/haskell-team/pandoc
Rules-Requires-Root: no

Package: pandoc
Architecture: any
Depends: ${cdbs:Depends}
 , ${misc:Depends}
 , ${shlibs:Depends}
 , pandoc-data (>= ${source:Version})
 , pandoc-data (<< ${source:Version}.~)
Recommends: ${cdbs:Recommends}
Suggests: ${cdbs:Suggests}
Provides:
 pandoc-abi (= ${pandoc:Abi}),
Multi-Arch: foreign
Description: general markup converter
 Pandoc is a Haskell library for converting
 from one markup format to another,
 and a command-line tool that uses this library.
 It can read several dialects of Markdown and (subsets of) HTML,
 reStructuredText, LaTeX, DocBook,
 JATS, MediaWiki markup, DokuWiki markup, TWiki markup, TikiWiki markup,
 Jira markup, Creole 1.0,
 Haddock markup, OPML, Emacs Org-mode, Emacs Muse,
 txt2tags, ipynb (Jupyter notebooks), Vimwiki,
 Word Docx, ODT, EPUB, FictionBook2,
 roff man, Textile, and CSV,
 and it can write Markdown, reStructuredText, XHTML, HTML 5,
 LaTeX (including rendering as plain PDF or beamer slide shows),
 ConTeXt, DocBook, JATS, OPML, TEI,
 OpenDocument, ODT, Word docx, PowerPoint pptx, RTF,
 MediaWiki, DokuWiki, XWiki, ZimWiki,
 Textile, Jira, roff man, roff ms, GNU Texinfo,
 plain text, Emacs Org-Mode, AsciiDoc, Haddock markup,
 EPUB (v2 and v3), ipynb, FictionBook2, InDesign ICML,
 Muse, LaTeX beamer slides, PowerPoint,
 and several kinds of HTML/javaScript slide shows
 (S5, Slidy, Slideous, DZSlides, reveal.js).
 .
 In contrast to most existing tools for converting Markdown to HTML,
 pandoc has a modular design:
 it consists of a set of readers, which parse text in a given format
 and produce a native representation of the document,
 and a set of writers,
 which convert this native representation into a target format.
 Thus,
 adding an input or output format requires only adding a reader or writer.
 .
 This package contains the pandoc tool.
 .
 Some uses of Pandoc require additional packages:
  * SVG content in PDF output requires librsvg2-bin.
  * YAML metadata in TeX-related output requires texlive-latex-extra.
  * *.hs filters not set executable requires ghc.
  * *.js filters not set executable requires nodejs.
  * *.php filters not set executable requires php.
  * *.pl filters not set executable requires perl.
  * *.py filters not set executable requires python.
  * *.rb filters not set executable requires ruby.
  * *.r filters not set executable requires r-base-core.
  * LaTeX output, and PDF output via PDFLaTeX,
    require texlive-latex-recommended.
  * XeLaTeX output, and PDF output via XeLaTeX, require texlive-xetex.
  * LuaTeX output, and PDF output via LuaTeX, require texlive-luatex.
  * ConTeXt output, and PDF output via ConTeXt, require context.
  * PDF output via wkhtmltopdf requires wkhtmltopdf.
  * Roff man and roff ms output, and PDF output via roff ms,
    require groff.
  * MathJax-rendered equations require libjs-mathjax.
  * KaTeX-rendered equations require node-katex.
  * option --csl may use styles in citation-style-language-styles.

Package: pandoc-data
Architecture: all
Depends: ${cdbs:Depends}
 , ${misc:Depends}
Recommends: ${cdbs:Recommends}
Suggests: ${cdbs:Suggests}
Multi-Arch: foreign
Description: general markup converter - data files
 Pandoc is a Haskell library for converting
 from one markup format to another,
 and a command-line tool that uses this library.
 It can read several dialects of Markdown and (subsets of) HTML,
 reStructuredText, LaTeX, DocBook,
 JATS, MediaWiki markup, DokuWiki markup, TWiki markup, TikiWiki markup,
 Jira markup, Creole 1.0,
 Haddock markup, OPML, Emacs Org-mode, Emacs Muse,
 txt2tags, ipynb (Jupyter notebooks), Vimwiki,
 Word Docx, ODT, EPUB, FictionBook2,
 roff man, Textile, and CSV,
 and it can write Markdown, reStructuredText, XHTML, HTML 5,
 LaTeX (including rendering as plain PDF or beamer slide shows),
 ConTeXt, DocBook, JATS, OPML, TEI,
 OpenDocument, ODT, Word docx, PowerPoint pptx, RTF,
 MediaWiki, DokuWiki, XWiki, ZimWiki,
 Textile, Jira, roff man, roff ms, GNU Texinfo,
 plain text, Emacs Org-Mode, AsciiDoc, Haddock markup,
 EPUB (v2 and v3), ipynb, FictionBook2, InDesign ICML,
 Muse, LaTeX beamer slides, PowerPoint,
 and several kinds of HTML/javaScript slide shows
 (S5, Slidy, Slideous, DZSlides, reveal.js).
 .
 In contrast to most existing tools for converting Markdown to HTML,
 pandoc has a modular design:
 it consists of a set of readers, which parse text in a given format
 and produce a native representation of the document,
 and a set of writers,
 which convert this native representation into a target format.
 Thus,
 adding an input or output format requires only adding a reader or writer.
 .
 This package contains the data files for pandoc.

Package: libghc-pandoc-dev
Section: haskell
Architecture: any
Depends: ${cdbs:Depends}
 , ${haskell:Depends}
 , ${haskell:Extra-Depends}
 , ${misc:Depends}
 , ${shlibs:Depends}
Recommends: ${haskell:Recommends}
Suggests: ${haskell:Suggests}
Provides: ${haskell:Provides}
Description: general markup converter - libraries
 Pandoc is a Haskell library for converting
 from one markup format to another,
 and a command-line tool that uses this library.
 It can read several dialects of Markdown and (subsets of) HTML,
 reStructuredText, LaTeX, DocBook,
 JATS, MediaWiki markup, DokuWiki markup, TWiki markup, TikiWiki markup,
 Jira markup, Creole 1.0,
 Haddock markup, OPML, Emacs Org-mode, Emacs Muse,
 txt2tags, ipynb (Jupyter notebooks), Vimwiki,
 Word Docx, ODT, EPUB, FictionBook2,
 roff man, Textile, and CSV,
 and it can write Markdown, reStructuredText, XHTML, HTML 5,
 LaTeX (including rendering as plain PDF or beamer slide shows),
 ConTeXt, DocBook, JATS, OPML, TEI,
 OpenDocument, ODT, Word docx, PowerPoint pptx, RTF,
 MediaWiki, DokuWiki, XWiki, ZimWiki,
 Textile, Jira, roff man, roff ms, GNU Texinfo,
 plain text, Emacs Org-Mode, AsciiDoc, Haddock markup,
 EPUB (v2 and v3), ipynb, FictionBook2, InDesign ICML,
 Muse, LaTeX beamer slides, PowerPoint,
 and several kinds of HTML/javaScript slide shows
 (S5, Slidy, Slideous, DZSlides, reveal.js).
 .
 In contrast to most existing tools for converting Markdown to HTML,
 pandoc has a modular design:
 it consists of a set of readers, which parse text in a given format
 and produce a native representation of the document,
 and a set of writers,
 which convert this native representation into a target format.
 Thus,
 adding an input or output format requires only adding a reader or writer.
 .
 This package contains the libraries compiled for GHC.

Package: libghc-pandoc-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${haskell:Depends}
 , ${haskell:Extra-Depends}
 , ${misc:Depends}
Recommends: ${haskell:Recommends}
Description: general markup converter - library documentation
 Pandoc is a Haskell library for converting
 from one markup format to another,
 and a command-line tool that uses this library.
 It can read several dialects of Markdown and (subsets of) HTML,
 reStructuredText, LaTeX, DocBook,
 JATS, MediaWiki markup, DokuWiki markup, TWiki markup, TikiWiki markup,
 Jira markup, Creole 1.0,
 Haddock markup, OPML, Emacs Org-mode, Emacs Muse,
 txt2tags, ipynb (Jupyter notebooks), Vimwiki,
 Word Docx, ODT, EPUB, FictionBook2,
 roff man, Textile, and CSV,
 and it can write Markdown, reStructuredText, XHTML, HTML 5,
 LaTeX (including rendering as plain PDF or beamer slide shows),
 ConTeXt, DocBook, JATS, OPML, TEI,
 OpenDocument, ODT, Word docx, PowerPoint pptx, RTF,
 MediaWiki, DokuWiki, XWiki, ZimWiki,
 Textile, Jira, roff man, roff ms, GNU Texinfo,
 plain text, Emacs Org-Mode, AsciiDoc, Haddock markup,
 EPUB (v2 and v3), ipynb, FictionBook2, InDesign ICML,
 Muse, LaTeX beamer slides, PowerPoint,
 and several kinds of HTML/javaScript slide shows
 (S5, Slidy, Slideous, DZSlides, reveal.js).
 .
 In contrast to most existing tools for converting Markdown to HTML,
 pandoc has a modular design:
 it consists of a set of readers, which parse text in a given format
 and produce a native representation of the document,
 and a set of writers,
 which convert this native representation into a target format.
 Thus,
 adding an input or output format requires only adding a reader or writer.
 .
 This package contains the library documentation for Pandoc.

Package: libghc-pandoc-prof
Section: haskell
Architecture: any
Depends: ${haskell:Depends}
 , ${haskell:Extra-Depends}
 , ${misc:Depends}
Suggests: ${cdbs:Suggests}
Provides: ${haskell:Provides}
Description: general markup converter - profiling libraries
 Pandoc is a Haskell library for converting
 from one markup format to another,
 and a command-line tool that uses this library.
 It can read several dialects of Markdown and (subsets of) HTML,
 reStructuredText, LaTeX, DocBook,
 JATS, MediaWiki markup, DokuWiki markup, TWiki markup, TikiWiki markup,
 Jira markup, Creole 1.0,
 Haddock markup, OPML, Emacs Org-mode, Emacs Muse,
 txt2tags, ipynb (Jupyter notebooks), Vimwiki,
 Word Docx, ODT, EPUB, FictionBook2,
 roff man, Textile, and CSV,
 and it can write Markdown, reStructuredText, XHTML, HTML 5,
 LaTeX (including rendering as plain PDF or beamer slide shows),
 ConTeXt, DocBook, JATS, OPML, TEI,
 OpenDocument, ODT, Word docx, PowerPoint pptx, RTF,
 MediaWiki, DokuWiki, XWiki, ZimWiki,
 Textile, Jira, roff man, roff ms, GNU Texinfo,
 plain text, Emacs Org-Mode, AsciiDoc, Haddock markup,
 EPUB (v2 and v3), ipynb, FictionBook2, InDesign ICML,
 Muse, LaTeX beamer slides, PowerPoint,
 and several kinds of HTML/javaScript slide shows
 (S5, Slidy, Slideous, DZSlides, reveal.js).
 .
 In contrast to most existing tools for converting Markdown to HTML,
 pandoc has a modular design:
 it consists of a set of readers, which parse text in a given format
 and produce a native representation of the document,
 and a set of writers,
 which convert this native representation into a target format.
 Thus,
 adding an input or output format requires only adding a reader or writer.
 .
 This package contains the profiling libraries for Pandoc.
