Description: revert dependency on base-noprelude
Origin: upstream, https://github.com/jgm/pandoc/commit/a9ef15b
Author: John MacFarlane <jgm@berkeley.edu>
Bug: https://github.com/jgm/pandoc/issues/6187
Forwarded: yes
Last-Update: 2020-08-23
---
This patch header follows DEP-3: http://dep.debian.net/deps/dep3/
--- a/pandoc.cabal
+++ b/pandoc.cabal
@@ -377,8 +377,7 @@
   Default:       False
 
 library
-  build-depends: base-noprelude >= 4.9 && < 5,
-                 base-compat >= 0.9,
+  build-depends: base >= 4.9 && < 5,
                  syb >= 0.1 && < 0.8,
                  containers >= 0.4.2.1 && < 0.7,
                  unordered-containers >= 0.2 && < 0.3,
@@ -441,6 +440,10 @@
      build-depends: basement >= 0.0.10,
                     foundation >= 0.0.23
                     -- basement 0.0.9 won't build on 32-bit windows.
+  if impl(ghc < 8.4)
+     hs-source-dirs: prelude
+     other-modules:  Prelude
+     build-depends:  base-compat >= 0.9
   if os(windows)
     cpp-options:      -D_WINDOWS
   else
@@ -468,8 +471,7 @@
 
   default-language: Haskell2010
   other-extensions: NoImplicitPrelude
-  hs-source-dirs:  src,
-                   prelude
+  hs-source-dirs:  src
 
   exposed-modules: Text.Pandoc,
                    Text.Pandoc.App,
@@ -563,8 +565,7 @@
                    Text.Pandoc.ImageSize,
                    Text.Pandoc.BCP47,
                    Text.Pandoc.Class
-  other-modules:   Prelude,
-                   Text.Pandoc.App.CommandLineOptions,
+  other-modules:   Text.Pandoc.App.CommandLineOptions,
                    Text.Pandoc.App.FormatHeuristics,
                    Text.Pandoc.App.Opt,
                    Text.Pandoc.App.OutputSettings,
@@ -641,19 +642,19 @@
   buildable:       True
 
 executable pandoc
-  build-depends:   pandoc,
-                   base-noprelude >= 4.8 && < 5,
-                   base-compat >= 0.9
+  build-depends: pandoc, base >= 4.8 && < 5
+  if impl(ghc < 8.4)
+     hs-source-dirs: prelude
+     other-modules:  Prelude
+     build-depends:  base-compat >= 0.9
   if flag(static)
     ld-options:     -static
   default-language: Haskell2010
   other-extensions: NoImplicitPrelude
   hs-source-dirs:  .
-                   prelude
   main-is:         pandoc.hs
   buildable:       True
-  other-modules:   Paths_pandoc,
-                   Prelude
+  other-modules:   Paths_pandoc
   ghc-options:     -rtsopts -with-rtsopts=-K16m -threaded
                    -Wall -fno-warn-unused-do-bind
                    -Wincomplete-record-updates
@@ -668,16 +669,19 @@
 
 executable trypandoc
   main-is:         trypandoc.hs
-  hs-source-dirs:  trypandoc, prelude
+  hs-source-dirs:  trypandoc
   default-language: Haskell2010
   other-extensions: NoImplicitPrelude
-  other-modules:    Prelude
   if flag(trypandoc)
-    build-depends: base-noprelude, base-compat >= 0.9, aeson, pandoc,
+    build-depends: base, aeson, pandoc,
                    text, wai-extra, wai >= 0.3, http-types
     buildable:     True
   else
     buildable:     False
+  if impl(ghc < 8.4)
+     hs-source-dirs: prelude
+     other-modules:  Prelude
+     build-depends:  base-compat >= 0.9
   ghc-options:     -rtsopts -with-rtsopts=-K16m -threaded
                    -Wall -fno-warn-unused-do-bind
                    -Wincomplete-record-updates
@@ -693,16 +697,18 @@
 benchmark weigh-pandoc
   type:            exitcode-stdio-1.0
   main-is:         weigh-pandoc.hs
-  hs-source-dirs:  benchmark,
-                   prelude
+  hs-source-dirs:  benchmark
   build-depends:   pandoc,
-                   base-noprelude >= 4.8 && < 5,
-                   base-compat >= 0.9,
+                   base >= 4.8 && < 5,
                    text,
                    weigh >= 0.0 && < 0.1,
                    mtl >= 2.2 && < 2.3
+  if impl(ghc < 8.4)
+     hs-source-dirs: prelude
+     other-modules:  Prelude
+     build-depends:  base-compat >= 0.9
   default-language: Haskell2010
-  other-modules:   Prelude
+  other-extensions: NoImplicitPrelude
   ghc-options:     -rtsopts -with-rtsopts=-K16m -threaded
                    -Wall -fno-warn-unused-do-bind
                    -Wincomplete-record-updates
@@ -719,10 +725,8 @@
 test-suite test-pandoc
   type:           exitcode-stdio-1.0
   main-is:        test-pandoc.hs
-  hs-source-dirs: test,
-                  prelude
-  build-depends:  base-noprelude >= 4.8 && < 5,
-                  base-compat >= 0.9,
+  hs-source-dirs: test
+  build-depends:  base >= 4.8 && < 5,
                   pandoc,
                   pandoc-types >= 1.20 && < 1.21,
                   mtl >= 2.2 && < 2.3,
@@ -748,8 +752,11 @@
                   xml >= 1.3.12 && < 1.4,
                   doctemplates >= 0.8 && < 0.9,
                   Glob >= 0.7 && < 0.11
-  other-modules:  Prelude
-                  Tests.Old
+  if impl(ghc < 8.4)
+     hs-source-dirs: prelude
+     other-modules:  Prelude
+     build-depends:  base-compat >= 0.9
+  other-modules:  Tests.Old
                   Tests.Command
                   Tests.Helpers
                   Tests.Lua
@@ -804,6 +811,7 @@
   if os(windows)
     cpp-options:      -D_WINDOWS
   default-language: Haskell2010
+  other-extensions: NoImplicitPrelude
   ghc-options:     -rtsopts -with-rtsopts=-K16m -threaded
                    -Wall -fno-warn-unused-do-bind
                    -Wincomplete-record-updates
@@ -820,17 +828,19 @@
 benchmark benchmark-pandoc
   type:            exitcode-stdio-1.0
   main-is:         benchmark-pandoc.hs
-  hs-source-dirs:  benchmark,
-                   prelude
+  hs-source-dirs:  benchmark
   build-depends:   pandoc,
                    time, bytestring, containers,
-                   base-noprelude >= 4.8 && < 5,
-                   base-compat >= 0.9,
+                   base >= 4.8 && < 5,
                    text >= 1.1.1.0 && < 1.3,
                    mtl >= 2.2 && < 2.3,
                    criterion >= 1.0 && < 1.6
+  if impl(ghc < 8.4)
+     hs-source-dirs: prelude
+     other-modules:  Prelude
+     build-depends:  base-compat >= 0.9
   default-language: Haskell2010
-  other-modules:    Prelude
+  other-extensions: NoImplicitPrelude
   ghc-options:     -rtsopts -with-rtsopts=-K16m -threaded
                    -Wall -fno-warn-unused-do-bind
                    -Wincomplete-record-updates
--- a/src/Text/Pandoc/App/CommandLineOptions.hs
+++ b/src/Text/Pandoc/App/CommandLineOptions.hs
@@ -30,7 +30,7 @@
 import Data.Char (toLower)
 import Data.List (intercalate, sort)
 #ifdef _WINDOWS
-#if MIN_VERSION_base_noprelude(4,12,0)
+#if MIN_VERSION_base(4,12,0)
 import Data.List (isPrefixOf)
 #endif
 #endif
@@ -1028,7 +1028,7 @@
 -- beginning with \\ to \\?\UNC\. -- See #5127.
 normalizePath :: FilePath -> FilePath
 #ifdef _WINDOWS
-#if MIN_VERSION_base_noprelude(4,12,0)
+#if MIN_VERSION_base(4,12,0)
 normalizePath fp =
   if "\\\\" `isPrefixOf` fp && not ("\\\\?\\" `isPrefixOf` fp)
     then "\\\\?\\UNC\\" ++ drop 2 fp
