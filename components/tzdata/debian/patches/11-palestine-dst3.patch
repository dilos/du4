commit c42d108da5a95cd0432bfeeff35498d695d09da8
Author: Paul Eggert <eggert@cs.ucla.edu>
Date:   Tue Aug 30 12:45:52 2022 -0500

    Palestine transitions are now Saturdays at 02:00
    
    (Thanks to Heba Hamad.)
    * NEWS: Mention this.
    * asia (Palestine): Mar and Oct Sat>=24 2:00 going forward.

diff --git a/NEWS b/NEWS
index 7dc8812f..68ab024e 100644
--- a/NEWS
+++ b/NEWS
@@ -4,6 +4,11 @@ News for the tz database
 
   Changes to future timestamps
 
+    Palestine now springs forward and falls back at 02:00 on the first
+    Saturday on or after March 24 and October 24, respectively.  This
+    this means 2022 falls back 10-29 at 02:00, not 10-28 at 01:00.
+    (Thanks to Heba Hamad.)
+
     Chile's 2022 DST start is delayed from September 2 to September 11.
     (Thanks to Juan Correa.)
 
diff --git a/asia b/asia
index 6e3ea4b3..1a3d8c11 100644
--- a/asia
+++ b/asia
@@ -3395,6 +3395,17 @@ Zone	Asia/Karachi	4:28:12 -	LMT	1907
 # From Heba Hamad (2022-03-10):
 # summer time will begin in Palestine from Sunday 03-27-2022, 00:00 AM.
 
+# From Heba Hamad (2022-08-30):
+# winter time will begin in Palestine from Saturday 10-29, 02:00 AM by
+# 60 minutes backwards.  Also the state of Palestine adopted the summer
+# and winter time for the years: 2023,2024,2025,2026 ...
+# https://mm.icann.org/pipermail/tz/attachments/20220830/9f024566/Time-0001.pdf
+#
+# From Paul Eggert (2022-08-30):
+# Guess they spring forward on the first Saturday on or after March 24,
+# and fall back on the first Saturday on or after October 24, as this is
+# consistent with the given transitions and with previous date practice.
+
 # Rule	NAME	FROM	TO	-	IN	ON	AT	SAVE	LETTER/S
 Rule EgyptAsia	1957	only	-	May	10	0:00	1:00	S
 Rule EgyptAsia	1957	1958	-	Oct	 1	0:00	0	-
@@ -3431,8 +3442,10 @@ Rule Palestine	2019	only	-	Mar	29	0:00	1:00	S
 Rule Palestine	2019	only	-	Oct	Sat>=24	0:00	0	-
 Rule Palestine	2020	2021	-	Mar	Sat>=24	0:00	1:00	S
 Rule Palestine	2020	only	-	Oct	24	1:00	0	-
-Rule Palestine	2021	max	-	Oct	Fri>=23	1:00	0	-
-Rule Palestine	2022	max	-	Mar	Sun>=25	0:00	1:00	S
+Rule Palestine	2021	only	-	Oct	29	1:00	0	-
+Rule Palestine	2022	only	-	Mar	27	0:00	1:00	S
+Rule Palestine	2022	max	-	Oct	Sat>=24	2:00	0	-
+Rule Palestine	2023	max	-	Mar	Sat>=24	2:00	1:00	S
 
 # Zone	NAME		STDOFF	RULES	FORMAT	[UNTIL]
 Zone	Asia/Gaza	2:17:52	-	LMT	1900 Oct

commit 1a0e30a84b2f76663123a4555c2785b450ce06f1
Author: Paul Eggert <eggert@cs.ucla.edu>
Date:   Wed Aug 31 16:58:10 2022 -0500

    Tweak expression of Palestine transition
    
    * asia (Palestine): Say "Sat<=30" instead of "Sat>=24".
    These have equivalent effect, so this does not change the
    generated data.  “Sat<=30” is a bit closer to the intended
    “Saturday before the last Sunday” given by Heba Hamad.

diff --git a/asia b/asia
index 1a3d8c11..9fbeb71f 100644
--- a/asia
+++ b/asia
@@ -3375,10 +3375,6 @@ Zone	Asia/Karachi	4:28:12 -	LMT	1907
 # The winter time in 2015 started on October 23 at 01:00.
 # https://wafa.ps/ar_page.aspx?id=CgpCdYa670694628582aCgpCdY
 # http://www.palestinecabinet.gov.ps/portal/meeting/details/27583
-#
-# From Paul Eggert (2019-04-10):
-# For now, guess spring-ahead transitions are at 00:00 on the Saturday
-# preceding March's last Sunday (i.e., Sat>=24).
 
 # From P Chan (2021-10-18):
 # http://wafa.ps/Pages/Details/34701
@@ -3400,11 +3396,11 @@ Zone	Asia/Karachi	4:28:12 -	LMT	1907
 # 60 minutes backwards.  Also the state of Palestine adopted the summer
 # and winter time for the years: 2023,2024,2025,2026 ...
 # https://mm.icann.org/pipermail/tz/attachments/20220830/9f024566/Time-0001.pdf
+# (2022-08-31): ... the Saturday before the last Sunday in March and October
+# at 2:00 AM ,for the years from 2023 to 2026.
 #
-# From Paul Eggert (2022-08-30):
-# Guess they spring forward on the first Saturday on or after March 24,
-# and fall back on the first Saturday on or after October 24, as this is
-# consistent with the given transitions and with previous date practice.
+# From Paul Eggert (2022-08-31):
+# For now, assume that this rule will also be used after 2026.
 
 # Rule	NAME	FROM	TO	-	IN	ON	AT	SAVE	LETTER/S
 Rule EgyptAsia	1957	only	-	May	10	0:00	1:00	S
@@ -3436,16 +3432,16 @@ Rule Palestine	2013	only	-	Sep	27	0:00	0	-
 Rule Palestine	2014	only	-	Oct	24	0:00	0	-
 Rule Palestine	2015	only	-	Mar	28	0:00	1:00	S
 Rule Palestine	2015	only	-	Oct	23	1:00	0	-
-Rule Palestine	2016	2018	-	Mar	Sat>=24	1:00	1:00	S
-Rule Palestine	2016	2018	-	Oct	Sat>=24	1:00	0	-
+Rule Palestine	2016	2018	-	Mar	Sat<=30	1:00	1:00	S
+Rule Palestine	2016	2018	-	Oct	Sat<=30	1:00	0	-
 Rule Palestine	2019	only	-	Mar	29	0:00	1:00	S
-Rule Palestine	2019	only	-	Oct	Sat>=24	0:00	0	-
-Rule Palestine	2020	2021	-	Mar	Sat>=24	0:00	1:00	S
+Rule Palestine	2019	only	-	Oct	Sat<=30	0:00	0	-
+Rule Palestine	2020	2021	-	Mar	Sat<=30	0:00	1:00	S
 Rule Palestine	2020	only	-	Oct	24	1:00	0	-
 Rule Palestine	2021	only	-	Oct	29	1:00	0	-
 Rule Palestine	2022	only	-	Mar	27	0:00	1:00	S
-Rule Palestine	2022	max	-	Oct	Sat>=24	2:00	0	-
-Rule Palestine	2023	max	-	Mar	Sat>=24	2:00	1:00	S
+Rule Palestine	2022	max	-	Oct	Sat<=30	2:00	0	-
+Rule Palestine	2023	max	-	Mar	Sat<=30	2:00	1:00	S
 
 # Zone	NAME		STDOFF	RULES	FORMAT	[UNTIL]
 Zone	Asia/Gaza	2:17:52	-	LMT	1900 Oct

commit 2d49828e3cefd27ae8ff04567e24a773021f29cb
Author: Paul Eggert <eggert@cs.ucla.edu>
Date:   Mon Sep 5 11:18:06 2022 -0500

    * asia: Add Palestine URL (thanks to Heba Hamad).

diff --git a/asia b/asia
index 9fbeb71f..e0daae0c 100644
--- a/asia
+++ b/asia
@@ -3398,6 +3398,7 @@ Zone	Asia/Karachi	4:28:12 -	LMT	1907
 # https://mm.icann.org/pipermail/tz/attachments/20220830/9f024566/Time-0001.pdf
 # (2022-08-31): ... the Saturday before the last Sunday in March and October
 # at 2:00 AM ,for the years from 2023 to 2026.
+# (2022-09-05): https://mtit.pna.ps/Site/New/1453
 #
 # From Paul Eggert (2022-08-31):
 # For now, assume that this rule will also be used after 2026.
