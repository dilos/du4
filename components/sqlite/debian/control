Source: sqlite
Section: devel
Priority: optional
Maintainer: Laszlo Boszormenyi (GCS) <gcs@debian.org>
Build-Depends: debhelper (>= 11), autoconf (>= 2.59), libtool (>= 1.5.2), automake, libreadline-dev, tcl8.6-dev, dh-exec [solaris-any]
Standards-Version: 4.1.5

Package: sqlite
Section: database
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Recommends: sqlite3
Suggests: sqlite-doc
Description: command line interface for SQLite 2
 SQLite is a C library that implements an SQL database engine. 
 Programs that link with the SQLite library can have SQL database 
 access without running a separate RDBMS process.
 .
 NOTE: This package is SQLite version 2. Most programs that use
 SQLite use SQLite version 3. See the "sqlite3" package for that.

Package: sqlite-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Recommends: sqlite
Description: SQLite 2 documentation
 SQLite is a C library that implements an SQL database engine. 
 Programs that link with the SQLite library can have SQL database 
 access without running a separate RDBMS process.
 .
 NOTE: This package is SQLite version 2. Most programs that use
 SQLite use SQLite version 3. See the "sqlite3-doc" package for that.
 .
 This package contains the documentation that is also available on
 the SQLite homepage.

Package: libsqlite0
Section: libs
Architecture: any
Pre-Depends: ${misc:Depends}
Depends: ${shlibs:Depends}
Description: SQLite 2 shared library
 SQLite is a C library that implements an SQL database engine. 
 Programs that link with the SQLite library can have SQL database 
 access without running a separate RDBMS process.
 .
 NOTE: This package is SQLite version 2. Most programs that use
 SQLite use SQLite version 3. See the "libsqlite3-0" package for that.

Package: libsqlite0-dev
Provides: libsqlite-dev
Conflicts: libsqlite-dev
Replaces: libsqlite-dev (<< 2.7.0)
Suggests: sqlite-doc
Section: libdevel
Architecture: any
Depends: libsqlite0 (= ${binary:Version}), libc6-dev | libc-dev, ${misc:Depends}
Description: SQLite 2 development files
 SQLite is a C library that implements an SQL database engine. 
 Programs that link with the SQLite library can have SQL database 
 access without running a separate RDBMS process.
 .
 NOTE: This package is SQLite version 2. Most programs that use
 SQLite use SQLite version 3. See the "libsqlite3-dev" package for
 that.
 .
 This package contains the development files (headers, static libraries)

Package: libsqlite-tcl
Conflicts: libsqlite0-tcl
Replaces: libsqlite0-tcl
Suggests: sqlite-doc
Section: interpreters
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: SQLite 2 Tcl bindings
 SQLite is a C library that implements an SQL database engine. 
 Programs that link with the SQLite library can have SQL database 
 access without running a separate RDBMS process.
 .
 NOTE: This package is SQLite version 2. Most programs that use
 SQLite use SQLite version 3. See the "libsqlite3-tcl" package for
 that.
 .
 This package contains the Tcl bindings.
