#
# This file and its contents are supplied under the terms of the
# Common Development and Distribution License ("CDDL"), version 1.0.
# You may only use this file in accordance with the terms of version
# 1.0 of the CDDL.
#
# A full copy of the text of the CDDL should have accompanied this
# source.  A copy of the CDDL is also available via the Internet at
# http://www.illumos.org/license/CDDL.
#

#
# Copyright 2015, Igor Kozhukhov <ikozhukhov@gmail.com>.
#

# DEB makefile

-include ${HOME}/debs_conf.mk
BUILD_VERSION := $(BUILD_NUM)
-include instdepends.mk

CLEAN_PATHS += $(SOURCE_DIR)/.prep
CLEAN_PATHS += $(COMPONENT_DIR)/.deb_prep
CLEAN_PATHS += $(COMPONENT_DIR)/*.deb
CLEAN_PATHS += $(COMPONENT_DIR)/*.changes
#CLEAN_PATHS += $(DEBIAN_DIR)
CLOBBER_PATHS += $(COMPONENT_DIR)/.deb_prep
#CLOBBER_PATHS += $(SRC_DIR)

DEB_PREP ?= yes

APT_REPO ?= 		/myshare/repo/dilos
APT_DISTRIBUTION ?= 	du-unstable

DEB_MAINTAINER ?= Igor Kozhukhov <igor@dilos.org>
DEB_ARCHITECTURE ?= solaris-$(MACH)

DEBS := $(shell cat $(COMPONENT_DIR)/debs/control | $(GREP) "Source: " | $(AWK) '{print $$2}')
DEBSVER := $(shell cat $(COMPONENT_DIR)/debs/changelog | head -n 1 | $(GSED) -e 's;.*(;;' -e 's;).*;;')

#DEBPKGS ?= $(CANONICAL_MANIFESTS:%.p5m=$(BUILD_DIR)/%.deb_stamp)
APTPKGS ?= $(DEBS:%=$(BUILD_DIR)/%_$(DEBSVER)_$(DEB_ARCHITECTURE).apt_stamp)

REPREPRO = /usr/bin/reprepro

DEB_BUILD_FLAGS ?= -d -b -uc -j$(JOBS)
SUDO ?= sudo -E

#export DEB_BUILD_OPTIONS=parallel=$(JOBS)

BUILD_DEB2 = \
	cd $(DEBIAN_DIR)/.. && \
	PATH=/usr/bin:/sbin:/usr/sbin \
	/usr/bin/dpkg-buildpackage $(DEB_BUILD_FLAGS)

CLEAN_DEBS = \
	for f in $(DEBS); do \
	    $(RM) $(WS_DEBS)/$$f"_"* ; \
	    $(RM) -r $(DEBIAN_DIR)/$$f ; \
	    $(RM) $(BUILD_DIR)/$$f"_"* ; \
	done

CLEAN_DEBS_BLD = \
	for f in `ls -1 $(COMPONENT_DIR)/debs`; do \
	    $(RM) $(DEBIAN_DIR)/$$f ; \
	done

INSTALLED_BUILD_DEPENDS = $(INSTDEPENDS:%=/var/lib/dpkg/info/%.md5sums)

include $(WS_TOP)/make-rules/deb_bld_deps.mk

INSTALLED_INIT_BUILD_DEPENDS_ALL = $(INSTALLED_INIT_BUILD_DEPENDS:%=/var/lib/dpkg/info/%.md5sums)

COMPONENT_PRE_DEB_ACTION = \
	( $(CLEAN_DEBS) \
	&& $(RM) $(DEBIAN_DIR)/files $(DEBIAN_DIR)/*.log $(DEBIAN_DIR)/*.substvars )

.DEFAULT:		deb

.SECONDARY:

#apt:	deb $(APTPKGS)

#deb:		debprep build install $(WS_DEBS) $(BUILD_DIR)/.debs2
deb:		build install $(BUILD_DIR)/.debs2

debprep:	$(COMPONENT_DIR)/.deb_prep

deb-unprep:
	$(SUDO) apt-get remove -y $(INSTALLED_INIT_BUILD_DEPENDS) ; \
	PATH=/usr/bin:/sbin:/usr/sbin \
	$(SUDO) apt-get autoremove -y

$(COMPONENT_DIR)/.deb_prep:
	echo "Y" | \
	$(SUDO) apt-get install -y $(INSTALLED_INIT_BUILD_DEPENDS) $(INSTDEPENDS)
	$(TOUCH) $@

ifeq ($(DEB_PREP),yes)
$(SOURCE_DIR)/.prep:	$(COMPONENT_DIR)/.deb_prep

#$(CONFIGURE_32):	$(COMPONENT_DIR)/.deb_prep $(BUILD_COMPONENTS_DEPENDS.32)
#$(CONFIGURE_64):	$(COMPONENT_DIR)/.deb_prep $(BUILD_COMPONENTS_DEPENDS.64)
#$(BUILD_32):		$(COMPONENT_DIR)/.deb_prep $(BUILD_COMPONENTS_DEPENDS.32)
#$(BUILD_64):		$(COMPONENT_DIR)/.deb_prep $(BUILD_COMPONENTS_DEPENDS.64)
endif

#$(WS_DEBS):
#	$(RM) -r $(WS_DEBS)
#	$(MKDIR) $(WS_DEBS)

apt-export:
	@(while true; do \
	    if [ ! -f $(APT_REPO)/db/lockfile ]; then \
		break; \
	    fi; \
	    sleep 5; \
	done)
	(cd $(APT_REPO) && $(REPREPRO) -b . export $(APT_DISTRIBUTION))

$(BUILD_DIR)/.debian_stamp: $(DEBIAN_DIR) $(COMPONENT_DIR)/debs/control
	$(CP) -r $(COMPONENT_DIR)/debs/* $(DEBIAN_DIR)
	touch $@

$(BUILD_DIR)/.debs2: $(COMPONENT_DIR)/.deb_prep $(WS_DEBS) $(DEBIAN_DIR) $(BUILD_DIR)/.debian_stamp
	-$(COMPONENT_PRE_DEB_ACTION)
	$(BUILD_DEB2)
	-$(CP) $(DEBIAN_DIR)/../../*.deb $(DEBIAN_DIR)/../../*.changes $(WS_DEBS)/
	-$(COMPONENT_POST_DEB_ACTION)
	touch $@

$(BUILD_DIR)/%.apt_stamp: $(WS_DEBS)/%.changes
	@(while true; do \
	    if [ ! -f $(APT_REPO)/db/lockfile ]; then \
		break; \
	    fi; \
	    sleep 5; \
	done)
	( cd $(APT_REPO) && \
	    ( $(REPREPRO) -b . \
	    --export=never include $(APT_DISTRIBUTION) \
	    $< ) )
	touch $@

/var/lib/dpkg/info/%.md5sums:
	$(SUDO) apt-get install -y $(INSTALLED_INIT_BUILD_DEPENDS) $(INSTDEPENDS)
#	pkgname=`echo $(@F) | sed -e 's;\.md5sums;;'`; \
#	PATH=/usr/gnu/bin:/usr/bin:/sbin:/usr/sbin \
#	$(SUDO) apt-get install -y $$pkgname

$(WS_COMPONENTS)/%/build/prototype:
	INSTDEPTARGET=`dirname $(@D)`; \
	echo $$INSTDEPTARGET ; \
	$(MAKE) -C $$INSTDEPTARGET JOBS=$(JOBS) install

ifneq ($(BLDDEPENDS),)
$(WS_COMPONENTS)/%/build/$(MACH32)/.installed:
	BLDDEPTARGET=`dirname $(@D)`; \
	BLDDEPTARGET=`dirname $$BLDDEPTARGET`; \
	$(MAKE) -C $$BLDDEPTARGET JOBS=$(JOBS) install

$(WS_COMPONENTS)/%/build/$(MACH64)/.installed:
	BLDDEPTARGET=`dirname $(@D)`; \
	BLDDEPTARGET=`dirname $$target`; \
	$(MAKE) -C $$BLDDEPTARGET JOBS=$(JOBS) install
endif

debinstdeps:
	@echo $(INSTDEPENDS)

debuninstdeps:
	$(SUDO) apt-get remove -y $(INSTDEPENDS) ; \
	$(SUDO) apt-get autoremove -y

#debclean:
#	$(RM) $(DEBPKGS) $(APTPKGS)
#	$(RM) $(COMPONENT_DIR)/*.deb
#	$(RM) $(COMPONENT_DIR)/*.changes
#	$(RM) $(COMPONENT_DIR)/*.deb_prep
#	$(RM) $(BUILD_DIR)/*.mangled
#	$(RM) $(BUILD_DIR)/*.mogrified
#	$(RM) $(BUILD_DIR)/*.published
#	$(RM) $(BUILD_DIR)/*.depend*

debclean:
	$(CLEAN_DEBS)
	$(RM) $(BUILD_DIR)/.debs2
	$(RM) $(BUILD_DIR)/.debian_stamp
	$(CLEAN_DEBS_BLD)
	$(RM) $(DEBIAN_DIR)/files $(DEBIAN_DIR)/*.log $(DEBIAN_DIR)/*.substvars

debgenlog:
	$(WS_TOP)/tools/deb_gen_log.pl

#debpkgclean:
#	$(CLEAN_DEBS)

debclobber:	debuninstdeps clean clobber
	$(RM) $(CLEAN_PATHS) $(CLOBBER_PATHS)
#	$(RM) -r $(BUILD_DIR)

#$(SOURCE_DIR) $(DEBIAN_DIR) $(PROTO_DIR) $(WS_DEBS):
#	$(MKDIR) $@

#clean::
#	$(RM) -r $(BUILD_DIR)
#	$(RM) -r $(CLEAN_PATHS)

#clobber:: clean
#	$(RM) -r $(CLOBBER_PATHS)

#.PHONY: deb debprep build install debclean debclobber debuninstdeps apt apt-export debsclean
